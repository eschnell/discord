from timeit					import default_timer as timer

from cogs.utils.api			import MyAPIs
from cogs.utils.webhooks	import Webhook
from cogs.utils.perms		import *
from cogs.utils.config		import *

import discord.ext.commands as cmds
import discord as dis
import cprint
import datetime
import asyncio
import json
import math
import os
#import psutil
import re
import subprocess
#import signal
import sys
import time
import traceback



prefix = ("/") #, "!")
bot = cmds.AutoShardedBot(command_prefix=prefix)
startup_extensions = [
	"@bot_core_commands",
	"activity",
	"bts",
	"bugtracker",
	"cmds",
	"debugger",
	"devtools",
	"fun",
	"greeter",
	"help",
	"language",
	"leveler",
	"lockdown",
	"logger",
	"moderation",
	"music",
	"osu",
	"qrgen",
	"sauce",
	"serverinfo",
	"ttt",
	"urb_d",
	"userinfo",
	"weather"
	]

##============================================== #
# =============================================  #
#                                                #
#   EVENTS                                       #
#  ============================================= #
# ==============================================##

## Preload
def preload(bot):
	bot.name = "Bot-anical"
	bot.debug = False
	bot.apis = MyAPIs(bot).get_keys()
	bot.startup_cogs = startup_extensions
	bot.bot_id = 669976987485339658
	bot.owner_id = 286562388461748228
	bot.prefix = prefix
	bot.uptime = datetime.datetime.now()
	bot.self_log = bot.all_log = {}
	bot.log_conf = {}
	bot.prefixes = prefix
	bot.bot_prefix = ":robot:"
	bot.is_stream = False
	bot.message_count = bot.mention_count = bot.keyword_log = 0
	bot.game = bot.game_interval = bot.avatar = bot.avatar_interval = bot.subpro = bot.keyword_found = None
	bot.game_time = bot.avatar_time = bot.gc_time = bot.refresh_time = datetime.time()
	bot.command_count = {}
	if not dis.opus.is_loaded():
		dis.opus.load_opus("libopus.so")
	# DO NOT TOUCH THIS LINE BELOW (For chainloading cogs !)
	bot.load_extension("cogs.@bot_core_commands")

## On error
@bot.event
async def on_command_error(ctx, err):
	if isinstance(err, cmds.CommandNotFound):
		await ctx.send("Command doesn't exist.")

	elif isinstance(err, cmds.MissingRequiredArgument):
		cmd_name = ctx.message.content.split('/')[1]
		await ctx.send("You are missing required argument(s).\nPlease consult `/help {}`".format(cmd_name))

		#print(ctx.command)
		#try:
		#	help_str = ctx.help()
		#	print(help_str)
		#except:
		#	pass
		#help = ctx.invoke()

	elif isinstance(err, cmds.NotOwner):
		await ctx.send("You do not own this bot.")

	elif isinstance(err, cmds.CheckFailure):
		await ctx.send("You don't have permissions to use that command.")

	elif isinstance(err, cmds.BadArgument):
		await ctx.send("You have given an invalid argument.")

	elif isinstance(err, cmds.ExtensionNotLoaded):
		await ctx.send("Module is not loaded.")

	elif isinstance(err, cmds.TooManyArguments):
		await ctx.send("You have given too many argument(s).")

	elif isinstance(err, cmds.CommandOnCooldown):
		cmd_name = ctx.message.content.split('/')[1].split(' ')[0]
		await ctx.send("{} command on cooldown. Try again in {}s.".format(cmd_name.capitalize(), int(err.retry_after)))

	elif isinstance(err, cmds.errors.CommandInvokeError): #cmds.errors.DiscordException):
		trace = traceback.format_exception(type(err), err, err.__traceback__)
		print("".join(trace))
		await ctx.send(err)

	else:
		await ctx.send("An error occurred with the `{}` command. Please contact an administrator for details.".format(ctx.command.name))
		print("Ignoring exception in command {}".format(ctx.command.name))
		trace = traceback.format_exception(type(err), err, err.__traceback__)
		print("".join(trace))




if __name__ == "__main__":

	# load all variables
	preload(bot)

	if len(sys.argv) == 3:
		if (sys.argv[1] == "-t" or sys.argv[1] == "--token"):
			bot.run(sys.argv[2])
