from discord.ext		import commands as cmds
from pymongo			import MongoClient			# DB

from cogs.utils.perms	import *					# Perms
#from cogs.utils.logger	import Logger				# Logging class
#from cogs.utils.dataIO	import dataIO				# JSON & HJSON IO
#from cogs.utils.locale	import get_user_locale		# Locales

import discord as dis
import requests

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

class Gmod(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot

		## For Logger module
		#self.logname = __name__								# Needed by logger class
		#self.log = Logger(self)								# Logger obj

		## For DB socket
		self.dbclient = MongoClient("25.105.87.145", 27717)	# IP and port
		self.db = self.dbclient.gmod						# DB Table name

	@cmds.group(no_pm=True, aliases=["gmod"])
	async def gm(self, ctx):
		"""Gmod options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@gm.command(aliases=["dk"])
	async def delkey(self, ctx, key: str):
		pass

	@gm.command(aliases=["ak"])
	async def addkey(self, ctx, key: str):
		await ctx.message.delete()
		gid = ctx.guild.id
		d = { f"api_key": key }
		self.db.gmod.update_one({"guild_id": gid}, {"$set": d}, upsert=True)
		await ctx.send("API key successfully updated for this server.")

	@gm.command(aliases=["stat"])
	async def stats(self, ctx):
		gid = ctx.guild.id
		d = self.db.gmod.find_one({"guild_id": gid})
		if d is None: return await ctx.send("No key registered.")
		r = requests.get(
			f"https://gmod-servers.com/api/?object=servers&element=detail&key={d['api_key']}")
		#print(r.json())

		if r.status_code == 200 and "error" in r.text[0:10].lower():
			return await ctx.send(r.text)
		ret = r.json()
		steamlink = f"steam://connect/{ret['address']}:{ret['port']}"

		em = dis.Embed(title="Stats 📈") #, description=m)
		em.add_field(name="Title // Hostname",
					 value=f"**{ret['hostname']}**",
					 inline=False)
		em.add_field(name="🔗 Connect with Steam",
					 value=steamlink,
					 inline=False)
		em.add_field(name="🚥 Status",
					 value=("Offline", "Online")[int(ret["is_online"])])
		em.add_field(name="🔐 Password required to join",
					 value=("No Password", "Password REQUIRED")[int(ret["password"])])
		em.add_field(name="🖧 Address",
					 value=ret["address"],
					 inline=False)
		em.add_field(name="🔌 Port",
					 value=ret["port"],
					 inline=False)
		em.add_field(name="🕴️ Players",
					 value=f"{ret['players']}/{ret['maxplayers']}")
		em.add_field(name="🗺 Map",
					 value=ret["map"])
		em.add_field(name="⚙️ Version",
					 value=f"`{ret['version']}`")
		em.add_field(name="🖥️ Platform",
					 value=ret["platform"].capitalize())

		em.set_footer(text=f"Last Check : {ret['last_check']}")
		await ctx.send(embed=em)







def setup(bot):
	bot.add_cog(Gmod(bot))
