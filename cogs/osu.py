from discord.ext			import commands as cmds
from discord.utils			import find
from bs4					import BeautifulSoup

from cogs.utils.perms		import *
from cogs.utils.dataIO		import dataIO
from cogs.utils.locale		import get_user_locale

import discord as dis
import aiohttp
import datetime
import operator
import os
import re
import urllib.request


import gettext


class Osu(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		self.help_msg = [
			f"**No linked account (`{bot.prefix}osuset user [username]`) or not using **`{bot.prefix}command [username] [gamemode]`",
			f"**No linked account (`{bot.prefix}osuset user [username]`)**"
		]
		self.modes = ["osu", "taiko", "ctb", "mania"]
		self.osu_api_key = self.bot.apis["osu"]
		self.user_settings = dataIO.load_hjson("data/osu/user_settings.hjson")
		self.track = dataIO.load_hjson("data/osu/track.hjson")
		self.osu_settings = dataIO.load_hjson("data/osu/osu_settings.hjson")
		self.num_max_prof = 8
		self.max_map_disp = 3


# ===================================================
#	SETTINGS
# ===================================================

	@cmds.group(no_pm=True)
	async def osuset(self, ctx):
		"""Where you can define some settings"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@cmds.check(is_me)
	@osuset.command(no_pm=True)
	async def tracktop(self, ctx, top_num:int):
		""" Set # of top plays being tracked """
		msg = ""
		if top_num < 1 or top_num > 100:
			msg = "**Please enter a valid number. (1 - 100)**"
		else:
			self.osu_settings["num_track"] = top_num
			msg = f"**Now tracking Top {top_num} Plays.**"
			dataIO.save_hjson("data/osu/osu_settings.hjson", self.osu_settings)
		await ctx.send(msg)

	@cmds.check(is_me)
	@osuset.command(no_pm=True)
	async def displaytop(self, ctx, top_num:int):
		""" Set # of best plays being displayed in top command """
		msg = ""
		if top_num < 1 or top_num > 10:
			msg = "**Please enter a valid number. (1 - 10)**"
		else:
			self.osu_settings["num_best_plays"] = top_num
			msg = f"**Now Displaying Top {top_num} Plays.**"
			dataIO.save_hjson("data/osu/osu_settings.hjson", self.osu_settings)
		await ctx.send(msg)

	@cmds.check(is_admin)
	@osuset.command(no_pm=True)
	async def tracking(self, ctx, toggle=None):
		""" For disabling tracking on server (enable/disable) """
		g = ctx.message.guild
		gid = str(g.id)

		if gid not in self.osu_settings:
			self.osu_settings[gid] = {}
			self.osu_settings[gid]["tracking"] = True

		status = ""
		if not toggle:
			self.osu_settings[gid]["tracking"] = not self.osu_settings[gid]["tracking"]
			status = ("Disabled", "Enabled")[self.osu_settings[gid]["tracking"]]
		elif toggle.lower() == "enable":
			self.osu_settings[gid]["tracking"] = True
			status = "Enabled"
		elif toggle.lower() == "disable":
			self.osu_settings[gid]["tracking"] = False
			status = "Disabled"
		dataIO.save_hjson("data/osu/osu_settings.json", self.osu_settings)
		await self.bot.say(f"**Player Tracking {g.name} on {status}.**")

	@cmds.check(is_dev)
	@osuset.command(pass_context=True, no_pm=True)
	async def overview(self, ctx):
		""" Get an overview of your settings """
		guild = ctx.message.guild
		gid = str(guild.id)
		user = ctx.message.author

		em = dis.Embed(colour=user.colour)
		em.set_author(name="Current Settings for {}".format(guild.name), icon_url = guild.icon_url)

		# determine api to use
		if gid in self.osu_settings and "api" in self.osu_settings[gid]:
			if self.osu_settings[gid]["api"] == self.osu_settings["type"]["default"]:
				api = "Official Osu! API"
			elif self.osu_settings[gid]["api"] == self.osu_settings["type"]["ripple"]:
				api = "Ripple API"
		else:
			api = "Official Osu! API"

		# determine
		if gid not in self.osu_settings or "tracking" not in self.osu_settings[gid] or self.osu_settings[gid]["tracking"] == True:
			tracking = "Enabled"
		else:
			tracking = "Disabled"
		info = f"**▸ Default API:** {api}\n"
		info += f"**▸ Tracking:** {tracking}\n"
		if tracking == "Enabled":
			info += f"**▸ Tracking Number:** {self.osu_settings['num_track']}\n"
		info += f"**▸ Top Plays:** {self.osu_settings['num_best_plays']}"

		em.description = info
		await ctx.send(embed=em)

	@cmds.check(is_me)
	@osuset.command(no_pm=True)
	async def api(self, ctx, *, choice):
		"""'official' or 'ripple'"""
		guild = ctx.message.guild
		gid = str(guild.id)
		if gid not in self.osu_settings:
			self.osu_settings[gid] = {}

		if not choice.lower() == "official" and not choice.lower() == "ripple":
			await ctx.send("The two choices are `official` and `ripple`")
			return
		elif choice.lower() == "official":
			self.osu_settings[gid]["api"] = self.osu_settings["type"]["default"]
		elif choice.lower() == "ripple":
			self.osu_settings[gid]["api"] = self.osu_settings["type"]["ripple"]
		dataIO.save_hjson("data/osu/osu_settings.hjson", self.osu_settings)
		await ctx.send(f"**Switched to `{choice}` server as default on `{guild.name}`.** :arrows_counterclockwise:")

	@osuset.command(no_pm=True)
	async def default(self, ctx, mode:str):
		""" Set your default gamemode """
		user = ctx.message.author
		uid = str(user.id)
		#server = ctx.message.guild

		if mode.lower() in self.modes:
			gamemode = self.modes.index(mode.lower())
		elif int(mode) >= 0 and int(mode) <= 3:
			gamemode = int(mode)
		else:
			await ctx.send("**Please enter a valid gamemode.**")
			return

		if uid in self.user_settings:
			self.user_settings[uid]['default_gamemode'] = int(gamemode)
			await ctx.send(f"**`{user.name}`'s default gamemode has been set to `{self.modes[gamemode]}`.** :white_check_mark:")
			dataIO.save_hjson("data/osu/user_settings.hjson", self.user_settings)
		else:
			await ctx.send(self.help_msg[1])

	@osuset.command(no_pm=True)
	async def user(self, ctx, *, username):
		"""Sets user information given an osu! username"""
		user = ctx.message.author
		gid = str(user.guild.id)
		uid = str(user.id)
		key = self.osu_api_key

		if gid not in self.user_settings:
			self.user_settings[gid] = {}

		if not self._check_user_exists(user):
			try:
				osu_user = list(await self.get_user(key, self.osu_settings["type"]["default"], username, 1))
				newuser = {
					"discord_username": user.name,
					"osu_username": username,
					"osu_user_id": osu_user[0]["user_id"],
					"default_gamemode": 0,
					"ripple_username": ""
				}
				self.user_settings[uid] = newuser
				dataIO.save_hjson("data/osu/user_settings.hjson", self.user_settings)
				await ctx.send(f"{user.mention}, your account has been linked to osu! username `{osu_user[0]['username']}`")
			except:
				await ctx.send(f"`{username}` doesn't exist in the osu! database.")
		else:
			try:
				osu_user = list(await self.get_user(key, self.osu_settings["type"]["default"], username, 1))
				self.user_settings[uid]["osu_username"] = username
				self.user_settings[uid]["osu_user_id"] = osu_user[0]["user_id"]
				dataIO.save_hjson("data/osu/user_settings.hjson", self.user_settings)
				await ctx.send(f"{user.mention}, your osu! username has been edited to `{osu_user[0]['username']}`")
			except:
				await ctx.send(f"`{username}` doesn't exist in the osu! database.")


	@cmds.command(no_pm=True)
	async def osu(self, ctx, *username):
		"""Gives osu user(s) stats. Use -ripple/-official to use specific api."""
		await self._process_user_info(ctx, username, 0)




# METODS

	# Gets json information to proccess the small version of the image
	async def _process_user_info(self, ctx, usernames, gamemode: int):
		key = self.osu_api_key
		user = ctx.message.author
		g = user.guild

		if not usernames:
			usernames = [None]
		# get rid of duplicates
		usernames = list(set(usernames))
		# determine api to use
		usernames, api = self._determine_api(g, usernames)
		# gives the final input for osu username
		final_usernames = []
		for username in usernames:
			test_username = await self._process_username(ctx, username)
			if test_username != None:
				final_usernames.append(test_username)
		# testing if username is osu username
		all_user_info = []
		sequence = []
		count_valid = 0
		for i in range(len(final_usernames)):
			userinfo = list(await self.get_user(key, api, final_usernames[i], gamemode)) # get user info from osu api
			if userinfo != None and len(userinfo) > 0 and userinfo[0]["pp_raw"] != None:
				all_user_info.append(userinfo[0])
				sequence.append((count_valid, int(userinfo[0]["pp_rank"])))
				count_valid = count_valid + 1
			else:
				await ctx.send(f"**`{final_usernames[i]}` has not played enough.**")

		sequence = sorted(sequence, key=operator.itemgetter(1))
		all_players = []
		for i, __ in sequence:
			all_players.append(await self._get_user_info(api, g, user, all_user_info[i], gamemode))

		disp_num = min(self.num_max_prof, len(all_players))
		if disp_num < len(all_players):
			await ctx.send(f"Found {len(all_players)} users, but displaying top {disp_num}.")
		#print(disp_num)
		for player in all_players[0:disp_num]:
			await ctx.send(content=None, embed=player)

	## processes username. probably the worst chunck of code in this project so far. will fix/clean later
	async def _process_username(self, ctx, username):
		channel = ctx.message.channel
		user = ctx.message.author
		key = self.osu_api_key

		# if nothing is given, must rely on if there's account
		if not username:
			if self._check_user_exists(user):
				username = self.user_settings[str(user.id)]["osu_username"]
			else:
				await ctx.send(f"It doesn't seem that you have an account linked. Do **{self.bot.prefix}osuset user [username]**.")
				return None # bad practice, but too lazy to make it nice
		# if it's a discord user, first check to see if they are in database and choose that username
		# then see if the discord username is a osu username, then try the string itself
		elif find(lambda m: m.name == username, channel.guild.members) is not None:
			target = find(lambda m: m.name == username, channel.guild.members)
			try:
				self._check_user_exists(target)
				username = self.user_settings[target.id]["osu_username"]
			except:
				if await self.get_user(key, self.osu_settings["type"]["default"], username, 0):
					username = str(target)
				else:
					await ctx.send(self.help_msg[1])
					return
		# @ implies its a discord user (if not, it will just say user not found in the next section)
		# if not found, then oh well.
		elif "@" in username:
			user_id = re.findall(r"\d+", username)
			user_id = user_id[0]
			if user_id in self.user_settings:
				username = self.user_settings[user_id]["osu_username"]
			else:
				await ctx.send(self.help_msg[1])
				return
		else:
			username = str(username)
		return username

	# find user_info based on `id`
	async def get_user(self, key, api: str, user_id, mode):
		url_params = []

		url_params.append(self.parameterize_key(key))
		url_params.append(self.parameterize_id("u", user_id))
		url_params.append(self.parameterize_mode(mode))

		async with aiohttp.ClientSession() as client:
			async with client.get(self.build_request(url_params, f"https://{api}/api/get_user?")) as resp:
				return await resp.json()

	# Gives a small user profile
	async def _get_user_info(self, api: str, server, server_user, user, gamemode: int):
		_ = get_user_locale("osu", server_user)
		if api == self.osu_settings["type"]["default"]:
			profile_url = f"http://s.ppy.sh/a/{user['user_id']}.png"
			pp_country_rank = f" ({user['country']}#{user['pp_country_rank']})"
		elif api == self.osu_settings["type"]["ripple"]:
			profile_url = f"http://a.ripple.moe/{user['user_id']}.png"
			pp_country_rank = ""
		flag_url = f"https://new.ppy.sh//images/flags/{user['country']}.png"
		gamemode_text = self._get_gamemode(gamemode)

		#try:
		user_url = f"https://{api}/u/{user['user_id']}"
		em = dis.Embed(colour=server_user.colour)

		em.set_author(name=_("{} Profile for {}").format(gamemode_text, user["username"]),
					 icon_url=flag_url,
					 url=user_url)
		em.set_thumbnail(url=profile_url)
		level_int = int(float(user["level"]))
		level_percent = float(user["level"]) - level_int
													#
		info = _("**▸ {} Rank:** #{} {}\n").format(self._get_api_name(api), user['pp_rank'], pp_country_rank)
		info += _("**▸ Level:** {} ({:.2f}%)\n").format(level_int, level_percent * 100)
		info += _("**▸ Total PP:** {}\n").format(user["pp_raw"])
		info += _("**▸ Playcount:** {}\n").format(user["playcount"])
		info += _("**▸ Hit Accuracy:** {}%").format(user["accuracy"][0:5])
		em.description = info
		''' if api == self.osu_settings["type"]["default"]:
			soup = BeautifulSoup(urllib.request.urlopen(f"https://osu.ppy.sh/u/{user['user_id']}"), "lxml")
			timestamps = []
			# WORKS
			#for tag in soup.find_all("div", class_="osu-layout__section osu-layout__section--full js-content community_profile"):
			#	print(tag)
			ret = soup.select("div#fixable.prof-left.paddingboth.minimal")
				#.find(
				#class_="js-react--profile-page osu-layout osu-layout--full")
			with open("tmp.txt", "w") as f:
				for elem in ret:
					tag_to_str = str(elem)
					f.write(tag_to_str)
					f.write("\n\n")
			#for tag in soup.findAll(attrs={"class": "timeago"}):
			#	timestamps.append(datetime.datetime.strptime(tag.contents[0].strip().replace(" UTC", ""), "%Y-%m-%d %H:%M:%S"))
			#print(timestamps, "\n", len(timestamps))
			timeago = datetime.datetime(1, 1, 1) + (datetime.datetime.utcnow() - timestamps[1])
			time_ago = "Last Online "
			if timeago.year - 1 != 0:
				time_ago += f"{timeago.year - 1} Years "
			if timeago.month - 1 !=0:
				time_ago += f"{timeago.month - 1} Months "
			if timeago.day - 1 !=0:
				time_ago += f"{timeago.day - 1} Days "
			if timeago.hour != 0:
				time_ago += f"{timeago.hour} Hours "
			if timeago.minute != 0:
				time_ago += f"{timeago.minute} Minutes "
			time_ago += f"{timeago.second} Seconds ago"
			em.set_footer(text=time_ago) '''
		return em
		#except:
		#	return None

	# takes iterable of inputs and determines api, also based on defaults
	def _determine_api(self, server, inputs):
		if not inputs or ('-ripple' not in inputs and '-official' not in inputs): # in case not specified
			if server.id in self.osu_settings and "api" in self.osu_settings[server.id]:
				if self.osu_settings[server.id]["api"] == self.osu_settings["type"]["default"]:
					api = self.osu_settings["type"]["default"]
				elif self.osu_settings[server.id]["api"] == self.osu_settings["type"]["ripple"]:
					api = self.osu_settings["type"]["ripple"]
			else:
				api = self.osu_settings["type"]["default"]
		elif '-ripple' in inputs:
			inputs = list(inputs)
			inputs.remove('-ripple')
			api = self.osu_settings["type"]["ripple"]
		elif '-official' in inputs:
			inputs = list(inputs)
			inputs.remove('-official')
			api = self.osu_settings["type"]["default"]
		if not inputs:
			inputs = [None]
		return inputs, api

	# Checks if user exists
	def _check_user_exists(self, user):
		return (True, False)[str(user.id) not in self.user_settings]

	def _get_api_name(self, url:str):
		return ("Official", "Ripple")[url == self.osu_settings["type"]["ripple"]]

	# Int to str
	def _get_gamemode(self, gamemode: int):
		if gamemode == 1:
			return "Taiko"
		elif gamemode == 2:
			return "Catch the Beat!"
		elif gamemode == 3:
			return "Osu! Mania"
		else:
			return "Osu! Standard"

# Returns the full API request URL using the provided base URL and parameters.
	def build_request(self, url_params, url):
		for param in url_params:
			url += str(param)
			if (param != ""):
				url += "&"
		return url[:-1]

	def parameterize_event_days(self, event_days):
		if event_days == "":
			event_days = "event_days=1"
		elif (int(event_days) >= 1 and int(event_days) <= 31):
			event_days = f"event_days={event_days}"
		else:
			print("Invalid Event Days")
		return event_days

	def parameterize_id(self, t, id):
		if (t != "b" and t != "s" and t != "u" and t != "mp"):
			print("Invalid Type")
		if len(str(id)) != 0:
			return f"{t}={id}"
		else:
			return ""

	def parameterize_key(self, key):
		if len(key) == 40:
			return f"k={key}"
		else:
			print("Invalid Key")

	def parameterize_limit(self, limit):
		## Default case: 10 scores
		if limit == "":
			limit = "limit=10"
		elif (int(limit) >= 1 and int(limit) <= 100):
			limit = f"limit={limit}"
		else:
			print("Invalid Limit")
		return limit

	def parameterize_mode(self, mode):
		## Default case: 0 (osu!)
		if mode == "":
			mode = "m=0"
		elif (int(mode) >= 0 and int(mode) <= 3):
			mode = f"m={mode}"
		else:
			print("Invalid Mode")
		return mode




# ===================================================
#	SETUP
# ===================================================

def check_files():
	if not os.path.exists("data/osu"):
		print("Creating data/osu folder...")
		os.makedirs("data/osu")

	# creates file for user backgrounds
	user_file = "data/osu/user_settings.hjson"
	if not dataIO.is_valid_hjson(user_file):
		print("Adding data/osu/user_settings.hjson...")
		dataIO.save_hjson(user_file, {})

	# creates file for player tracking
	user_file = "data/osu/track.hjson"
	if not dataIO.is_valid_hjson(user_file):
		print("Adding data/osu/track.hjson...")
		dataIO.save_hjson(user_file, {})

	# creates file for server to use
	settings_file = "data/osu/osu_settings.hjson"
	if not dataIO.is_valid_hjson(settings_file):
		print("Adding data/osu/osu_settings.hjson...")
		dataIO.save_hjson(settings_file, {
			"type": {
				"default": "osu.ppy.sh",
				"ripple":"ripple.moe"
			},
			"num_track" : 50,
			"num_best_plays": 5
		})





def setup(bot):
	check_files()
	bot.add_cog(Osu(bot))
