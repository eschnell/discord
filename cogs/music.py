import discord as dis
import asyncio
import lavalink
import math
import re
import requests

from discord.ext		import commands as cmds
from bs4				import BeautifulSoup
from itertools			import chain
from cogs.utils.perms	import *

url_rx = re.compile("https?:\\/\\/(?:www\\.)?.+")  # noqa: W605


class Music(cmds.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.np = {}
		self.startChannel = {}

		if not hasattr(bot, "lavalink"):  # This ensures the client isn't overwritten during cog reloads.
			bot.lavalink = lavalink.Client(bot.user.id)
			bot.lavalink.add_node("25.105.87.145", 2333, "youshallnotpass", "eu", "default-node")  # Host, Port, Password, Region, Name
			bot.add_listener(bot.lavalink.voice_update_handler, "on_socket_response")

		#bot.add_listener(self.voice_update_hook, "on_voice_state_update")
		bot.lavalink.add_event_hook(self.track_hook)

	def cog_unload(self):
		self.bot.lavalink._event_hooks.clear()

	async def cog_before_invoke(self, ctx):
		guild_check = ctx.guild is not None
		# This is essentially the same as `@cmds.guild_only()`
		# except it saves us repeating ourselves (and also a few lines).
		if guild_check:
			await self.ensure_voice(ctx)
			#  Ensure that the bot and command author share a mutual voicechannel.
		return guild_check

	async def cog_command_error(self, ctx, error):
		if isinstance(error, cmds.CommandInvokeError):
			await ctx.send(error.original)
			# The above handles errors thrown in this cog and shows them to the user.
			# This shouldn't be a problem as the only errors thrown in this cog are from `ensure_voice`
			# which contain a reason string, such as "Join a voicechannel" etc. You can modify the above
			# if you want to do things differently.

	async def voice_update_hook(self, member, before, after):
		print(f"BEFORE\nMember = {member}\n{before}")
		print(f"AFTER\nMember = {member}\n{after}")
		print("\n\n\n")
		if len(after.channel.members) == 1:
			if after.channel.members == self.bot.user:
				pass
		#print(self.bot.user)

	async def track_hook(self, event):
		#print(f"EVENT = {event}") // lavalink.events.TrackEndEvent \\
		if isinstance(event, lavalink.events.QueueEndEvent):
			# Disconnect from the channel -- there's nothing else to play.
			guild_id = int(event.player.guild_id)
			del(self.startChannel[str(guild_id)])
			if str(guild_id) in self.np.keys():
				await self.np[str(guild_id)].delete()			# Del old now_playing msg
				del(self.np[str(guild_id)])
			await asyncio.sleep(60)
			await self.connect_to(guild_id, None)
		if isinstance(event, lavalink.events.TrackStartEvent):
			guild_id = int(event.player.guild_id)
			ch = self.startChannel[str(guild_id)]
			async with ch.typing():
				em = self.create_embed(event.player, True)			# Current track
				if str(guild_id) in self.np.keys():
					await self.np[str(guild_id)].delete()			# Del old now_playing msg
					del(self.np[str(guild_id)])
			self.np[str(guild_id)] = await ch.send(content=None, embed=em)


	async def connect_to(self, guild_id: int, channel_id: str):
		"""Connects to the given voicechannel ID. A channel_id of `None` means disconnect."""
		ws = self.bot._connection._get_websocket(guild_id)
		await ws.voice_state(str(guild_id), channel_id)
		# The above looks dirty, we could alternatively use `bot.shards[shard_id].ws` but that assumes
		# the bot instance is an AutoShardedBot.

	async def delete_np(self, ctx):
		"""Cleanup now playing msg."""
		try:
			await self.np[str(ctx.guild.id)].delete() ##
			del(self.startChannel[str(ctx.guild.id)])
		except:
			pass

	@cmds.command(aliases=["p"])
	async def play(self, ctx, *, query: str):
		"""Searches and plays a song from a given query."""
		await ctx.message.delete()
		player = self.bot.lavalink.players.get(ctx.guild.id)
		query = query.strip("<>")

		if not url_rx.match(query):
			query = f"ytsearch:{query}"

		results = await player.node.get_tracks(query)

		if not results or not results["tracks"]:
			return await ctx.send("Nothing found!")

		em = dis.Embed(color=dis.Color.blurple())

		if results["loadType"] == "PLAYLIST_LOADED":
			tracks = results["tracks"]

			for track in tracks:
				player.add(requester=ctx.author.id, track=track)

			em.title = "Playlist Enqueued!"
			em.description = f"{results['playlistInfo']['name']} - {len(tracks)} tracks"
		else:
			track = results["tracks"][0]
			em.title = "Track Enqueued"
			em.description = f"[{track['info']['title']}]({track['info']['uri']})"
			player.add(requester=ctx.author.id, track=track)
		await ctx.send(embed=em)

		if not player.is_playing:
			self.startChannel[str(ctx.guild.id)] = ctx.channel
			await player.play()

	@cmds.check(is_dj)
	@cmds.command()
	async def seek(self, ctx, *, seconds: int):
		"""Seeks to a given position in a track."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		track_time = player.position + (seconds * 1000)
		await player.seek(track_time)

		await ctx.send(f"Moved track to **{lavalink.utils.format_time(track_time)}**")

	@cmds.command(aliases=["s"])
	async def skip(self, ctx, index: str = None):
		"""
		Skips a (or several) track(s).
		"""

		def group_to_range(group):
			group = ''.join(group.split())
			sign, g = ('-', group[1:]) if group.startswith('-') else ('', group)
			r = g.split('-', 1)
			r[0] = sign + r[0]
			r = sorted(int(__) for __ in r)
			return range(r[0], 1 + r[-1])

		def rangeexpand(txt):
			ranges = chain.from_iterable(group_to_range(__) for __ in txt.split(','))
			return sorted(set(ranges))

		player = self.bot.lavalink.players.get(ctx.guild.id)
		m = ctx.message.content

		if not player.is_playing:
			return await ctx.send("Not playing.")
		elif m.startswith("/skip"):
			args = m.split("/skip ")
		else:
			args = m.split("/s ")

		if len(args) == 1:
			if ctx.author.id == int(player.current.requester) or is_dj(ctx):
				await ctx.send("⏭ | Skipped.")
				await player.skip()
			else:
				await ctx.send("You can't skip another person's song.")
		else:
			if is_dj(ctx):
				param = str(args[1:])[2:-2]
				nbs = rangeexpand(param)
				qlen = len(player.queue)
				for nb in nbs:
					if nb > qlen or nb < 1:
						raise cmds.BadArgument
				#nbs.reverse()
				while len(nbs) > 0:
					#print(nb)
					current_ind = nbs.pop(0)
					i = 0
					for n in nbs:
						nbs[i] -= 1
						i += 1
					skipped = player.queue.pop(current_ind - 1)
					name = f"`{ctx.author.name}#{ctx.author.discriminator}`"
					req = ctx.channel.guild.get_member(skipped.requester).mention
					await ctx.send(f"{name} skipped song `{skipped.title}` requested by {req}")
			else:
				raise cmds.CheckFailure

	@cmds.command()
	async def stop(self, ctx):
		"""Stops the player and clears its queue."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.is_playing:
			return await ctx.send("Not playing.")

		player.queue.clear()
		await self.delete_np(ctx)
		await player.stop()
		await ctx.send("⏹ | Stopped.")

	def create_embed(self, player, pos=False):
		position = (lavalink.utils.format_time(player.position), "00:00:00")[pos == True]
		duration = (lavalink.utils.format_time(player.current.duration), "🔴 LIVE")[player.current.stream]
		song = f"**[{player.current.title}]({player.current.uri})**\n"

		yt = "https://youtube.com"
		requ = self.bot.get_user(player.current.requester).mention
		id = player.current.uri.split("?v=")[1]
		r = requests.get(player.current.uri)
		soup = BeautifulSoup(r.text, "html.parser")
		matches = soup.find("a", href=re.compile("channel"))
		em = (dis.Embed(color=dis.Color.blurple(),
							title="Now Playing", description=song)
				.add_field(name="Requested by", value=requ)
				.add_field(name="Uploader", value=f"[{player.current.author}]({yt}{matches.attrs['href']})")
				.add_field(name="Duration", value=f"{position}/{duration}")
				.set_thumbnail(url=f"https://img.youtube.com/vi/{id}/0.jpg"))
		return em

	@cmds.command(aliases=["np", "n", "playing"])
	async def now(self, ctx):
		"""Shows some stats about the currently playing song."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.current:
			return await ctx.send("Nothing playing.")

		async with ctx.channel.typing():
			em = self.create_embed(player, False)
		await ctx.send(embed=em)

	@cmds.command(aliases=["q"])
	async def queue(self, ctx, page: int = 1):
		"""Shows the player's queue."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.queue:
			return await ctx.send("Nothing queued.")

		items_per_page = 10
		pages = math.ceil(len(player.queue) / items_per_page)

		start = (page - 1) * items_per_page
		end = start + items_per_page

		queue_list = ""
		for index, track in enumerate(player.queue[start:end], start=start):
			queue_list += f"`{index + 1}.` [**{track.title}**]({track.uri})\n"

		embed = dis.Embed(colour=dis.Color.blurple(),
						  description=f"**{len(player.queue)} tracks**\n\n{queue_list}")
		embed.set_footer(text=f"Viewing page {page}/{pages}")
		await ctx.send(embed=embed)

	@cmds.command(aliases=["res"])
	async def resume(self, ctx):
		"""Resumes the current track."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.is_playing:
			return await ctx.send("Not playing.")
		if player.paused:
			await player.set_pause(False)
			await ctx.send("⏯ | Resumed")
		else:
			await ctx.send("Song is already playing.")

	@cmds.command()
	async def pause(self, ctx):
		"""Pauses the current track. """
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.is_playing:
			return await ctx.send("Not playing.")
		if not player.paused:
			await player.set_pause(True)
			await ctx.send("⏯ | Paused")
		else:
			await ctx.send("Song is already paused.")

	@cmds.check(is_dj)
	@cmds.command(aliases=["vol"])
	async def volume(self, ctx, volume: int = None):
		"""Changes the player's volume (1-100)."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not volume:
			return await ctx.send(f"🔈 | {player.volume}%")

		if volume < 1 or volume > 100:
			return await ctx.send(f"Invalid {volume} volume percentage, choose 1% to 100%.")
		await player.set_volume(volume)  # Lavalink will automatically cap values between, or equal to 0-1000.
		await ctx.send(f"🔈 | Set to {player.volume}%")

	@cmds.check(is_dj)
	@cmds.command()
	async def shuffle(self, ctx):
		"""Shuffles the player's queue."""
		player = self.bot.lavalink.players.get(ctx.guild.id)
		if not player.is_playing:
			return await ctx.send("Nothing playing.")

		player.shuffle = not player.shuffle
		await ctx.send(f"🔀 | Shuffle {('disabled', 'enabled')[player.shuffle]}.")

	@cmds.check(is_dj)
	@cmds.command(aliases=["loop"])
	async def repeat(self, ctx):
		"""Repeats the current song until the command is invoked again."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.is_playing:
			return await ctx.send("Nothing playing.")

		player.repeat = not player.repeat
		await ctx.send(f"🔁 | Repeat {('disabled', 'enabled')[player.repeat]}.")

	'''
	@cmds.check(is_dj)
	@cmds.command()
	async def remove(self, ctx, index: int):
		"""Removes an item from the player's queue with the given index."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.queue:
			return await ctx.send("Nothing queued.")

		if index > len(player.queue) or index < 1:
			return await ctx.send(f"Index has to be **between** 1 and {len(player.queue)}")

		removed = player.queue.pop(index - 1)  # Account for 0-index.

		await ctx.send(f"Removed **{removed.title}** from the queue.") '''

	@cmds.command()
	async def find(self, ctx, *, query):
		"""Lists the first 10 search results from a given query."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not query.startswith("ytsearch:") and not query.startswith("scsearch:"):
			query = "ytsearch:" + query

		results = await player.node.get_tracks(query)

		if not results or not results["tracks"]:
			return await ctx.send("Nothing found.")

		tracks = results["tracks"][:10]  # First 10 results

		o = ""
		for index, track in enumerate(tracks, start=1):
			track_title = track["info"]["title"]
			track_uri = track["info"]["uri"]
			o += f"`{index}.` [{track_title}]({track_uri})\n"

		embed = dis.Embed(color=dis.Color.blurple(), description=o)
		await ctx.send(embed=embed)

	@cmds.check(is_dj)
	@cmds.command(aliases=["dc", "dis"])
	async def disconnect(self, ctx):
		"""Disconnects the player from the voice channel and clears its queue."""
		player = self.bot.lavalink.players.get(ctx.guild.id)

		if not player.is_connected:
			return await ctx.send("Not connected.")

		if not ctx.author.voice or (player.is_connected and ctx.author.voice.channel.id != int(player.channel_id)):
			return await ctx.send("You're not in my voicechannel!")

		player.queue.clear()
		await self.delete_np(ctx)
		await player.stop()
		await self.connect_to(ctx.guild.id, None)
		await ctx.send("*⃣ | Disconnected.")

	async def ensure_voice(self, ctx):
		"""This check ensures that the bot and command author are in the same voicechannel."""
		player = self.bot.lavalink.players.create(ctx.guild.id, endpoint=str(ctx.guild.region))
		# Create returns a player if one exists, otherwise creates.

		should_connect = ctx.command.name in ("play")  # Add cmds that require joining voice to work.

		if not ctx.author.voice or not ctx.author.voice.channel:
			raise cmds.CommandInvokeError("Join a voicechannel first.")

		if not player.is_connected:
			if not should_connect:
				raise cmds.CommandInvokeError("Not connected.")

			permissions = ctx.author.voice.channel.permissions_for(ctx.me)

			if not permissions.connect or not permissions.speak:  # Check user limit too?
				raise cmds.CommandInvokeError("I need the `CONNECT` and `SPEAK` permissions.")

			player.store("channel", ctx.channel.id)
			await self.connect_to(ctx.guild.id, str(ctx.author.voice.channel.id))
		else:
			if int(player.channel_id) != ctx.author.voice.channel.id:
				raise cmds.CommandInvokeError("You need to be in my voicechannel.")


def setup(bot):
	bot.add_cog(Music(bot))
