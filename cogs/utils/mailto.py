from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from cogs.utils.api import MyAPIs

import smtplib


def mailto(email_destination: str, message: str, subject: str = "[ No subject ]"):
	apis = MyAPIs(None).get_keys()
	msg = MIMEMultipart()

	msg["From"] = "bot.botanical@gmail.com"
	msg["To"] = email_destination

	msg["Subject"] = subject
	message = message
	msg.attach(MIMEText(message))

	mailserver = smtplib.SMTP("smtp.gmail.com", 587)
	mailserver.ehlo()
	mailserver.starttls()
	mailserver.ehlo()

	mailserver.login("bot.botanical@gmail.com", apis["gmail"])
	mailserver.sendmail("bot.botanical@gmail.com", email_destination, msg.as_string())
	mailserver.quit()

