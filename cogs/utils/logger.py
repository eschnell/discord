from discord.ext		import commands as cmds
import logging
import os

class Logger:
	"""
	PARAMETERS
	==========

	module: usually `self` if called within a module\n

	// ###############################################\n
	0: debug		// lvl 0 and lvl 1 logging\n
	1: info			// are always logged to sys.stdout\n
	// ###############################################\n
	2: warning		// lvl 2+ are always logged to\n
	3: critical		// files\n
	4: fatal\n
	5: error\n
	"""

	def __init__(self, module):
		#print(f"\n\nprevw: {module}\ndir: {dir(module)}\ntype: {type(module)}\n\n")
		self.bot = module.bot
		self.__log = None
		# Set attrs
		self.__path = module.logname
		self.__name = " - ".join((module.logname, module.__cog_name__))
		self.__debug = self.bot.debug
		self.__log = logging.getLogger(''.join(("botanical.", self.__name))) # `log` needed b4 `level`
		self.__level = None
		self.__level_obj = None
		# Create logging folders' tree structure
		if not os.path.isdir("logs"): os.makedirs("logs")
		if not os.path.isdir("logs/modules"): os.makedirs("logs/modules")
		# Create formater
		self.__m = "%(asctime)s | %(name)s | %(levelname)s  | %(message)s"
		self.__fmt = logging.Formatter(fmt=self.__m, datefmt="%Y-%m-%d %H:%M:%S")

	def __add_file_header(self):
		if not os.path.isfile(f"logs/modules/{self.__path}.log"):
			tmp_name = self.__log.name
			m = ''.join((
				'-' * 20,
				'|',
				'-' * (len(tmp_name) + 2),
				"|----------|",
				'-' * 34,
				"\n Date               |",
				" Path - Name",
				' ' * (len(tmp_name) - 10),
				"| LogLevel |",
				" Message",
				' ' * 26,
				"\n--------------------|",
				'-' * (len(tmp_name) + 2),
				"|----------|",
				'-' * 34,
				'\n'))
			with open(f"logs/modules/{self.__path}.log", "w") as fp:
				fp.write(m)


	def __add_file_handler(self):
		# do not append to log files if debug is on
		if self.__level > 0:
			f_handler = logging.FileHandler(f"logs/modules/{self.__path}.log")
			f_handler.setFormatter(self.__fmt)
			f_handler.setLevel(self.__level)
			self.__log.addHandler(f_handler)

	def __add_console_handler(self):
		# create handler
		c_handler = logging.StreamHandler()
		c_handler.setFormatter(self.__fmt)
		# check handler lvl
		if self.__debug:
			c_handler.setLevel(self.__level_obj)
		elif self.__level == 0 or self.__level == 1:
			c_handler.setLevel(self.__getLevel(1))
		elif self.__debug and self.__level > 1:
			c_handler.setLevel(self.__getLevel(self.__level))
		# add it
		self.__log.addHandler(c_handler)

	def __getLevel(self, lvl):
		if lvl == 1:
			w = 6
		elif lvl == 2:
			w = 3
		elif lvl == 3:
			w = 2
		else:
			w = 5
		self.__m = f"%(asctime)s | %(name)s | %(levelname)s{' '*w}| %(message)s"

		## Console
		if self.__debug:
			return logging.DEBUG
		## File
		elif lvl == 1 or lvl == 0:
			return logging.INFO
		elif lvl == 2:
			return logging.WARNING
		elif lvl == 3:
			return logging.CRITICAL
		elif lvl == 4:
			return logging.FATAL
		elif lvl == 5:
			return logging.ERROR
		else:
			raise cmds.errors.ArgumentParsingError(message=f"error: logging level {lvl} doesn't exist")

	def __setLevel(self, i: int):
		self._purge()
		self.__level = i
		self.__level_obj = self.__getLevel(i)
		if i == 4:
			self.__log.setLevel(logging.FATAL)
		else:
			self.__log.setLevel(self.__level_obj)
		self.__fmt = logging.Formatter(fmt=self.__m, datefmt="%Y-%m-%d %H:%M:%S")

	########################################
	########################################
	def _get_logger(self):
		return self.__log

	def _purge(self):
		self.__debug = self.bot.debug
		self.__log.propagate = False
		self.__add_file_header()
		if self.__log.hasHandlers(): self.__log.handlers.clear()

	## DEBUG
	def debug(self, text: str):
		self.__setLevel((1, 0)[self.__debug])

		self.__add_console_handler()
		self.__log.debug(text)

	def dbg(self, text: str):
		self.debug(text)


	## INFO
	def info(self, text: str):
		self.__setLevel(1)

		self.__add_console_handler()
		if self.__debug:
			self.__add_file_handler()
		self.__log.info(text)


	## WARNING
	def warning(self, text: str):
		self.__setLevel(2)

		if self.__debug:
			self.__add_console_handler()
		else:
			self.__add_file_handler()
		self.__log.warning(text)

	def warn(self, text: str):
		self.warning(text)


	## CRITICAL
	def critical(self, text: str):
		self.__setLevel(3)

		if self.__debug:
			self.__add_console_handler()
		else:
			self.__add_file_handler()
		self.__log.critical(text)

	def critic(self, text: str):
		self.critical(text)


	# FATAL
	def fatal(self, text: str):
		self.__setLevel(4)

		if self.__debug:
			self.__add_console_handler()
		else:
			self.__add_file_handler()
		self.__log.fatal(text)


	## ERROR
	def error(self, text: str, stack_info: bool = False, exc_info: bool = True):
		self.__setLevel(5)
		self.__add_console_handler()
		if not self.__debug:
			stack_info = True
			exc_info = True
			self.__add_file_handler()
		self.__log.error(text, stack_info=stack_info, exc_info=exc_info)

	########################################
	########################################
