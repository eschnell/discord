import hjson
import os


class MyAPIs(object):

	def __init__(self, bot):
		self.bot = bot

		if os.path.isfile("config/config_api.hjson"):
			with open("config/config_api.hjson") as fd:
				self.apis = hjson.load(fd)
		else:
			self.apis = None

	def get_keys(self):
		return self.apis
