import discord.ext.commands as cmds
import discord as dis

# Most OP check
def is_me(ctx):
	# Me or Flavian
	return ctx.message.author.id == 286562388461748228 #or ctx.message.author.id == 275118051501015040

# Inherits `is_me` check
def is_admin(ctx):
	chan = ctx.channel
	permissions = chan.permissions_for(ctx.author)
	return (is_me(ctx) or getattr(permissions, "administrator"))

# Inherits `is_admin` check
def is_dev(ctx):
	dev = False
	for r in ctx.message.author.roles:
		if r.name.lower() == "dev" or r.name.lower() == "developper":
			dev = True
	return (dev or is_admin(ctx))

# Inherits `is_dev` check
def is_mod(ctx):
	mod = False
	for r in ctx.message.author.roles:
		if r.name.lower() == "mod" or r.name.lower() == "moderator":
			mod = True
	return (mod or is_dev(ctx))

# Inherits `is_dev` check
def is_beta_tester(ctx):
	tester = False
	for r in ctx.message.author.roles:
		if r.name.lower() == "beta tester":
			tester = True
	return (tester or is_dev(ctx))

# Inherits `is_dev` check
def is_dj(ctx):
	dj = False
	for r in ctx.message.author.roles:
		if r.name.lower() == "dj":
			dj = True
	return (dj or is_dev(ctx))
