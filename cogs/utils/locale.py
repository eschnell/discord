from pymongo			import MongoClient

import gettext
import discord

def get_user_locale(filename: str, user: discord.Member):
	db = MongoClient("25.105.87.145", 27717).language
	userinfo = db.users.find_one({"user_id": user.id})
	lang_translations = gettext.translation(filename,
											localedir="locales",
											languages=[userinfo["locale"]])
	lang_translations.install()
	return lang_translations.gettext
