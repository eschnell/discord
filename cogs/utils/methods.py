from discord    import Emoji, Guild

def getEmojiByName(self, name: str):
    l = []
    for e in self.guild.emojis:
        l.append(str(e)[2:-1])
    for e in l:
        if e.startswith(name):
            id = e.split(':')[1]
    data = {
        "name": name,
        "id": int(id),
        "require_colons": True,
        "animated": False,
        "managed": False,
        "guild_id": self.guild.id
    }
    e = Emoji(guild=self.guild, state=None, data=data)
    return e

def getEmojiByID(self, id: int):
    pass