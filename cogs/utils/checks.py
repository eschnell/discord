from cogs.utils.dataIO	import dataIO
from discord.ext		import commands as cmds
from pbwrap				import Pastebin
from urllib.parse		import quote as uriquote

import discord
import aiohttp
import json
import time
import git
import os



try:
	from lxml import etree
except ImportError:
	from bs4 import BeautifulSoup
from urllib.parse import parse_qs, quote_plus



#############################

def open_read(name: str):
	try:
		with open("config/{}.json".format(name), 'r') as f:
			return json.load(f)
	except Exception as e:
		print(e)


def load_config():
	return open_read("config")

def load_optional_config():
	return open_read("optional_config")

def load_moderation():
	return open_read("moderation")

def load_log_config():
	return open_read("log")


def has_passed(oldtime):
	if time.time() - 20.0 < oldtime:
		return False
	return time.time()


def set_status(bot):
	if bot.default_status == 'idle':
		return discord.Status.idle
	elif bot.default_status == 'dnd':
		return discord.Status.dnd
	else:
		return discord.Status.invisible


def user_post(key_users, user):
	if time.time() - float(key_users[user][0]) < float(key_users[user][1]):
		return False, [time.time(), key_users[user][1]]
	else:
		p = "config/log.json"
		log = dataIO.load_json(p)
		now = time.time()
		log["keyusers"][user] = [now, key_users[user][1]]
		dataIO.save_json(p, log)
		return True, [now, key_users[user][1]]


def gc_clear(gc_time):
	if time.time() - 3600.0 < gc_time:
		return False
	return time.time()


def game_time_check(oldtime, interval):
	if time.time() - float(interval) < oldtime:
		return False
	return time.time()


def avatar_time_check(oldtime, interval):
	if time.time() - float(interval) < oldtime:
		return False
	return time.time()


def update_bot(message):
	g = git.cmd.Git(working_dir=os.getcwd())
	branch = g.execute(["git", "rev-parse", "--abbrev-ref", "HEAD"])
	g.execute(["git", "fetch", "origin", branch])
	update = g.execute(["git", "remote", "show", "origin"])
	if ("up to date" in update or "fast-forward" in update) and message:
		return False
	else:
		if message is False:
			version = 4
		else:
			version = g.execute(["git", "rev-list", "--right-only", "--count", "{0}...origin/{0}".format(branch)])
		version = description = str(int(version))
		if int(version) > 4:
			version = "4"
		commits = g.execute(["git", "rev-list", "--max-count={0}".format(version), "origin/{0}".format(branch)])
		commits = commits.split('\n')
		em = discord.Embed(color=0x24292E, title="Latest changes for the bot:", description="{0} release(s) behind.".format(description))
		for i in range(int(version)):
			i = i - 1  # Change i to i -1 to let the formatters below work
			title = g.execute(["git", "log", "--format=%ar", "-n", "1", commits[i]])
			field = g.execute(["git", "log", "-n1", "--pretty=format:%s", commits[i]])
			diff = g.execute(["git", "log", "-n1", "--stat", commits[i]]).split(field)[1].strip()
			link = "https://gitlab.com/eschnell/discord/commits/%s" % commits[i]
			em.add_field(name=title, value=f"[{field}]({link})\n{diff}", inline=False)
		base_proj_url = g.execute(["git", "config", "--get", "remote.origin.url"])
		if base_proj_url.find("github") != -1:
			em.set_thumbnail(url="https://image.flaticon.com/icons/png/512/25/25231.png")
		elif base_proj_url.find("gitlab") != -1:
			em.set_thumbnail(url="https://about.gitlab.com/images/press/logo/png/gitlab-logo-white-stacked-rgb.png")
		em.set_footer(text='Full project: https://gitlab.com/eschnell/discord')
		return em


def cmd_prefix_len():
	config = load_config()
	return len(config["cmd_prefix"])


def embed_perms(m):
	try:
		check = m.author.permissions_in(m.channel).embed_links
	except:
		check = True

	return check


def get_user(m, user):
	try:
		member = m.mentions[0]
	except:
		member = m.guild.get_member_named(user)
	if not member:
		try:
			member = m.guild.get_member(int(user))
		except ValueError:
			pass
	if not member:
		return None
	return member


def find_channel(channel_list, text):
	if text.isdigit():
		found_channel = discord.utils.get(channel_list, id=int(text))
	elif text.startswith("<#") and text.endswith(">"):
		found_channel = discord.utils.get(channel_list,
										  id=text.replace("<", "").replace(">", "").replace("#", ""))
	else:
		found_channel = discord.utils.get(channel_list, name=text)
	return found_channel


def attach_perms(m):
	return m.author.permissions_in(m.channel).attach_files


def parse_prefix(bot, text):
	prefix = bot.cmd_prefix
	if type(prefix) is list:
		prefix = prefix[0]
	return text.replace("[c]", prefix).replace("[b]", bot.bot_prefix)


async def pbin(ctx, key):
	if isinstance(ctx, str):
		m = ctx
	else:
		m = ctx.message.content

	if m.startswith("/pastebin") or m.startswith("/pb") or m.startswith("/pbin"):
		m = m.split(" ", 1)[1]
	if m == "":
		return 0

	pastebin = Pastebin(api_dev_key=key)
	resp = pastebin.create_paste(m.encode("utf-8"), api_paste_expire_date="1D")

	if resp.startswith("https"):
		#print(resp)
		return "https://pastebin.com/raw/" + resp[21:]
	else:
		return resp
