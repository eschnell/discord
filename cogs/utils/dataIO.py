from random import randint
#from json import decoder, dump, load
from os import replace
from os.path import splitext
import json
import hjson

class DataIO():

	def save_json(self, filename, data):
		"""Atomically save a JSON file given a filename and a dictionary."""
		path, ext = splitext(filename)
		del(ext)
		tmp_file = "{}.{}.tmp".format(path, randint(1000, 9999))
		with open(tmp_file, 'a', encoding='utf-8') as f:
			json.dump(data, f, indent=4, sort_keys=True, separators=(',',' : '))
		try:
			with open(tmp_file, 'r', encoding='utf-8') as f:
				data = json.load(f)
		except json.decoder.JSONDecodeError:
			print("Attempted to write file {} but JSON "
								  "integrity check on tmp file has failed. "
								  "The original file is unaltered."
								  "".format(filename))
			return False
		except Exception as e:
			print('A issue has occured saving ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False

		replace(tmp_file, filename)
		return True

	def save_hjson(self, filename, data):
		"""Atomically save a HJSON file given a filename and a dictionary."""
		path, ext = splitext(filename)
		del(ext)
		tmp_file = "{}.{}.tmp".format(path, randint(1000, 9999))
		with open(tmp_file, 'a', encoding='utf-8') as f:
			hjson.dump(data, f, indent=4, sort_keys=True)
		try:
			with open(tmp_file, 'r', encoding='utf-8') as f:
				data = hjson.load(f)
		except json.decoder.JSONDecodeError:
			print("Attempted to write file {} but HJSON "
								  "integrity check on tmp file has failed. "
								  "The original file is unaltered."
								  "".format(filename))
			return False
		except Exception as e:
			print(f"A issue has occured saving {filename}.\n"
				  "Traceback:\n"
				  f"{str(e)} {e.args}")
			return False

		replace(tmp_file, filename)
		return True

	def load_json(self, filename):
		"""Load a JSON file and return a dictionary."""
		try:
			with open(filename, 'r', encoding='utf-8') as f:
				data = json.load(f)
			return data
		except Exception as e:
			print('A issue has occured loading ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return {}

	def load_hjson(self, filename):
		"""Load a HJSON file and return a dictionary."""
		try:
			with open(filename, 'r', encoding='utf-8') as f:
				data = hjson.load(f)
			return data
		except Exception as e:
			print('A issue has occured loading ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return {}

	def append_json(self, filename, data):
		"""Append a value to a JSON file."""
		try:
			with open(filename, 'r', encoding='utf-8') as f:
				file = json.load(f)
		except Exception as e:
			print('A issue has occured loading ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False
		try:
			file.append(data)
		except Exception as e:
			print('A issue has occured updating ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False
		path, ext = splitext(filename)
		del(ext)
		tmp_file = "{}.{}.tmp".format(path, randint(1000, 9999))
		with open(tmp_file, 'w', encoding='utf-8') as f:
			json.dump(file, f, indent=4,sort_keys=True,separators=(',',' : '))
		try:
			with open(tmp_file, 'r', encoding='utf-8') as f:
				data = json.load(f)
		except json.decoder.JSONDecodeError:
			print("Attempted to write file {} but JSON "
								  "integrity check on tmp file has failed. "
								  "The original file is unaltered."
								  "".format(filename))
			return False
		except Exception as e:
			print('A issue has occured saving ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False
		replace(tmp_file, filename)
		return True

	def is_valid_json(self, filename):
		"""Verify that a JSON file exists and is readable. Take in a filename and return a boolean."""
		try:
			with open(filename, 'r', encoding='utf-8') as f:
				data = json.load(f)
			del(data)
			return True
		except (FileNotFoundError, json.decoder.JSONDecodeError):
			return False
		except Exception as e:
			print('A issue has occured validating ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False

	def is_valid_hjson(self, filename):
		"""Verify that a HJSON file exists and is readable. Take in a filename and return a boolean."""
		try:
			with open(filename, 'r', encoding='utf-8') as f:
				data = hjson.load(f)
			del(data)
			return True
		except (FileNotFoundError, hjson.decoder.HjsonDecodeError):
			return False
		except Exception as e:
			print('A issue has occured validating ' + filename + '.\n'
				  'Traceback:\n'
				  '{0} {1}'.format(str(e), e.args))
			return False

dataIO = DataIO()
