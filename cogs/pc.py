from discord.ext		import commands as cmds
from discord.utils		import find
#from pymongo			import MongoClient

from cogs.utils.perms	import *
#from cogs.utils.dataIO	import dataIO
#from cogs.utils.locale	import get_user_locale

import discord as dis
import re

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

class PrivateChannels(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		self.perm_text =		dis.PermissionOverwrite(read_messages=True,
														read_message_history=True,
														send_messages=True)
		self.perm_voice =		dis.PermissionOverwrite(connect=True,
														speak=True,
														use_voice_activation=True)
		self.perm_t_deny =		dis.PermissionOverwrite(read_messages=False,
														read_message_history=False)
		self.perm_v_deny =		dis.PermissionOverwrite(connect=False,
														speak=False,
														use_voice_activation=False,
														view_channel=False)


	@staticmethod
	async def check_ctg(g):
		pc_ctg = find(lambda c: c.name == "Private Channels", g.categories)
		if not pc_ctg:
			pc_ctg = await g.create_category("Private Channels")
		return pc_ctg

	@cmds.check(is_me)
	@cmds.command(aliases=["cpt"])
	async def _clear_private_text(self, ctx):
		pass

	@cmds.check(is_me)
	@cmds.command(aliases=["cpv"])
	async def _clear_private_voice(self, ctx):
		pass

	@cmds.command(aliases=["ptc"])
	async def private_text_channel(self, ctx, *users: dis.Member):
		# Apply perms
		p = { ctx.guild.default_role: self.perm_t_deny }
		for u in users:
			p[u] = self.perm_text
		# Check for similar-named chans
		try:
			i = 1
			while find(lambda c: c.name == f"private_text_{i:03d}", ctx.guild.text_channels):
				i += 1
		except:
			pass
		# Create category if not exist
		ctg = await self.check_ctg(ctx.guild)
		# Create the private channel
		ch = await ctx.guild.create_text_channel(
				f"private_text_{i:03d}",
				overwrites=p,
				category=ctg)
		await ctx.send(f"Private text channel: {ch.mention} created.")


	@cmds.command(aliases=["pvc"])
	async def private_voice_channel(self, ctx, *users: dis.Member):
		# Apply perms
		p = { ctx.guild.default_role: self.perm_v_deny }
		for u in users:
			p[u] = self.perm_voice
		# Check for similar-named chans
		try:
			i = 1
			while find(lambda c: c.name == f"private_voice_{i:03d}", ctx.guild.voice_channels):
				i += 1
		except:
			pass
		# Create category if not exist
		ctg = await self.check_ctg(ctx.guild)
		# Create the private channel
		ch = await ctx.guild.create_voice_channel(
				f"private_voice_{i:03d}",
				overwrites=p,
				category=ctg)
		await ctx.send(f"Private voice channel: {ch.mention} created.")



def setup(bot):
	bot.add_cog(PrivateChannels(bot))
