from discord.ext		import commands as cmds
from discord.utils		import find
#from pymongo			import MongoClient			# DB

from cogs.utils.perms	import *					# Perms
#from cogs.utils.logger	import Logger				# Logging class
#from cogs.utils.dataIO	import dataIO				# JSON & HJSON IO
#from cogs.utils.locale	import get_user_locale		# Locales

import discord as dis
import asyncio
import datetime

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

class DevTools(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot

		self.disabled_cmds = []
		self.blacklist_cmds = ["help", "bot"]

		## For Logger module
		#self.logname = __name__								# Needed by logger class
		#self.log = Logger(self)								# Logger obj

		## For DB socket
		#self.dbclient = MongoClient("25.105.87.145", 27717)	# IP and port
		#self.db = self.dbclient.___							# DB Table name


	def _load_cmds(self):
		# Relist user-commands
		[self.bot.add_command(c) for c in self.disabled_cmds]
		self.disabled_cmds = []

	def _unload_cmds(self):
		# Unlist user-commands
		for c in self.bot.commands:
			if c.name in self.blacklist_cmds: continue
			self.disabled_cmds.append(c)
			self.bot.remove_command(c.name)

	## Say
	@cmds.command()
	async def say(self, ctx, channel: dis.TextChannel, *, msg):
		await channel.send(msg)

	## MdSay
	@cmds.command(aliases=["esay", "emsay"])
	async def embed_say(self, ctx, channel: dis.TextChannel, *, msg):
		em = dis.Embed(title="Embed")
		em.add_field(name="Markdown", value=msg)
		await channel.send(embed=em)

	## List extensions (cogs)
	@cmds.command(name="cogs", aliases=["lc"])
	async def list_cogs(self, ctx, *args):
		"""Lists all loaded cogs."""
		cog_list = ()
		for c in self.bot.cogs:
			cog_list += ("* " + c,)
		await ctx.channel.send(''.join((
			"```asciidoc\n",
			"== List of cogs loaded: ==\n\n",
			'\n'.join(sorted(cog_list)),
			"```")))

	## Stats
	@cmds.check(is_dev)
	@cmds.command(aliases=["sts"])
	async def stats(self, ctx):
		em = dis.Embed(title="📊 Statistics 📊")
		em.set_footer(text=f"🕑 Date: {datetime.datetime.now().__format__('%A, %d. %B %Y @ %H:%M:%S')}")

		s = ""
		for g in self.bot.guilds:
			s += f"[{g.name}](https://discordapp.com/channels/{g.id}) -> ID: {g.id}\n"
		em.add_field(name="🌐 Guilds", value=s, inline=False)
		em.add_field(name="🔶 Shards",
					 value=f"Shard ID: {self.bot.shard_id}\nShard count: {self.bot.shard_count}",
					 inline=True)
		em.add_field(name="📩 Cached messages",
					 value=len(self.bot.cached_messages),
					 inline=True)
		await ctx.send(embed=em)

	# Emoji
	@cmds.check(is_dev)
	@cmds.command()
	async def emoji(self, ctx, emoji: str):
		try:
			stripped = emoji.split(':')
			if len(stripped) > 1:
				eid = int(stripped[2][:-1])
				#print(f"\n{type(eid)}", '\n', "eid:", eid, '\n\n')
				#emoji = dis.PartialEmoji(name=stripped[1], id=eid)
				emoji = self.bot.get_emoji(eid)
		except:
			pass
		
		if isinstance(emoji, dis.emoji.Emoji): s = "Custom"
		elif not emoji.startswith("<:"):
			raise cmds.errors.CommandInvokeError(f"Emoji `{emoji}` not found.")
		else: s = "Vanilla"
		s = f"{s} emoji: {emoji}"

		embed = dis.Embed(title=s)
		if isinstance(emoji, dis.emoji.Emoji):
			embed.add_field(name="id", value=repr(emoji.id))
			embed.add_field(name="name", value=repr(emoji.name))
		msg = await ctx.send(embed=embed)
		await msg.add_reaction(emoji)

	# bot commands
	@cmds.check(is_me)
	@cmds.group(name="bot", aliases=["b"])
	async def _bot(self, ctx):
		"""Bot Subcommands"""
		if ctx.invoked_subcommand is None:
			await ctx.send_help(ctx.command)

	@cmds.check(is_me)
	@_bot.command(aliases=["td"])
	async def toggle_debug(self, ctx):
		old_debug = self.bot.debug
		self.bot.debug = (True, False)[old_debug]

		if self.bot.debug:
			busy = dis.Status.dnd
			act = dis.Activity(name="eric code. In maintenance", type=dis.ActivityType.watching)
			await self.bot.change_presence(status=busy, activity=act)
			self._unload_cmds()
		else:
			await self.bot.change_presence(status=dis.Status.online, activity=None)
			self._load_cmds()

		await ctx.send("```md\n# Modules' logging debug_flag\n> is now :\n{}```".format(("+ On", "- Off")[old_debug]))

	@cmds.check(is_me)
	@_bot.command(aliases=["d", "dis"])
	async def disable(self, ctx, name: str):
		c = find(lambda cmd: name in cmd.aliases or cmd.name == name, self.bot.commands)
		if c is None:
			return await ctx.send(f"Cmd {name} not enabled.")

		bot_user = await ctx.guild.fetch_member(self.bot.bot_id)
		em = dis.Embed(color=bot_user.color, description=f"❌ Cmd {name} disabled!")
		del(bot_user)
		print(f"{c.aliases}\n{type(c.aliases)}")

		self.bot.remove_command(c.name)
		self.disabled_cmds.append((c.aliases, c))
		await ctx.send(embed=em)

	@cmds.check(is_me)
	@_bot.command(aliases=["e", "en"])
	async def enable(self, ctx, name: str):
		c = find(lambda cmd: name in cmd[0] or cmd[1].name == name, self.disabled_cmds)
		if c is None:
			return await ctx.send(f"Cmd {name} not disabled.")

		bot_user = await ctx.guild.fetch_member(self.bot.bot_id)
		em = dis.Embed(color=bot_user.color, description=f"✅ Cmd {name} enabled!")
		del(bot_user)
		self.bot.add_command(c[1])
		self.disabled_cmds.remove(c)
		await ctx.send(embed=em)






def setup(bot):
	bot.add_cog(DevTools(bot))
