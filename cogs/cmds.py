import discord as dis
import asyncio
import io
import json
import os
import requests
import subprocess
import sys
import traceback
#import aiohttp
import urllib.parse
import cogs.utils.api	as api


import requests
#import json
import random
import string


from discord.ext		import commands as cmds
from discord.utils		import get, find
from io					import StringIO


from cogs.utils.mailto	import mailto
from cogs.utils.perms	import *
from cogs.utils.dataIO	import dataIO as d
from cogs.utils.config	import get_config_value
from cogs.utils.checks	import pbin, load_optional_config, embed_perms, update_bot



class   Cmds(cmds.Cog):
	"""
	Miscellaneous commands.

	Really, just plenty cmds..
	"""

	def __init__(self, bot):
		self.bot = bot
		self.apis = self.bot.apis
		self.stream = io.StringIO()
		self.states = {}

	@cmds.check(is_dev)
	@cmds.command(aliases=["mail"])
	async def email(self, ctx, destination_email: str, message: str, subject: str = None):
		if "@" not in destination_email:
			return await ctx.send(":negative_squared_cross_mark:"
						" Invalid email `{}`".format(destination_email))
		mailto(destination_email, message, subject)
		if subject is None: subject = "[ No subject ]"
		await ctx.send((" Mail sent to `{}`"
						" as subject `{}`.").format(destination_email, subject))


	def _get_shortener_request_header(self, url: str):
		linkRequest = {
		"destination": url,
		"domain": { "fullName": "rebrand.ly" },
		"slashtag": f"{''.join(random.choices(string.ascii_uppercase + string.digits, k=5))}"
		# , "title": "Rebrandly YouTube channel"
		}
		requestHeaders = {
		"Content-type": "application/json",
		"apikey": self.apis["rebrandly"]
		#"workspace": "YOUR_WORKSPACE_ID"
		}
		r = requests.post("https://api.rebrandly.com/v1/links",
			data = json.dumps(linkRequest),
			headers=requestHeaders)
		del(linkRequest)
		del(requestHeaders)
		return r

	# Link shortener
	@cmds.command(aliases=["short"])
	async def shorten(self, ctx, link: str, title: str = None):
		if not (link.startswith("https://") or link.startswith("http://")):
			return await ctx.send("Not a valid link!")

		r = self._get_shortener_request_header(link)
		if (r.status_code == 200):
			link = r.json()
			return await ctx.send((
				"Long URL was **`{}`**\n"
				"Short URL is **`https://{}`**").format(
					link["destination"], link["shortUrl"]))
		await ctx.send("Error.")

	## Delete X msgs
	@cmds.command(name="prune", aliases=["purge", "prn"])
	@cmds.check(is_admin)
	async def prune(self, ctx, nb: str):
		if nb.isdigit():
			await ctx.channel.purge(limit=int(nb) + 1, bulk=True)
		else:
			await ctx.send("Please enter a number as parameter.")

	## Delete a certain msg
	@cmds.command(name="delete", aliases=["del"])
	@cmds.check(is_admin)
	async def delete(self, ctx, msg: dis.Message):
		try:
			await msg.delete()
		except:
			await ctx.send("Message with ID {} not found.".format(msg.id))

	## Delete X embeds
	@cmds.command(name="clear", aliases=["clr"])
	@cmds.check(is_admin)
	async def clear(self, ctx):
		"""Delete last 100 embed messages."""
		async def _clear(nb: int):
			try:
				async for msg in ctx.channel.history():
					if msg.embeds:
						await msg.delete()
						nb -= 1
					if nb == 0:
						return
			except Exception as e:
				await ctx.send(e)

		nb = ctx.message.content.split(" ")
		if len(nb) == 2:
			await _clear(int(nb[1]))
		elif len(nb) == 1:
			await _clear(100)
		else:
			raise cmds.BadArgument


	## Pastebin
	@cmds.command(aliases=["pb", "pbin"],
		brief="Upload online. Ex: /hb <text>")
	async def pastebin(self, ctx):
		"""#Upload text or code online. Ex: /pastebin <text_or_FormattedCode>"""
		await ctx.message.delete()
		hb_resp = await pbin(ctx, self.apis["pastebin"])
		await asyncio.sleep(3)
		await ctx.channel.send(hb_resp)


	## Selfrename command
	@cmds.command(brief="Rename yourself",
		description="Rename yourself with the desired nickname.\n** Must have [change_nickname] permission **",
		aliases=["srn"])
	@cmds.has_permissions(change_nickname=True)
	async def selfrename(self, ctx, *, nickname):
		"""Command to rename yourself. Ex: /selfrename "New Nickname"
		or /srn none
		to reset your nickname
		"""
		return print(nickname)
		c = ctx.channel
		try:
			if not nickname:
				raise cmds.MissingRequiredArgument
			else:
				for u in self.bot.get_all_members():
					if u.id == ctx.author.id:
						if nickname.lower() == "none":
							await u.edit(nick=None)
							return await c.send("User {} nickname has been reset!".format(u),
								delete_after=20)
						else:
							await u.edit(nick=nickname)
							return await c.send("You renamed yourself to {}.".format(nickname), delete_after=20)
		except Exception as e:
			await c.send(e)

	## Command Rename
	@cmds.check(is_admin)
	@cmds.command(aliases=["rn"], brief="Rename someone",
		description="Rename someone into a new nickname.\nMust have [manage_nickname] permission")
	async def rename(self, ctx, *args):
		c = ctx.channel
		await ctx.message.delete()
		#print(args)

		if args == None:
			raise cmds.BadArgument

		if len(args) < 2:
			raise cmds.BadArgument
		elif len(args) == 2:
			#print("check 1")
			try:
				snowf = args[0]
				nick = args[1]

				if len(nick) <= 32:
					#print("check 2")
					if snowf[0] == '<':
						if snowf[2] == '!':
							i = 1
						else:
							i = 0
						id = int(snowf[i+2:i+20])
					else:
						id = int(snowf)
					#print("id = %i\ncheck 3" % id)
					for u in self.bot.get_all_members():

						if u.id == id and u.guild == ctx.guild:
							#print(u.name+"#"+str(u.id))
							snowf = str(snowf)
							if nick.lower() == "none":
								await u.edit(nick=None)
								return await c.send("User {}#{} nickname has been reset!".format(u.name, u.discriminator))
							else:
								await u.edit(nick=nick)
								return await c.send("User {}#{} successfully renamed to {}.".format(u.name, u.discriminator, snowf))
					#print("check 4")
			except Exception as e:
				await c.send(e)
		else:
			raise cmds.errors.BadArgument


	@cmds.command(aliases=["mv"], hidden=True)
	@cmds.check(is_admin)
	async def move(self, ctx, user: int = None, channel: int = None):
		target = None
		c = self.bot.get_channel(channel)
		u = dis.utils.get(self.bot.get_all_members(), id=user)

		if c is None or u is None:
			return await ctx.send("Channel and/or User not found. Please verify IDs!")
		for g in self.bot.guilds:
			m = g.get_member(u.id)
			if m.voice is not None:
				target = m
				break
		if not target:
			return await ctx.send("User {}#{} isn't connected to voice!".format(u.name, u.discriminator))
		try:
			await target.move_to(c)
		except Exception as e:
			raise cmds.CommandInvokeError(e)


	@cmds.command(hidden=True)
	@cmds.check(is_me)
	async def roleadd(self, ctx, user: int, role: int):
		"""
		Parameters
		==========

		user: [id] of user
		role: [id] of role to give to user
		"""
		try:
			if not (user or role):
				raise cmds.MissingRequiredArgument

			user = ctx.channel.guild.get_member(user)
			if user == None:
				return await ctx.send("User {} not found".format(user))
			target_role = ctx.channel.guild.get_role(role)
			if target_role == None:
				return await ctx.send("Role {} not found".format(role))

			#print("BEFORE:\n{}\n".format(user.roles))
			l = user.roles
			l.append(target_role)
			#print("AFTER:\n{}\n".format(l))
			await user.edit(roles=l)
		except dis.HTTPException as e:
			await ctx.send(e)


	@cmds.command(hidden=True)
	@cmds.check(is_me)
	async def get(self, ctx, c: dis.TextChannel):
		"""
		Parameters
		==========

		channel: [id] target channel
		"""
		txt_file = c.name + '_' + str(c.id) + ".txt"
		with open("/tmp/" + txt_file, "w") as fd:
			async for m in c.history():
				try:
					if not m.author.bot:
						msg = m.author.name + ' ' + str(m.created_at)[:-7] + ":\n"
						if len(m.attachments) >= 1:
							i = 0
							while i < len(m.attachments):
								msg = msg + m.attachments[i].url
								i += 1
						else:
							msg += m.content
						msg += "\n\n"
						fd.write(msg)
						del(msg)
				except Exception as e:
					print(e)
		await ctx.send("Done processing. File saved as `/tmp/%s`" % txt_file, delete_after=5)


	@cmds.command(hidden=True, aliases=["lg", "leaveguild"])
	@cmds.check(is_me)
	async def leave_guild(self, ctx, gid: int):
		"""
		gid: Guild id only.
		"""
		try:
			g = self.bot.get_guild(gid)
			assert g is not None
			await g.leave()
			await ctx.send("Successfully left guild {}".format(g.name))
		except AssertionError:
			await ctx.send("Guild with ID {} not found.".format(gid))



	@cmds.command(hidden=True)
	@cmds.check(is_me)
	async def cat(self, ctx, file: str, min: int, max: int):
		if os.path.isfile(file):
			if (min > max):
				return await ctx.send("min `must be <=` max")
			if (min < 0 or max < 0):
				return await ctx.send("min and max `must be Null or positive integers`")

			lineIObuf = open(file, 'r')
			line = lineIObuf.readlines()[min:max]

			ret = "```python\n"
			for l in line:
				ret = ret + l
			lineIObuf.close()
			ret = ret + "```"
			await ctx.send(ret)
		else:
			await ctx.send("File {} not found.".format(file))


	@cmds.command(hidden=True, aliases=["pp"])
	@cmds.check(is_mod)
	async def print_permission(self, ctx, u: dis.Member):

		def check_bool(predicate):
			return ("❌ False", "✅ True")[predicate]

		def add(embed: dis.Embed, name: str, perms: dis.Permissions, inline: bool):
			embed.add_field(name=name, value=check_bool(perms), inline=inline)


		p = u.guild_permissions

		em = dis.Embed(title="Permissions for {}".format(u), colour=u.color)

		add(em, "admin", p.administrator, True)
		add(em, "view audit log", p.view_audit_log, True)
		add(em, "kick", p.kick_members, True)
		add(em, "ban", p.ban_members, True)
		add(em, "move members", p.move_members, True)
		add(em, "mute members", p.mute_members, True)
		add(em, "deafen members", p.deafen_members, True)

		add(em, "manage guild", p.manage_guild, True)
		add(em, "manage webhooks", p.manage_webhooks, True)
		add(em, "manage emojis", p.manage_emojis, True)
		add(em, "manage roles/permissions", p.manage_roles, True)
		add(em, "manage channels", p.manage_channels, True)
		add(em, "manage messages", p.manage_messages, True)
		add(em, "manage nicknames", p.manage_nicknames, True)
		add(em, "create invite", p.create_instant_invite, True)
		add(em, "add reactions", p.add_reactions, True)
		add(em, "priority speaker", p.priority_speaker, True)
		add(em, "stream", p.stream, True)
		add(em, "read messages/view channels", p.read_messages, True)
		add(em, "send messages", p.send_messages, True)
		add(em, "send tts messages", p.send_tts_messages, True)
		add(em, "embed links", p.embed_links, True)
		add(em, "attach files", p.attach_files, True)
		add(em, "read message history", p.read_message_history, True)
		add(em, "mention everyone", p.mention_everyone, True)
		add(em, "external emojis", p.external_emojis, True)
		add(em, "use external emojis", p.use_external_emojis, True)
		add(em, "view guild insights", p.view_guild_insights, True)
		add(em, "connect", p.connect, True)
		add(em, "speak", p.speak, True)
		add(em, "use voice activation", p.use_voice_activation, True)
		add(em, "change nickname", p.change_nickname, True)

		em.add_field(name="Permissions Bits", value=p.value, inline=False)
		await ctx.send(embed=em, delete_after=60)

	@cmds.command(no_pm=True)
	async def ping(self, ctx, ip_or_host: str = None):
		if ip_or_host is None:
			em = dis.Embed(title="Pong! :ping_pong:",
				description="{} ms".format(round(self.bot.latency, 2)))
		else:
			ping_response = subprocess.Popen(["/bin/ping", "-c1", "-w100", ip_or_host], stdout=subprocess.PIPE).stdout.read()
			em = dis.Embed(title="Pong! :ping_pong:",
				description="{}'s ping:\n{} ms".format(ip_or_host.lower(), str(ping_response).split("time=")[1].split(' ')[0]))
		await ctx.send(content=None, embed=em)

	@cmds.command(aliases=["cib", "invitebot"])
	async def create_invite_bot(self, ctx):
		uri = (
			"To invite this bot to your discord server, use this __**magical**__ link:\n"
			f"`https://discordapp.com/oauth2/authorize?client_id={self.bot.bot_id}&permissions=8&scope=bot`")
		await ctx.send(uri)

	@cmds.command(aliases=["ci", "link", "invite"])
	async def create_invite(self, ctx, time = 5, uses = 0):
		"""
		Parameters
		==========

		time: [int]	time in mins until link expires
					(defaults to 5 mins)
		uses: [int]	number of max uses for this link
					(defaults to ∞ uses)
		"""
		try:
			url = await ctx.channel.create_invite(
				max_age=time*60,
				max_uses=uses,
				reason=f"User {ctx.author} used /ci command")
			await ctx.send("https://discord.gg/{}".format(url.code))
		except Exception as e:
			raise cmds.CommandInvokeError(e)



def setup(bot):
	bot.add_cog(Cmds(bot))
