from discord.ext    import commands as cmds


class MyHelpCommand(cmds.MinimalHelpCommand):

	""" def __init__(self):
		self._command_impl = None
		self.command_attrs=dict(pass_context=True)
		self.no_category = "None" """


	def get_command_signature(self, command):
		if command.help is not None:
			command.help = command.help.replace("[p]", self.clean_prefix)
		return '{0.clean_prefix}{1.qualified_name} {1.signature}'.format(self, command)


class Help(cmds.Cog):
	"""Formats help for commands."""

	def __init__(self, bot):
		self._original_help_command = bot.help_command
		self.bot = bot
		self.case_insensitive = True
		self.bot.remove_command("help")
		self.bot.help_command = MyHelpCommand() #no_category = "None")
		self.bot.help_command.cog = self

	def cog_unload(self):
		self.bot.help_command = self._original_help_command

	""" @cmds.command(pass_context=True)
	async def help(self, ctx):
		pass """

def setup(bot):
	bot.add_cog(Help(bot))
