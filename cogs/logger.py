from discord.ext		import commands as cmds

from cogs.utils.perms	import *

import discord	as dis
import asyncio
import logging
import os
import time		as time

class Logger(cmds.Cog):

	def __init__(self, bot):
		self.bot = bot

	def _trim_at(self, m):
		try:
			while m.content.find("<@") != -1:
				ind = m.content.find("<@")
				i = (0, 1)[m.content[ind+2] == '&']
				tag = m.content[ind:ind+i+21]		# @<.......> or @<&......>
				t_id = (tag[3:-1], tag[2:-1])[i == 0]
				target = (m.guild.get_role(int(t_id)), self.bot.get_user(int(t_id)))[i == 0]
				name = target.name
				if i == 0:
					name += '#' + target.discriminator
				m.content = m.content.replace(tag, ('@' + name))
		except:
			pass
		return m.content

	# Return max size (in kB) for file uploads
	# in discord guild respectively with guild tier
	def _get_max_size(self, tier: int):
		if tier == 0 or tier == 1:
			return 7500
		elif tier == 2:
			return 45000
		elif tier == 3:
			return 95000

	# Check if message was not sent by bot,
	# then processes and logs it.
	@cmds.Cog.listener()
	async def on_message(self, m):
		i = 1
		if not os.path.exists("logs"): os.makedirs("logs")
		if m.author.id != self.bot.user.id:
			try:
				gnm, gid, cnm, cid = m.guild.name, m.guild.id, m.channel.name, m.channel.id
				gnm_nowhitespace = gnm.replace(" ", "_")
				name = "logs/{}.{}.discord_{}.log".format(gnm_nowhitespace, gid, "{0:0=4d}".format(i))

				while os.path.isfile(name) and os.path.getsize(name) >= self._get_max_size(m.guild.premium_tier) * 1000:
					# while file (exists and exceeds max_allowed_size), increment `i`
					i += 1
					name = "logs/{}.{}.discord_{}.log".format(gnm_nowhitespace, gid, "{0:0=4d}".format(i))
				l = ("", "\n")[os.path.isfile(name)]
				with open(name, "a", encoding="utf-8") as fd:
					lct = time.localtime(time.time())
					l += time.strftime("||%d/%m/%Y @%H:%M:%S||", lct) + "\n"
					l += "Guild name & ID:	|{}| - |{}|\n".format(gnm, gid)
					l += "Channel name & ID:	|{}| - |{}|\n".format(cnm, cid)
					l += "Author:			|{}|\n".format(m.author)
					l += "Message ID:		|{}|\n".format(m.id)
					l += "Message: |{}|\n".format(self._trim_at(m))
					fd.write(l)
					fd.close()
			except: # Exception as e:
				name = "logs/@dms.discord.log"
				l = ("", "\n")[os.path.isfile(name)]
				with open(name, "a", encoding="utf-8") as fd:
					lct = time.localtime(time.time())
					l += time.strftime("||%d/%m/%Y @%H:%M:%S||", lct) + "\n"
					l += "Author:			|{}|\n".format(m.author)
					l += "Message ID:		|{}|\n".format(m.id)
					l += "Message: |{}|\n".format(self._trim_at(m))
					fd.write(l)
					fd.close()
				#print(e)


	@cmds.check(is_mod)
	@cmds.command(no_pm=True, aliases=["log"])
	async def logs(self, ctx, g: dis.Guild = None): #implementation to later see other guild's log
		partial_name = f"{ctx.guild.name.replace(' ', '_')}.{ctx.guild.id}.discord_"
		i = 0

		for f in os.listdir('logs'):
			if f.startswith(partial_name) and f.endswith(".log"):
				i += 1

		await ctx.send(f"{i} log file(s) found.", delete_after=60)
		async with ctx.channel.typing():
			for f in sorted(os.listdir("logs")):
				await asyncio.sleep(1)
				if f.startswith(partial_name) and f.endswith(".log"):
					await ctx.send(
						file=dis.File(open(f"logs/{f}", "rb"), filename=f"{f}.txt"),
						delete_after=60)





def setup(bot):
	bot.add_cog(Logger(bot))
