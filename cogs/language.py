from discord.ext		import commands as cmds
from pymongo			import MongoClient

import discord as dis


class Language(cmds.Cog):
	"""
	Module for
	changing / setting locale.
	"""

	def __init__(self, bot):
		self.bot = bot
		self.dbclient = MongoClient("25.105.87.145", 27717)
		self.db = self.dbclient.language
		self.locales = ["en", "fr", "pt"]

	@cmds.Cog.listener()
	async def on_message(self, message):
		if message.author.bot:
			return
		user = message.author
		userinfo = self.db.users.find_one({"user_id": user.id})
		if not userinfo:
			new_account = {
				"user_id" : user.id,
				"locale": "en"
			}
			self.db.users.insert_one(new_account)


	@cmds.command(no_pm=True, aliases=["language", "lang"])
	async def languages(self, ctx):
		maxlen, i = len(self.locales), 1
		msg = "Set your desired language with.\n"
		msg += f"Example: **`{self.bot.prefix}langset en`**```"
		msg += "Available languages:\n\n"
		for l in self.locales:
			msg += (l, f"{l}, ")[i != maxlen]
			i += 1
		msg += "```"

		await ctx.send(msg)

	@cmds.command(no_pm=True)
	async def langset(self, ctx, language: str):
		loc = language.lower()
		if loc not in self.locales:
			return await ctx.send(f"`{loc}` language doesn't exist.")

		self.db.users.update_one({"user_id": ctx.message.author.id},
			{"$set": {"locale": loc}
		})
		await ctx.send(f"Updated to `{loc}` language.")





def setup(bot):
	bot.add_cog(Language(bot))
