from discord.ext		import commands as cmds
from discord.utils		import find
from bs4				import BeautifulSoup

from cogs.utils.perms	import *


import discord as dis
import asyncio
import feedparser
import requests


from pprint import pprint

class News(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		self.loaded = True
		self.bg_task = self.bot.loop.create_task(self.test())

	def cog_unload(self):
		self.loaded = False

	def channel_finder(self):
		# for each guild
		for g in self.bot.guilds:
			# find a "news" channel
			for tc in g.text_channels:
				if "news" in tc.name.lower():
					yield tc

	def get_first_img(self, url: str):
	    r = requests.get(url)
	    if r.status_code != 200:
	        return None
	    soup = BeautifulSoup(r.content, 'html.parser')
	    imgTags = soup.findAll('img')
	    for imgTag in imgTags:
	        imgUrl = imgTag['src']
	        if "https://a1" not in imgUrl:
	            return imgUrl
	    return None


	#@cmds.check(is_me)
	#@cmds.command()
	async def test(self):
		i = 0

		await self.bot.wait_until_ready()
		while not self.bot.is_closed() and self.loaded:
			chans = self.channel_finder()

			for c in chans:
				d = feedparser.parse("http://feeds.bbci.co.uk/news/rss.xml")
				# BFMTV
				#"https://www.bfmtv.com/rss/info/flux-rss/flux-toutes-les-actualites/")
				# BBC
				#"http://feeds.bbci.co.uk/news/rss.xml"
				d_chronologic = sorted(d["entries"], key=lambda k: k["updated"], reverse=True)
				print(len(d_chronologic))
				for entry in d_chronologic:
					# Check if not already posted
					try:
						#try:
						if not await c.history(limit=200).find(lambda m: len(m.embeds) > 0 and entry['title'] in m.embeds[0].description):
							print(entry["title"])
							title = f"[{entry['title']}]({entry['link']})\n\n{entry['summary']}"
							em = dis.Embed(title=entry["published"], description=title)
							img_url = self.get_first_img(entry["link"])
							if img_url and img_url.startswith("https://"):
								em.set_thumbnail(url=img_url)

							await c.send(embed=em)
						#except:
						#	await genr.next()
					except:
						continue
					#print(f"{entry['updated']}\n{entry['title']}\n'{entry['summary']}\n\n")
					#break
			#print(f"i = {i}")
			#i += 1

			# 15 mins #900s
			await asyncio.sleep(10)





def setup(bot):
	bot.add_cog(News(bot))
