"""
Please understand Music bots are complex, and that even this basic example can be daunting to a beginner.
For this reason it's highly advised you familiarize yourself with discord.py, python and asyncio, BEFORE
you attempt to write a music bot.
This example makes use of: Python 3.6
For a more basic voice example please read:
	https://github.com/Rapptz/discord.py/blob/rewrite/examples/basic_voice.py
"""
import discord
import asyncio
import html
import itertools
import json
import sys
import random
import requests
import time
import traceback
import youtube_dl

from discord.ext		import commands as cmds
from async_timeout		import timeout
from functools			import partial
from itertools			import chain
from bs4				import BeautifulSoup
from random				import random, shuffle
from cogs.utils.checks	import *


bot = cmds.Bot(command_prefix=("/"))

class MyLogger(object):
	def debug(self, msg):
		pass

	def warning(self, msg):
		pass

	def error(self, msg):
		print(msg)
		#pass

# Silence ffmpeg's output
def my_hook(d):
	pass

ytdlopts = {
	'extractaudio': True,
	'format': 'bestaudio/best',
	'logger': MyLogger(),
	'progress_hooks': [my_hook],
	'outtmpl': 'downloads/%(extractor)s-%(id)s-%(title)s.%(ext)s',
	'restrictfilenames': True,
	'noplaylist': True,
	'nocheckcertificate': True,
	'ignoreerrors': False,
	'logtostderr': False,
	'quiet': True,
	'no_warnings': True,
	'default_search': 'auto',
	'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
}

ffmpegopts = {
	'before_options': ' -nostdin -reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
	'options': '-v'
}

ytdl = youtube_dl.YoutubeDL(ytdlopts)


class VoiceConnectionError(cmds.CommandError):
	"""Custom Exception class for connection errors."""


class InvalidVoiceChannel(VoiceConnectionError):
	"""Exception for cases of invalid Voice Channels."""


class CommandInvokeError(youtube_dl.utils.DownloadError):
	"""Exeption for download errors"""
	async def something(self, ctx):
		ctx.send("This video was not available.")

def get_title(data):
	r = requests.get(data['webpage_url'])
	try:
		if r.status_code == 200:
			soup = BeautifulSoup(r.content, "html.parser")
			elems = str(soup.find_all("h1", class_="watch-title-container"))
			ret = html.unescape(elems.split(" title=\"")[1].split("\">")[0])
			# to be deprecated
			del(r)
			del(soup)
			del(elems)
			#print(ret)
			time.sleep(1)
	except Exception as e:
		print(e)
		ret = " "
	return (ret)

class YTDLSource(discord.PCMVolumeTransformer):


	def __init__(self, source, *, data, requester):
		super().__init__(source)
		self.requester = requester
		self.title = get_title(data)
		self.web_url = data.get('webpage_url')
		#print(json.dumps(data, indent=2, sort_keys=True))
		# YTDL info dicts (data) have other useful information you might want
		# https://github.com/rg3/youtube-dl/blob/master/README.md

	def __getitem__(self, item: str):
		"""Allows us to access attributes similar to a dict.
		This is only useful when you are NOT downloading.
		"""
		return self.__getattribute__(item)

	@classmethod
	async def create_source(cls, ctx, search: str, *, loop, download: bool):
		loop = loop or asyncio.get_event_loop()
		to_run = partial(ytdl.extract_info, url=search, download=download)
		data = await loop.run_in_executor(None, to_run)

		if 'entries' in data:
			# take first item from a playlist
			if len(data['entries']) == 1:
				data = data['entries'][0]
			else:
				data = data['entries']

		if download:
			source = ytdl.prepare_filename(data)
			return (cls(discord.FFmpegPCMAudio(source), data=data, requester=ctx.author))
		else:
			return {'webpage_url': data['webpage_url'], 'requester': ctx.author, 'title': get_title(data)}

		await ctx.send(f"```ini\n[Added {data['title']} to the Queue.]\n```", delete_after=2)
		return cls(discord.FFmpegPCMAudio(source, before_options=ffmpegopts["before_options"]),
			data=data, requester=ctx.author)

	@classmethod
	async def regather_stream(cls, data, *, loop):
		"""Used for preparing a stream, instead of downloading.
		Since Youtube Streaming links expire."""
		loop = loop or asyncio.get_event_loop()
		requester = data['requester']

		to_run = partial(ytdl.extract_info, url=data['webpage_url'], download=False)
		data = await loop.run_in_executor(None, to_run)

		return cls(discord.FFmpegPCMAudio(data['url'], before_options=ffmpegopts["before_options"]),
			data=data, requester=requester)


class MusicPlayer:
	"""A class which is assigned to each guild using the bot for Music.
	This class implements a queue and loop, which allows for different guilds to listen to different playlists
	simultaneously.
	When the bot disconnects from the Voice it's instance will be destroyed.
	"""

	__slots__ = ('bot', '_guild', '_channel', '_cog', 'current', 'empty', 'failed',
					'loop', 'next', 'np', 'played', 'queue', 'shuffle', 'volume')

	def __init__(self, ctx):
		self.bot = ctx.bot
		self._guild = ctx.guild
		self._channel = ctx.channel
		self._cog = ctx.cog

		self.queue = asyncio.Queue()
		self.next = asyncio.Event()

		self.np = ""
		self.loop = False
		self.shuffle = False
		self.volume = .12
		self.empty = None
		self.current = None
		self.played = 0
		self.failed = 0

		ctx.bot.loop.create_task(self.player_loop())

	async def is_empty(self):
		"""Check if queue is empty"""
		if not self.queue.empty():
			self.empty = False
		else:
			self.empty = True
		return self.empty

	async def player_loop(self):
		"""Our main player loop."""
		await self.bot.wait_until_ready()
		while not self.bot.is_closed():
			self.next.clear()

			try:
				# Wait for the next song. If we timeout cancel the player and disconnect...
				async with timeout(300):  # 5 minutes...
					source = await self.queue.get()
					if self.loop:
						await self.queue.put(source)

			except asyncio.TimeoutError:
				return self.destroy(self._guild)

			if not isinstance(source, YTDLSource):
				# Source was probably a stream (not downloaded)
				# So we should regather to prevent stream expiration
				try:
					source = await YTDLSource.regather_stream(source, loop=self.bot.loop)
				except Exception as e:
					await self._channel.send("There was an error processing your song.\n```css\n[%s]\n```" % e)
					continue

			if self.shuffle:
				shuffle(self.queue._queue, random=random)
			self.empty = await self.is_empty()
			source.volume = self.volume
			self.current = source
			try:
				self.played += 1
				self._guild.voice_client.play(source, after=lambda _: self.bot.loop.call_soon_threadsafe(self.next.set))
			except Exception as e:
				self.failed += 1
				print(e)
				await self._channel.send(e)

			self.np = await self._channel.send("**Now Playing:** `{}` requested by `{}`".format(source.title, source.requester))
			await self.next.wait()

			# Make sure the FFmpeg process is cleaned up.
			source.cleanup()
			self.current = None

			try:
				# We are no longer playing this song...
				await self.np.delete()
				#self.played += 1
			except discord.HTTPException as e:
				await self._channel.send(e)
				continue

	def destroy(self, guild):
		"""Disconnect and cleanup the player."""
		return self.bot.loop.create_task(self._cog.cleanup(guild))


class Music(cmds.Cog):
	"""Music related commands."""

	__slots__ = ("bot", "players", "_shuffle", "_loop")

	def __init__(self, bot):
		self.bot        = bot
		self.players    = {}
		self._shuffle   = False
		self._loop      = False


	async def cleanup(self, guild):
		try:
			await guild.voice_client.disconnect()
		except AttributeError:
			pass

		try:
			del self.players[guild.id]
		except KeyError:
			pass

	async def __local_check(self, ctx):
		"""A local check which applies to all commands in this cog."""
		if not ctx.guild:
			raise cmds.NoPrivateMessage
		return True

	async def __error(self, ctx, error):
		"""A local error handler for all errors arising from commands in this cog."""
		if isinstance(error, cmds.NoPrivateMessage):
			try:
				return await ctx.send('This command can not be used in Private Messages.')
			except discord.HTTPException:
				pass
		elif isinstance(error, InvalidVoiceChannel):
			await ctx.send('Error connecting to Voice Channel. '
						   'Please make sure you are in a valid channel or provide me with one')

		print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
		traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

	def get_player(self, ctx):
		"""Retrieve the guild player, or generate one."""
		try:
			player = self.players[ctx.guild.id]
		except KeyError:
			player = MusicPlayer(ctx)
			self.players[ctx.guild.id] = player
		return player

	@cmds.command(name='musicinfo', aliases=['mi'], hidden=True)
	@cmds.check(is_me)
	async def musicinfo_(self, ctx):
		"""Show stats about music bot.
		"""
		player = self.get_player(ctx)
		em = discord.Embed(
			color=0xcb981d,
			title="Info about Music Player"
			)
		em.add_field(
			name="Loop:",
			value=(":no_entry_sign:", ":arrows_clockwise:")[self._loop] + (" Off", " On")[self._loop],
			inline=True
			)
		em.add_field(
			name="Shuffle:",
			value=(":no_entry_sign:", " :twisted_rightwards_arrows:")[self._shuffle] + (" Off", " On")[self._shuffle],
			inline=True
		)
		em.add_field(
			name='Nb of songs in queue:',
			value=len(player.queue._queue),
			inline=False
		)
		em.add_field(
			name='Nb of played songs:',
			value=player.played,
			inline=True
		)
		em.add_field(
			name='Nb of failed songs:',
			value=player.failed,
			inline=True
		)
		em.set_footer(text="Made by BB Bot#3005")
		await ctx.send(content=None, embed=em)

	#@cmds.command(name='connect', aliases=['join', 'co'])
	#@cmds.check(is_admin)
	async def connect_(self, ctx, *, channel: discord.VoiceChannel = None):
		"""Connect to voice.
		Parameters
		------------
		channel: discord.VoiceChannel [Optional]
			The channel to connect to. If a channel is not specified, an attempt to join the voice channel you are in
			will be made.
		This command also handles moving the bot to different channels.
		"""
		if not channel:
			try:
				channel = ctx.author.voice.channel
			except AttributeError:
				raise InvalidVoiceChannel('No channel to join. Please either specify a valid channel or join one.')

		vc = ctx.voice_client

		if vc:
			if vc.channel.id == channel.id:
				return vc
			try:
				await vc.move_to(channel)
			except asyncio.TimeoutError:
				raise VoiceConnectionError("Moving to channel: <{}> timed out.".format(channel))
		else:
			try:
				vc = await channel.connect(reconnect=False) ## timeout=40, reconnect=True
			except asyncio.TimeoutError:
				raise VoiceConnectionError("Connecting to channel: <{}> timed out.".format(channel))

		await ctx.send("Connected to: **{}**".format(channel), delete_after=20)
		return vc

	@cmds.command(name="disconnect", aliases=["dc", "disc"])
	@cmds.check(is_admin)
	async def disconnect_(self, ctx):
		"""Disconnect voice.

		This command disconnects the voice bot.
		"""

		try:
			channel = ctx.author.voice.channel
		except AttributeError as e:
			raise InvalidVoiceChannel(e)

		vc = ctx.voice_client
		if vc:
			try:
				await vc.disconnect(force=True)
			except asyncio.TimeoutError:
				raise VoiceConnectionError("Connecting to channel: <{}> timed out.".format(channel))
		else:
			await ctx.send("Bot not connected to any channel.", delete_after=20)

	@cmds.command(name="playlist", aliases=["pl"])
	@cmds.check(is_admin)
	async def playlist_(self, ctx, url):
		"""Request a playlist and add it to the queue.
		This command attempts to join a valid voice channel if the bot is not already in one.
		Uses YTDL to automatically search and retrieve a playlist.
		Parameters
		------------
		search: url [Required]
			The song to search and retrieve using YTDL. This could be a simple search, an ID or URL.

		https://lh3.googleusercontent.com/m-vfDykY9etRvP8hoUK3P2fDB9d_6PR4o6k7sRWuu_USYYdVnI9RtL5ie1exAZR2J612cIzVzMe5_g=w1920-h1005
		"""
		vc = ctx.voice_client
		await ctx.trigger_typing()
		if not vc:
			vc = await self.connect_(ctx)
		arg = url.split(" ")
		if arg[0].find("playlist") == -1:
			ctx.invoke()
			return await ctx.send("Specified link is invalid, please use a link containing the `playlist` keyword.\n \
			See `/help pl`")
		if len(arg) == 1:
			r = requests.get(arg[0]).text
			soup = BeautifulSoup(r, "html.parser")
			domain = "https://youtube.com"
			player = self.get_player(ctx)
			for link in soup.find_all("a", {"dir": "ltr"}):
				href = link.get("href")
				if href.startswith("/watch"):
					source = await YTDLSource.create_source(ctx, domain + href, loop=self.bot.loop, download=False)
					try:
						await player.queue.put(source)
						player.queue.task_done()
					except Exception as e:
						print(e)
				time.sleep(2)
			await ctx.send("Finished queueing all songs in playlist requested by {}".format(ctx.author.mention))
		else:
			raise cmds.TooManyArguments

	@cmds.command(name="play", aliases=["p"])
	async def play_(self, ctx, *, search: str):
		"""Request a song and add it to the queue.
		This command attempts to join a valid voice channel if the bot is not already in one.
		Uses YTDL to automatically search and retrieve a song.
		Parameters
		------------
		search: str [Required]
			The song to search and retrieve using YTDL. This could be a simple search, an ID or URL.
		"""
		vc = ctx.voice_client
		await ctx.trigger_typing()
		if not vc:
			vc = await self.connect_(ctx)
		player = self.get_player(ctx)

		# If download is False, source will be a dict which will be used later to regather the stream.
		# If download is True, source will be a discord.FFmpegPCMAudio with a VolumeTransformer.
		source = await YTDLSource.create_source(ctx, search, loop=self.bot.loop, download=False)
		await player.queue.put(source)
		player.queue.task_done()
		await ctx.message.delete()

	@cmds.command(name='pause')
	@cmds.check(is_admin)
	async def pause_(self, ctx):
		""" Pause the currently playing song. """
		vc = ctx.voice_client

		if not vc or not vc.is_playing():
			return await ctx.send('I am not currently playing anything!', delete_after=20)
		elif vc.is_paused():
			return

		vc.pause()
		await ctx.send("**`{}`**: Paused the song!".format(ctx.author))

	@cmds.command(name='resume', aliases=["res"])
	@cmds.check(is_admin)
	async def resume_(self, ctx):
		""" Resume the currently paused song. """
		vc = ctx.voice_client

		if not vc or not vc.is_connected():
			return await ctx.send('I am not currently playing anything!', delete_after=20)
		elif not vc.is_paused():
			return

		vc.resume()
		await ctx.send("**`{}`**: Resumed the song!".format(ctx.author))

	@cmds.command(pass_context=True, name='skip', aliases=["s"])
	async def skip_(self, ctx):
		"""Skip the song.
		"""

		player = self.get_player(ctx)

		def group_to_range(group):
			group = ''.join(group.split())
			sign, g = ('-', group[1:]) if group.startswith('-') else ('', group)
			r = g.split('-', 1)
			r[0] = sign + r[0]
			r = sorted(int(__) for __ in r)
			return range(r[0], 1 + r[-1])

		def rangeexpand(txt):
			ranges = chain.from_iterable(group_to_range(__) for __ in txt.split(','))
			return sorted(set(ranges))

		vc = ctx.voice_client
		m = ctx.message.content
		if not vc or not vc.is_connected():
			return await ctx.send('I am not currently playing anything!', delete_after=20)
		elif m.startswith("/skip"):
			args = m.split("/skip ")
		else:
			args = m.split("/s ")

		try:
			invoker = ctx.author.name + "#" + ctx.author.discriminator
		except:
			return
			# skip one
		if len(args) == 1:
			if (invoker == str(player.current.requester) or
				is_admin(ctx)):
				if vc.is_paused():
					pass
				elif not vc.is_playing():
				   	return
				vc.stop()
			else:
				await ctx.send("Can not skip another person's song.")
		else:
			if is_admin(ctx):
				param = str(args[1:])[2:-2]
				#await ctx.send("params = %s" % param)
				nbs = rangeexpand(param)
				l = []
				qlen = len(player.queue._queue)
				for nb in nbs:
					if nb > qlen or nb < 1:
						raise cmds.BadArgument
					i = 0
					l.append(nb)
					for j in l:
						if j < nb:
							i += 1

					player.queue._queue.rotate(-nb+i+1)
					skd = player.queue._queue.popleft()
					await ctx.send("`{}#{}` skipped song **#{}** - `{}` requested by {}".format(
						ctx.author.name, ctx.author.discriminator, nb, skd["title"], skd["requester"].mention))
					player.queue._queue.rotate(nb-i-1)
			else:
				raise cmds.CheckFailure

	@cmds.command(name="shuffle", aliases=["shf"])
	@cmds.check(is_admin)
	async def shuffle_(self, ctx):
		""" Toggle song shuffling. """
		player = self.get_player(ctx)
		if not self._shuffle:
			await player.queue.join()
			shuffle(player.queue._queue, random=random)
			await ctx.send(":twisted_rightwards_arrows: Shuffle on.")
		else:
			await ctx.send(":arrow_right: Shuffle off.")
		self._shuffle = not self._shuffle
		player.shuffle = self._shuffle

	@cmds.command(name="loop")
	@cmds.check(is_admin)
	async def loop_queue(self, ctx):
		""" Toggle song looping. """
		player = self.get_player(ctx)
		if not self._loop:
			await player.queue.join()
			self._loop = not self._loop
			player.loop = self._loop
			await ctx.send(":arrows_clockwise: Looping on.")
		else:
			self._loop = not self._loop
			player.loop = self._loop
			await ctx.send(":arrow_forward: Looping off.")

	@cmds.command(name='queue', aliases=['q'])
	async def queue_info(self, ctx):
		""" Retrieve a basic queue of upcoming songs. """
		vc = ctx.voice_client

		if not vc or not vc.is_connected():
			return await ctx.send('I am not currently connected to voice!', delete_after=20)

		player = self.get_player(ctx)
		if player.queue.empty():
			return await ctx.send('There are currently no more queued songs.')

		upcoming = list(itertools.islice(player.queue._queue, 0, 25))
		fmt = ""
		j = 1
		for i in upcoming:
			fmt += "\n%i. **`%s`**\tadded by %s" % (j, i["title"], i["requester"])
			j += 1
		em = discord.Embed(title="Upcoming - Next {} songs".format(len(upcoming)), description=fmt)
		l = list(itertools.islice(player.queue._queue, 0, None))
		em.set_footer(text="Displaying {} of {} queued songs. Requested by {}#{}".format(
			len(upcoming),
			len(l),
			ctx.author.name,
			ctx.author.discriminator))
		await ctx.send(embed=em)

	@cmds.command(name='now_playing', aliases=['np', 'current', 'currentsong', 'playing'])
	async def now_playing_(self, ctx):
		"""Display information about the currently playing song."""
		vc = ctx.voice_client

		if not vc or not vc.is_connected():
			return await ctx.send('I am not currently connected to voice!', delete_after=30)

		player = self.get_player(ctx)
		if not player.current:
			return await ctx.send('I am not currently playing anything!')

		# Remove our previous now_playing message.
		try:
			if player.np:
				del(player.np)
		except discord.HTTPException:
			pass

		player.np = await ctx.send("**Now Playing:** `{}` requested by `{}`".format(
			vc.source.title, vc.source.requester))

	@cmds.command(name='volume', aliases=['vol'])
	@cmds.check(is_admin)
	async def change_volume(self, ctx):
		"""Change the player volume.
		Ex: /vol 20     or      /vol

		Parameters
		------------
		volume: int [Optional]
			The volume to set the player to in percentage. This must be between 1 and 100.
		"""
		vol = ctx.message.content.split()
		player = self.get_player(ctx)
		vc = ctx.voice_client

		if len(vol) == 1:
			await ctx.send("Bot volume at **{}%**".format(player.volume * 100))
		else:
			if bot.check(cmds.has_permissions(administrator=True) or cmds.has_role("DJ")):
				vol = float(round(float(vol[1]), 0))
				if not vc or not vc.is_connected():
					return await ctx.send('I am not currently connected to voice!', delete_after=20)

				if not 0 < vol < 101:
					return await ctx.send('Please enter a value between 1 and 100.')

				if vc.source:
					vc.source.volume = vol / 100

				player.volume = vol / 100
				await ctx.send("**`{}`**: Set the volume to **{}%**".format(ctx.author, vol))
			else:
				raise cmds.errors.CheckFailure

	@cmds.command(name='stop', aliases=['st', 'destroy'])
	@cmds.check(is_admin)
	async def stop_(self, ctx):
		"""
		Stop the currently playing song and destroy the player.
		!Warning!
			This will destroy the player assigned to your guild, also deleting any queued songs and settings.
		"""
		vc = ctx.voice_client

		if not vc or not vc.is_connected():
			return await ctx.send('I am not currently playing anything!', delete_after=20)
		player = self.get_player(ctx)
		player.destroy(ctx.guild)

	@cmds.command(name="cleanup", aliases=["clean"])
	@cmds.check(is_me)
	async def cleanup_(self, ctx):
		await self.cleanup(ctx.guild)

	@cmds.cooldown(1, 3, cmds.BucketType.user)
	@cmds.command(name="tuturu", aliases=["ttr"])
	async def tuturu_(self, ctx):
		""" Tuturu !!! """
		vc = ctx.voice_client
		if not vc:
			vc = await self.connect_(ctx)

		with open("data/assets/tuturu.mp3", "rb") as f:
			if vc.is_connected():
				vc.play(discord.FFmpegPCMAudio(f, pipe=True))
				await asyncio.sleep(2)
				await ctx.invoke(self.disconnect_)


def setup(bot):
	bot.add_cog(Music(bot))
