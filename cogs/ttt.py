from discord.ext		import commands as cmds
from discord.utils		import find, get
from pymongo			import MongoClient

from cogs.utils.perms	import *

import discord as dis
import asyncio
import os
import random


import pprint
pp = pprint.PrettyPrinter(indent=4)



try:
	from PIL import Image, ImageDraw, ImageFont, ImageColor, ImageOps, ImageFilter
except:
	raise RuntimeError("Can't load pillow. Do 'pip3 install pillow'.")

class TicTacToe(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		self.active_channels = {}
		self.playing = []
		self.x = self.draw_x()
		self.o = self.draw_o()
		self.clean_tmp()								# clean tmp img
		client = MongoClient("25.105.87.145", 27717)	# prod db
		self.db = client.leveler						# for money reward
		self.perm_read_write = dis.PermissionOverwrite(read_messages=True,
													   read_message_history=True,
													   send_messages=True)
		self.perm_deny = dis.PermissionOverwrite(read_messages=False,
												 read_message_history=False)

	def clean_tmp(self):
		if not os.path.isdir("tmp"):
			os.makedirs("tmp")
		for f in os.listdir("tmp"):
			if f.startswith("ttt_") and f.endswith("_tmp.png"):
				os.remove(f"tmp/{f}")


	@cmds.command()
	async def ttt(self, ctx, player2: dis.Member = None):
		u = [ctx.author]
		p = {
			ctx.guild.default_role: self.perm_deny,
			ctx.author: self.perm_read_write
		}
		# Append perms and player2 slot if specified
		if player2 is not None:
			if player2 == ctx.author:
				return await ctx.send("Can't play vs. yourself.")
			p[player2] = self.perm_read_write
			u.append(player2)
		# TEMPORARY DISABLE AI-MODE
		else:
			pfx = ctx.message.content[0]
			return await ctx.send((
				"Solomode (vs. AI) is temporarily disabled.\n"
				"Please play with a friend:\n\n"
				"**{}ttt @someone**".format(pfx)
			))
		# Check if already in-game or start vs.bot
		if player2.bot:
			return
		elif u[0] in self.playing:
			return await ctx.send("Error.\nYou are already in a game.")
		elif player2 != None and player2 in self.playing:
			return await ctx.send("Error.\n{} is already in a game.".format(player2))
		# Like this because: user `list` then perms `dict`
		await self.init_channel(ctx, u, p)


	@cmds.check(is_me)
	@cmds.command()
	async def ttta(self, ctx):
		pp.pprint(self.active_channels)




	def display_welcome_msg(self, users: list):
		msg = "Welcome to Tic-Tac-Toe!\n"
		msg += (
			"**Multi player Mode ({} VS. {})**".format(users[0].mention, users[1].mention),
			"**Solo player Mode ({} VS. Me :robot:)**".format(users[0].mention)
		)[len(users) == 1]
		return msg

	def is_space_free(self, board, digit):
		return board[digit] == ''

	def is_board_full(self, board):
		for i in range(1, 10):
			if self.is_space_free(board, i):
				return False
		return True


	def is_winner(self, bo: list, le):
		return ((bo[7] == le and bo[8] == le and bo[9] == le) or	# across the top
				(bo[4] == le and bo[5] == le and bo[6] == le) or	# across the middle
				(bo[1] == le and bo[2] == le and bo[3] == le) or	# across the bottom
				(bo[7] == le and bo[4] == le and bo[1] == le) or	# down the left side
				(bo[8] == le and bo[5] == le and bo[2] == le) or	# down the middle
				(bo[9] == le and bo[6] == le and bo[3] == le) or	# down the right side
				(bo[7] == le and bo[5] == le and bo[3] == le) or	# diagonal
				(bo[9] == le and bo[5] == le and bo[1] == le))		# diagonal

	def who_goes_first(self):
		# Randomly choose the player who goes first.
		return random.randint(0, 1)

	async def draw_board(self, ctx, digit: int, letter: str):
		uid = str(ctx.author.id)

		fn = f"tmp/ttt_{uid}_tmp.png"
		width = 249
		horz_gap = 12
		vert_gap = 12

		bg_color = (0, 0, 0, 0)
		bg_img = Image.open(fn)
		result = Image.new("RGBA", bg_img.size, bg_color)
		result.paste(bg_img, (0, 0))

		# Define matrix for easy img manipulation
		mult = [ (),
			(0, 2), (1, 2), (2, 2),
			(0, 1), (1, 1), (2, 1),
			(0, 0), (1, 0), (2, 0)
		]

		placeholder = Image.open((self.o, self.x)[letter == 'x'])
		coord = (
			horz_gap + int(mult[digit][0] * width/3),		### X-axis
			vert_gap + int(mult[digit][1] * width/3)		### Y-axis
		)
		result.paste(placeholder, coord, mask=placeholder)
		result.save(fn, "PNG", quality=100)

	def init_board(self, ctx):
		if not os.path.exists("tmp"):
			os.makedirs("tmp")
		## Init board only if not exists
		uid = str(ctx.author.id)
		filename = f"tmp/ttt_{uid}_tmp.png"
		if not os.path.exists(filename):
			width = 249
			bg_color = (0, 0, 0, 0)
			# Init image
			bg_img = Image.open("data/assets/ttt/bg.jpg")
			bg_img = bg_img.resize((width, width), Image.ANTIALIAS)
			result = Image.new("RGBA", (width, width), bg_color)
			result.paste(bg_img, (0, 0))
			# Draw grid
			draw = ImageDraw.Draw(result)
			ofs = 20
			fill = (186, 186, 186, 200)
			vert0_start, vert0_end = (width / 3, ofs), (width / 3, width - ofs)
			vert1_start, vert1_end = (width / 3 * 2, ofs), (width / 3 * 2, width - ofs)
			horz0_start, horz0_end = (ofs, width / 3), (width - ofs, width / 3)
			horz1_start, horz1_end = (ofs, width / 3 * 2), (width - ofs, width / 3 * 2)
			draw.line(vert0_start + vert0_end, width=5, fill=fill)
			draw.line(vert1_start + vert1_end, width=5, fill=fill)
			draw.line(horz0_start + horz0_end, width=5, fill=fill)
			draw.line(horz1_start + horz1_end, width=5, fill=fill)
			# Process img
			result = self._add_corners(result, 10)
			result.save(filename, "PNG", quality=100)
		return filename

	def draw_x(self):
		filename = "data/assets/ttt/x.png"
		if not os.path.exists(filename):
			width = 58
			x_color = (255, 0, 0, 255)
			bg_color = (0, 0, 0, 0)
			result = Image.new("RGBA", (width, width), bg_color)
			draw = ImageDraw.Draw(result)
			l0_start, l0_end = (0, 0), (width, width)
			l1_start, l1_end = (width, 0), (0, width)
			draw.line(l0_start + l0_end, width=3, fill=x_color)
			draw.line(l1_start + l1_end, width=3, fill=x_color)
			result.save(filename, "PNG", quality=100)
		return filename

	def draw_o(self):
		filename = "data/assets/ttt/o.png"
		if not os.path.exists(filename):
			width = 58
			o_color = (0, 0, 255, 255)
			bg_color = (0, 0, 0, 0)
			result = Image.new("RGBA", (width, width), bg_color)
			draw = ImageDraw.Draw(result)
			for i in range(0, 3):
				draw.ellipse((i, i, width-i, width-i), outline=o_color)
			result.save(filename, "PNG", quality=100)
		return filename

	def _add_corners(self, im, rad, multiplier = 6):
		raw_length = rad * 2 * multiplier
		circle = Image.new('L', (raw_length, raw_length), 0)
		draw = ImageDraw.Draw(circle)
		draw.ellipse((0, 0, raw_length, raw_length), fill=255)
		circle = circle.resize((rad * 2, rad * 2), Image.ANTIALIAS)
		alpha = Image.new('L', im.size, 255)
		w, h = im.size
		alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
		alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
		alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
		alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
		im.putalpha(alpha)
		return im


	async def init_channel(self, ctx, users: list, perms: dict):
		guild = ctx.guild
		gid = str(ctx.guild.id)
		cid = str(ctx.channel.id)
		uid = str(users[0].id)
		# Create "Games" category if inexistant
		games_exists = find(lambda c: c.name == "Games", guild.categories)
		if games_exists is None:
			await guild.create_category("Games")
		games = find(lambda c: c.name == "Games", guild.categories)
		# Create tic-tac-toe channel
		i = 1
		try:
			while find(lambda c: c.name == f"tictactoe_{i}", guild.text_channels):
				i += 1
		except:
			pass
		# Append playing users
		for u in users:
			self.playing.append(u)
		# Create channel and welcome players
		chan = await guild.create_text_channel(f"tictactoe_{i}", overwrites=perms, category=games)
		await chan.send(self.display_welcome_msg(users))
		# Append to dict
		if gid not in self.active_channels.keys():
			self.active_channels[gid] = {}
			self.active_channels[gid][uid] = {}
		else:
			if uid not in self.active_channels[gid].keys():
				self.active_channels[gid][uid] = {}
		self.active_channels[gid][uid] = {
			"channel": chan,
			"bo": [''] * 10,
			"mode": ("multi", "solo")[len(users) == 1],
			"turn": -1,
			"message": None,
			"first": None
		}
		# init img
		await self.start_game(ctx, self.init_board(ctx), users)



	async def input_player_letter(self, chan: dis.TextChannel, player: dis.Member, timeout: int):

		def check(msg):
			return msg.author == player

		answer = ''
		if player:
			await chan.send("{} do you want to be `X` or `O`?".format(player.mention))
		while (answer.lower() != 'x' and answer.lower() != 'o'):
			try:
				msg = await self.bot.wait_for("message", check=check, timeout=timeout)
				answer = msg.content
			except asyncio.TimeoutError:
				await chan.send("Game was cancelled because {} is afk.".format(player.mention))
				return None
		return answer.lower()

	async def delete_channel(self, chan: dis.TextChannel, sec: float):
		try:
			await chan.send((":boom: Channel will self-destruct in {} seconds."
							" Because the game has ended. :boom:").format(sec))
			await asyncio.sleep(sec)
			await chan.delete(reason="Game ended.")
		except:
			pass

	def opposite_bool(self, nb: int):
		return (0, 1)[nb == 0]

	async def get_player_move(self, ctx, users: list, timeout: int):

		def check(msg):
			return msg.author == player

		gid, uid = str(ctx.author.guild.id), str(ctx.author.id)
		game = self.active_channels[gid][uid]
		player = users[game["turn"]]
		msg = None

		await game["channel"].send("What is your next move {} ? (1-9)".format(player.mention))

		while (not msg) or (not msg.content.isdigit()) or	\
			(not 1 <= int(msg.content) <= 9) or (game["bo"][int(msg.content)] != ''):
			try:
				msg = await self.bot.wait_for("message", check=check, timeout=timeout)
			except asyncio.TimeoutError:
				await game["channel"].send("Game was cancelled because {} is afk.".format(player.mention))
				return None
		return int(msg.content)

	def make_move(self, ctx, digit: int, letter: str):
		gid, uid = str(ctx.author.guild.id), str(ctx.author.id)
		game = self.active_channels[gid][uid]

		game["bo"][digit] = letter

	async def remove_image(self, game: dict):
		if game["message"]:
			await game["message"].delete()
			game["message"] = None

	def reward_player(self, user: dis.Member, money: int):
		userinfo = self.db.users.find_one({"user_id": user.id})
		curr_credit = userinfo["servers"][str(user.guild.id)]["credit"]
		self.db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{user.guild.id}.credit": curr_credit + money
		}})


	async def start_game(self, ctx, img: str, users: list):
		gid, uid = str(ctx.author.guild.id), str(ctx.author.id)
		game = self.active_channels[gid][uid]
		chan = game["channel"]
		loop = True

		# t1: timeout for first player `x` or `o` choice
		# t2: timeout for move turn
		t1, t2 = 60, 30
		while loop:
			# Determine who goes first and let him choose 'X' or 'O'
			turn = self.who_goes_first()
			game["turn"] = turn
			game["first"] = users[turn]
			await chan.send(
				"__**How to play:**__\n\nGrid numbers are represented by NumPad",
				file=dis.File(open("data/assets/ttt/board.jpg", "rb")))
			await chan.send((
				"{} will go first.\n\n"
				"You have {} seconds to choose your sign.").format(
					game["first"], t1))
			marker = await self.input_player_letter(chan, game["first"], t1)
			#
			if marker is None:
				break
			if (turn == 0 and marker == 'x') or (turn == 1 and marker == 'o'):
				markers = ['x', 'o']
			if (turn == 0 and marker == 'o') or (turn == 1 and marker == 'x'):
				markers = ['o', 'x']
			await chan.send("{} will be **`{}`** and {} will be **`{}`**.".format(
				game["first"], markers[turn].upper(),
				users[self.opposite_bool(turn)], markers[self.opposite_bool(turn)].upper()))

			# Print empty board
			game["message"] = await chan.send(file=dis.File(open(img, "rb")))

			# Start Game
			game_is_playing = True
			while game_is_playing:
				# Get and set (if valid) player move
				await chan.send("{}, you have {} seconds to make your move".format(
					users[game["turn"]], t2))
				digit = await self.get_player_move(ctx, users, t2)
				if digit is None:
					loop = False
					break
				self.make_move(ctx, digit, markers[game["turn"]])
				await self.draw_board(ctx, digit, markers[game["turn"]])

				amount = 250
				amount2 = int(amount / 2)
				if self.is_winner(game["bo"], markers[game["turn"]]):
					await self.remove_image(game)
					await chan.send(file=dis.File(open(img, "rb")))
					await chan.send((
						"Congratulations {}! You have won the game!\n"
						"{} has been awarded `{}`$.").format(
							users[game["turn"]], users[game["turn"]], amount))
					self.reward_player(users[game["turn"]], amount)
					game_is_playing, loop = False, False
				else:
					if self.is_board_full(game["bo"]):
						await self.remove_image(game)
						await chan.send(file=dis.File(open(img, "rb")))
						await chan.send((
							"The game is a tie!\n"
							"You both have been awarded `{}`$.").format(amount2))
						self.reward_player(users[0], amount2)
						self.reward_player(users[1], amount2)
						game_is_playing, loop = False, False
					else:
						game["turn"] = self.opposite_bool(game["turn"])
						await self.remove_image(game)
						game["message"] = await chan.send(file=dis.File(open(img, "rb")))

		timer = 10
		# Remove tmp img
		if os.path.exists(img):
			os.remove(img)
		# Remove playing user(s)
		for u in users:
			self.playing.remove(u)
		# Remove tmp chan
		await self.delete_channel(chan, timer)







def setup(bot):
	bot.add_cog(TicTacToe(bot))
