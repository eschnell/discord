from discord.ext		import commands as cmds
from discord.utils		import find
from pymongo			import MongoClient
import datetime

from cogs.utils.perms	import *
from cogs.utils.dataIO	import dataIO
#from cogs.utils.locale	import get_user_locale

import discord as dis
import asyncio
import random
import requests
import time

import pprint
pp = pprint.PrettyPrinter(indent=4)

class Greeter(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, ctx):
		self.bot = ctx
		self.db = MongoClient("25.105.87.145", 27717).greeter	# prod db.leveler


	##########################################
	async def cog_command_error(self, ctx, error):
		if isinstance(error, cmds.CommandInvokeError):
			await ctx.send(error.original)

	async def _create_update_stats_category(self, member):

		async def _create_chan(ctg, pos: int, title: str, val: int):
			chan = find(lambda k: k.name.startswith(title), g.voice_channels)
			title = f"{title}: {val}"
			#print(f"Channel: {chan}")
			if chan is not None:
				await chan.edit(name=title)
				return chan
			return await ctg.create_voice_channel(title, position=pos)

		i = 0
		g = member.guild
		members = g.members
		for m in members:
			if m.bot: i += 1

		name = "『 Guild stats 』"
		ctg = find(lambda k: k.name == name, g.categories)
		if ctg is None:
			p = {
				g.default_role: dis.PermissionOverwrite(view_channel=True,
														connect=False,
													    read_message_history=False)
			}
			ctg = await g.create_category_channel(name, overwrites=p)
		
		chan0 = await _create_chan(ctg, 0, f"Members", len(members))
		await asyncio.sleep(.5)
		chan1 = await _create_chan(ctg, 1, f"Humans", len(members) - i)
		await asyncio.sleep(.5)
		chan2 = await _create_chan(ctg, 2, f"Bots", i)
		await chan0.edit(position=0)
		await chan1.edit(position=1)
		await chan2.edit(position=2)

	async def _greet_in_channel(self, m: dis.Member):
		g = m.guild																# get guild for role
		gid = g.id																# guild id
		d = self.db.greet.find_one({"guild_id": gid})							# db settings
		
		# Only greet if defined in db, and state is on
		if d is not None and d["state"]:
			chan = find(lambda c: c.id == d["chan_id"], g.text_channels)	# greeting channel
			if d["role"] is not None:										# get role to add on member_join if defined
				role = find(lambda r: r.id == d["role"], g.roles)
				await m.edit(roles=[role])
			# Greet
			with open("data/greeter/strings.txt", "r", encoding="utf-8") as f:
				msg = f.read()
			lst = msg.splitlines()
			hour = datetime.datetime.now().hour
			randint = random.randint(0, len(lst) - 1)
			# Day/night check
			if randint == 0 and not 5 <= hour < 17: randint = 1
			if randint == 1 and 5 <= hour < 17: randint = 0

			r = lst[randint].replace("_", m.mention)
			await chan.send(r)
		
	async def _leave_warn_in_channel(self, m: dis.Member):
		g = m.guild																# get guild for role
		gid = g.id																# guild id
		d = self.db.lw.find_one({"guild_id": gid})								# db settings
		
		if d is not None and d["state"]:
			chan_id = d["chan_id"]
			chan = g.get_channel(int(chan_id))
			await chan.send(f"@here\n`{m.name}` {m.mention} has left the server.. :(")
	
	##########################################

	# On member leave
	@cmds.Cog.listener()
	async def on_member_remove(self, member):
		if member.guild.id == 334559651167207426:
			await self._create_update_stats_category(member)
			await self._leave_warn_in_channel(member)

	# On reaction_add
	@cmds.Cog.listener()
	async def on_raw_reaction_add(self, payload):
		#print(f"reaction: {payload}\n\ndir: {dir(payload)}")
		""" if not payload.member.bot:
			#print(payload)
			print('\n'.join((f"reaction: {payload.emoji.name}",
				f"is_custom: {payload.emoji.is_custom_emoji()}",
				f"msg_id: {payload.message_id}\n\n"))) """
		pass

	# On member join
	@cmds.Cog.listener()
	async def on_member_join(self, member):
		if member.guild.id == 334559651167207426:
			await self._create_update_stats_category(member)
		await self._greet_in_channel(member)

		


	##########################
	# COMMANDS
	##########################

	##############
	# Auto roles
	@cmds.group(name="autorole", no_pm=True, aliases=["ar"])
	async def _autorole(self, ctx):
		"""AutoRole options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@cmds.check(is_admin)
	@_autorole.command(name="add", no_pm=True)
	async def _add(self, ctx, message: dis.Message, role: dis.Role, emoji: str):
		try:
			emoji = self.bot.get_emoji(int(emoji[-19:-1]))
		except:
			pass
		is_custom = (False, True)[isinstance(emoji, dis.Emoji)]
		d = {
			"emoji": (emoji, emoji.id)[is_custom],
			"is_custom": is_custom,
			"message_id": message.id
		}
		pp.pprint(d)

	##############
	# Leave buster
	@cmds.group(name="leavewarn", no_pm=True, aliases=["lw"])
	async def _leavewarn(self, ctx):
		"""LeaveWarn options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@cmds.check(is_me)
	@_leavewarn.command(name="toggle", no_pm=True)
	async def _toggle(self, ctx, guild: dis.Guild = None):
		"""Guild ID to specify (optional)"""
		g = (guild, ctx.guild)[guild is None]
		gid = g.id
		d = self.db.lw.find_one({"guild_id": gid})
		cid = d["chan_id"]
		state = d["state"]
		d = { f"state": (True, False)[state] }
		self.db.lw.update_one({"guild_id": gid}, {"$set": d})
		if state: m = f"{g} 's server `leave_warn` state is now `Off`."
		else: m = f"{g} 's server `leave_warn` state is now `On` logging in {g.get_channel(cid).mention} channel."
		await ctx.send(m)

	@cmds.check(is_admin)
	@_leavewarn.command(name="set", no_pm=True)
	async def _lw_set(self, ctx, channel: dis.TextChannel = None):
		"""Set `leave_warn` location."""
		c = (channel, ctx.channel)[channel is None]
		d = {
			f"state": True,
			f"chan_id": c.id
		}
		self.db.lw.update_one({"guild_id": c.guild.id}, {"$set": d}, upsert=True)
		await ctx.send(f"{c.guild} 's server `leave_warn` has been set to {c.mention}.")

	@cmds.check(is_admin)
	@_leavewarn.command(name="unset", no_pm=True)
	async def _lw_unset(self, ctx, channel: dis.TextChannel = None):
		"""Unset `leave_warn`."""
		c = (channel, ctx.channel)[channel is None]
		self.db.lw.delete_one({"guild_id": c.guild.id})
		await ctx.send(f"{c.guild} 's server `leave_warn` has been unset.")

	#########
	# Greeter
	@cmds.group(name="greeter", no_pm=True, aliases=["grt"])
	async def _grt(self, ctx):
		"""Greeter options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@cmds.check(is_admin)
	@_grt.command(name="set", no_pm=True)
	async def _set(self, ctx, channel: dis.TextChannel, role: dis.Role = None):
		"""
		Welcome users (and give them role [optional]).

		Parameters
		==========

		channel: [channel] channel or ID of channel to greet users
		role:        [role] role or ID of role to give to users when joining server
		"""
		g = channel.guild
		gid = g.id
		d = {
			f"chan_id": channel.id,
			f"state": True,
			f"role": (None, role.id)[role is not None]
		}
		self.db.greet.update_one({"guild_id": channel.guild.id}, {"$set": d}, upsert=True)
		if role is None: m = f"{g} 's server `greet` has been set to {channel.mention}."
		else: m = f"{g} 's server `greet` has been set to {channel.mention} with default role {role.mention}."
		await ctx.send(m)

	@cmds.check(is_admin)
	@_grt.command(name="unset", no_pm=True)
	async def _unset(self, ctx):
		"""Unset `greeter`."""
		g = ctx.guild
		gid = g.id
		self.db.greet.delete_one({"guild_id": gid})
		await ctx.send(f"{g} 's server `greet` has been unset.")

	@cmds.check(is_admin)
	@_grt.command(name="list", no_pm=True)
	async def _list(self, ctx):
		"""Lists `greeter` servers."""
		m = ""
		guilds = self.db.greet.find()
		for g in guilds:
			g["guild_name"] = self.bot.get_guild(g["guild_id"]).name
			g["chan_name"] = self.bot.get_channel(g["chan_id"]).name
			m += f"# {g['guild_name']}\n"
			m += f"> [{g['chan_name']}](https://discordapp.com/channels/{g['guild_id']}/{g['chan_id']})\n\n"
		em = dis.Embed(title="List of saved Greeters", description=m)		
		await ctx.send(embed=em)





def setup(bot):
	bot.add_cog(Greeter(bot))
