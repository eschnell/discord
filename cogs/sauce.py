from discord.ext	import commands
#from saucenao import SauceNao

import discord
import logging
import os
import requests
import shutil
import json

import cogs.utils.api		as api
from lib.saucenao.saucenao	import SauceNao

class Sauce(commands.Cog):

	def __init__(self, bot):
		self.bot = bot
		self.apis = self.bot.apis
		self.sn = SauceNao(
			directory="tmp",
			databases=999,
			minimum_similarity=65,
			combine_api_types=True,
			api_key=self.apis["saucenao"],
			exclude_categories='',
			move_to_categories=False,
			use_author_as_category=False,
			output_type=SauceNao.API_HTML_TYPE,
			start_file='',
			log_level=logging.ERROR,
			title_minimum_similarity=90)

	@commands.command(pass_context=True, aliases=["sn", "sauce"])
	@commands.cooldown(1, 5.0, type=commands.BucketType.user)
	async def saucenao(self, ctx, link: str = None):
		"""Image search"""
		self.ctx = ctx

		await ctx.message.delete()
		if not os.path.exists("tmp"):
			os.makedirs("tmp")

		# DL and store target img under tmp/ folder
		def get_image(url: str):
			r = requests.get(url, stream=True)
			if r.status_code == 200:
				open("tmp/saucenaobuf", 'w+')
				with open("tmp/saucenaobuf", 'wb') as f:
					r.raw.decode_content = True
					shutil.copyfileobj(r.raw, f)
				return 1
			else:
				return 0

		# Creates response from json response
		def create_embed(ret: json):
			i = 0
			lst = []
			if len(ret) == 0:
				return 0
			while i < 3:
				try:
					tmp = ret[i]
					em = discord.Embed(
						color = 0xad2929,
						title = tmp['data']['title'],
						description = "Original poster " + tmp['data']['author_name']
					)
					em.set_image(url=tmp['header']['thumbnail'])
					em.add_field(
						name="Similarity",
						value=tmp['header']['similarity'] + " %",
						inline=True
					)
					em.add_field(
						name="Link",
						value=tmp['data']['ext_urls'][0],
						inline=True
					)
					lst.append(em)
				except Exception as e:
					print(e)
				i += 1
			return lst

		# Response flow
		async def prepare_resp(self, ret: int = None):
			async with self.ctx.channel.typing():
				if ret:
					filtered_results = self.sn.check_file(file_name="saucenaobuf")
					resp = create_embed(filtered_results)
					if isinstance(resp, list):
						for i in resp:
							await self.ctx.send(content=None, embed=i)
					elif resp == 0:
						await self.ctx.send("No result. :pensive:")
					else:
						raise commands.CommandError
				else:
					raise commands.CommandInvokeError

		# Do magic
		if link == None:
			async for m in ctx.channel.history():
				if len(m.attachments) == 1:
						ret = get_image(m.attachments[0].url)
						return await prepare_resp(self, ret)
		else:
			ret = get_image(link)
			await prepare_resp(self, ret)


def setup(bot):
    bot.add_cog(Sauce(bot))
