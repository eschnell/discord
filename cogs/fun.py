from discord.ext		import commands as cmds

from cogs.utils.dataIO	import dataIO
from cogs.utils.locale	import get_user_locale
from cogs.utils.logger	import Logger

import discord as dis
import os
import random
import requests
import urllib


class Fun(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		## For Logger module
		self.logname = __name__								# Needed by logger class
		self.log = Logger(self)								# Logger obj

	def _get_ascii_art(self, name):
		with open(f"data/fun/ascii/{name}.txt", "r", encoding="utf-8") as fp:
			t = fp.read()
		return ''.join(["```\n", t, "```"])


	@cmds.Cog.listener()
	async def on_message(self, message):
		if message.author.bot:
			return
		msg = message.content.lower()
		chan = message.channel

		tableflip = (
			"¯\\_(ツ)_/¯" in msg or					\
			"tableflip" in msg or					\
			("table" in msg and "flip" in msg) or	\
			"flip" in msg
			)
		if tableflip:
			return await chan.send(self._get_ascii_art("tableflip"))
		#if "vader" in msg:
		#	return await chan.send(self._get_ascii_art("vader"))

	def _get_gif_path(self, gif_type: str):
		# Get urls
		gifs = dataIO.load_hjson("data/fun/urls.hjson")[gif_type]
		# Choose random image url from urls list
		rng = random.randint(0, len(gifs)-1)
		# Return url
		return gifs[rng]

	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command(name="lmgtfy", aliases=["letmegooglethatforyou", "lmgt", "lmg"])
	async def lmgtfy(self, ctx, *, query: str):
		"""Developers' man"""
		q = urllib.parse.quote(query)
		base_url = f"https://lmgtfy.com/?q={q}&s=g"
		await ctx.send(base_url)


	## Taunt
	@cmds.cooldown(1, 60, cmds.BucketType.user)
	@cmds.command(name="ggez", enabled=True)
	async def gg_ez(self, ctx):
		"""Taunts"""
		await ctx.send("https://youtu.be/664VCs3c1HU")

	@cmds.cooldown(1, 60, cmds.BucketType.user)
	@cmds.command(name="bjw", enabled=True)
	async def better_jungle_wins(self, ctx):
		"""Taunts"""
		await ctx.send("https://youtu.be/Sr7ZUlsTwKc")


	## Emotes
	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command()
	async def hug(self, ctx, user: dis.Member):
		"""Hug someone"""
		_ = get_user_locale("fun", ctx.author)
		if user.id == self.bot.bot_id:
			return await ctx.send(_("{} I don't want your hug!").format(self.bot.bot_prefix))
		path = self._get_gif_path("hug")
		em = dis.Embed(
			color=ctx.author.color,
			description=_("{} hugs {}!").format(ctx.author, user.mention))
		em.set_image(url=path)
		self.log.warn(f"{ctx.author} used hug gif: {path}")
		await ctx.send(embed=em)

	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command()
	async def pat(self, ctx, user: dis.Member):
		"""Pat someone"""
		_ = get_user_locale("fun", ctx.author)
		if user.id == self.bot.bot_id:
			return await ctx.send(_("{} I don't want you to pat me!").format(self.bot.bot_prefix))
		path = self._get_gif_path("pat")
		em = dis.Embed(
			color=ctx.author.color,
			description=_("{} pats {}!").format(ctx.author, user.mention))
		em.set_image(url=path)
		self.log.warn(f"{ctx.author} used pat gif: {path}")
		await ctx.send(embed=em)

	@cmds.cooldown(2, 60, cmds.BucketType.user)
	@cmds.command()
	async def poke(self, ctx, user: dis.Member):
		"""Poke someone"""
		#_ = get_user_locale("fun", ctx.author)
		if user.id == self.bot.bot_id:
			return await ctx.send(("{} don't even poke me!").format(self.bot.bot_prefix))
		path = self._get_gif_path("poke")
		em = dis.Embed(
			color=ctx.author.color,
			description=("{} pokes {}!").format(ctx.author, user.mention))
		em.set_image(url=path)
		self.log.warn(f"{ctx.author} used poke gif: {path}")
		await ctx.send(embed=em)








def setup(bot):
	bot.add_cog(Fun(bot))
