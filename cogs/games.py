from discord.ext		import commands as cmds
from discord.utils		import find, get
from pymongo			import MongoClient
from async_timeout		import timeout

from cogs.utils.perms	import *

import discord as dis
import asyncio
import io
import math
import os
import random
import re
import requests



#import pprint
#pp = pprint.PrettyPrinter(indent=4)



try:
	from PIL import Image, ImageDraw, ImageFont, ImageColor, ImageOps, ImageFilter
except:
	raise RuntimeError("Can't load pillow. Do 'pip3 install pillow'.")

class Games(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot
		self.active_channels = {}
		self.playing = []
		self.clean_tmp()
		client = MongoClient("25.105.87.145", 27717)	# prod db
		self.db = client.leveler						# for money reward
		self.perm_read_write = dis.PermissionOverwrite(read_messages=True,
													   read_message_history=True,
													   send_messages=True)
		self.perm_deny = dis.PermissionOverwrite(read_messages=False,
												 read_message_history=False)
		self.font_heavy_file = "data/assets/font/Uni_Sans_Heavy.ttf"

	#########################################################################
	#
	#
	#
	#########################################################################

	@cmds.cooldown(1, 60, cmds.BucketType.member)
	@cmds.command(aliases=["bts"])
	async def battleships(self, ctx, player2: dis.Member):
		u = [ctx.author, player2]
		#
		if player2.bot:
			return
		if ctx.author in self.playing:
			return await ctx.send("Error.\nYou are already in a game.")
		elif player2 in self.playing:
			return await ctx.send("Error.\n{} is already in a game.".format(player2))
		await self.start_game(ctx, u)

	@cmds.check(is_me)
	@cmds.command(aliases=["ca"])
	async def clear_all(self, ctx):
		tc = ctx.guild.text_channels
		for t in tc:
			try:
				if t.name.startswith("battleships_"):
					await t.delete()
			except:
				continue

	@cmds.check(is_me)
	@cmds.command()
	async def btsa(self, ctx):
		print(f"playing = {self.playing}")
		for u in self.playing:
			dic = self.active_channels[str(ctx.guild.id)][str(u.id)]
			for i in dic:
				if i != "bo":
					print(f"{i}: {dic[i]}")
				else:
					j = 1
					print(f"{i}: [ {dic[i][0]},")
					while j < len(dic[i]):
						print(f"{dic[i][j]}, {dic[i][j+1]},
								{dic[i][j+2]}, {dic[i][j+3]},
								{dic[i][j+4]}, {dic[i][j+5]},
								{dic[i][j+6]}, {dic[i][j+7]},
								{dic[i][j+8]}, {dic[i][j+9]},")
						j += 10
			print('\n')
		#print(self.active_channels)


	###############################################

	def clean_tmp(self):
		# Clean residual channels
		if not os.path.isdir("tmp"):
			os.makedirs("tmp")
		for f in os.listdir("tmp"):
			if f.startswith("bts_") and f.endswith(".png"):
				os.remove(f"tmp/{f}")

	def remove(self, path: str):
		os.remove(path)

	def place_boat_on_grid(self, grid: Image.Image, half: tuple, u: dis.Member, coords: list):
		tmp_layer = grid.copy()
		tenth = int(tmp_layer.width/11)

		ret = Image.open(f"tmp/bts_{u.id}.png")
		tmp = Image.open(f"data/assets/bts/sprite.png")
		tmp = tmp.resize((tenth, tenth), Image.ANTIALIAS)
		sprite = Image.new("RGBA", (tenth, tenth), (0, 0, 0, 0))
		sprite.paste(tmp, (0, 0))
		for coord in coords:
			y = int(coord / 10)
			x = coord - y * 10
			if x == 0:
				x = 10
				y -= 1
			tmp_layer.paste(sprite, (tenth*x, tenth*(y+1)), mask=sprite)
		ret.paste(tmp_layer, half, mask=tmp_layer)
		ret.save(f"tmp/bts_{u.id}_tmp.png", "PNG", quality=100)


	def init_image(self, u: list):
		if not os.path.exists("tmp"):
			os.makedirs("tmp")

		i = 0
		x = 1400 #width
		y = 1000 #heigth
		ofs = 20
		avatar_side = int((x/2 - ofs) / 6)
		red_fill = (255, 0, 0, 255)
		white_fill = (255, 255, 255, 255)
		height_fnt = int(math.sqrt(x**2 + y**2) / 46) # :23 ratio
		title_fnt = ImageFont.truetype(self.font_heavy_file, height_fnt)

		bg_img = Image.open("data/assets/bts/bg.jpg")
		bg_img.resize((x, y), Image.ANTIALIAS)
		result = Image.new("RGBA", (x, y), (0, 0, 0, 0))
		result.paste(bg_img, (0, 0))

		# Draw layout
		draw = ImageDraw.Draw(result)

		# add titles
		t0 = "Your fleet"
		t1 = "Enemy fleet"
		t0_x_pos = self._center(ofs, x/2, t0, title_fnt)
		t1_x_pos = self._center(x/2, x-ofs, t0, title_fnt)
		draw.text((t0_x_pos, ofs+int(height_fnt/2)), t0, font=title_fnt, fill=white_fill)
		draw.text((t1_x_pos, ofs+int(height_fnt/2)), t1, font=title_fnt, fill=white_fill)

		# vert lines
		t0_start, t0_end = (ofs, ofs), (ofs, y-ofs)
		t1_start, t1_end = (x/2, ofs), (x/2, y-ofs)
		t2_start, t2_end = (x-ofs, ofs), (x-ofs, y-ofs)
		# horiz lines
		t3_start, t3_end = (ofs, ofs), (x-ofs, ofs)
		t4_start, t4_end = (ofs, y-ofs), (x-ofs, y-ofs)
		median_start, median_end = (ofs, y - int(x/2 + ofs)), (x - ofs, y - int(x/2 + ofs))

		grid, half0, half1 = self.init_grid(result, ofs, median_start[1]) 		## TEMPORARY
		while i < 2:
			# paste avatar imgs
			opposite = (u[0], u[1])[i == 0]
			r0 = requests.get(u[i].avatar_url)
			r1 = requests.get(opposite.avatar_url)
			stream_io_0 = io.BytesIO(initial_bytes=r0.content)
			stream_io_1 = io.BytesIO(initial_bytes=r1.content)

			av0, av1 = Image.open(stream_io_0), Image.open(stream_io_1)
			av0 = av0.resize((avatar_side, avatar_side), Image.ANTIALIAS)
			av1 = av1.resize((avatar_side, avatar_side), Image.ANTIALIAS)
			result.paste(av0, (ofs, median_start[1] - avatar_side))
			result.paste(av1, (x - (ofs + avatar_side), median_start[1]-avatar_side))

			# draw
			draw.line(t0_start + t0_end, width=5, fill=red_fill)
			draw.line(t1_start + t1_end, width=5, fill=red_fill)
			draw.line(t2_start + t2_end, width=5, fill=red_fill)
			draw.line(t3_start + t3_end, width=5, fill=red_fill)
			draw.line(t4_start + t4_end, width=5, fill=red_fill)
			draw.line(median_start + median_end, width=5, fill=(30, 255, 30, 255))
			# process img
			result.save(f"tmp/bts_{u[i].id}.png", "PNG", quality=100)
			i += 1
		return (grid, half0, half1)

	def init_grid(self, ret: Image.Image, ofs: int, m_height: int, redraw: bool = True):
		i = 1
		white_fill = (255, 255, 255, 255)
		f_height = 35
		grid_txt_fnt = ImageFont.truetype(self.font_heavy_file, f_height)
		tmp = Image.new("RGBA", (int(ret.width/2) - ofs*4, int(ret.width/2) - ofs*4), (0, 0, 0, 0))
		draw = ImageDraw.Draw(tmp)

		tenth = int(tmp.width / 11)

		if redraw:
			while i < 12:
				# write 1 - 10
				if i < 10:
					pos_digit = self._center(tenth * i, tenth * (i + 1), chr(i + 48), grid_txt_fnt)
					draw.text((pos_digit, tenth - f_height), chr(i + 48), font=grid_txt_fnt, fill=white_fill)
				elif i == 10:
					pos_digit = self._center(tenth * i, tenth * (i + 1), "10", grid_txt_fnt)
					draw.text((pos_digit, tenth - f_height), "10", font=grid_txt_fnt, fill=white_fill)
				# write A - J
				if i < 11:
					pos_letter = self._center(ofs, tenth, chr(i + 64), grid_txt_fnt)
					draw.text((pos_letter, tenth * i + int(f_height/5*2)), chr(i + 64), font=grid_txt_fnt, fill=white_fill)

				# paste vert lines
				draw.line(((tenth * i, tenth) + (tenth * i, tmp.height)),		\
						fill=white_fill, width=3)
				# paste horiz lines
				draw.line(((tenth, (tenth * i)) + (tmp.width, (tenth * i))),	\
						fill=white_fill, width=3)
				i += 1
			ret.paste(tmp, (ofs+20, m_height+20), mask=tmp)
			ret.paste(tmp, (int(ret.width/2)+20, m_height+20), mask=tmp)

		return (tmp, (ofs+20, m_height+20), (int(ret.width/2)+20, m_height+20))





	def display_welcome_msg(self, users: list, t: float):
		msg = "Welcome to BattleShips!\n"										\
			  f"**{users[0].name} VS. {users[1].name}**\n\n"					\
			  "You may place your ships horizontally or vertically.\nEx:\n"		\
			  "`a1-a3` for **horizontal**\n"									\
			  "`4C-4E` for **vertical**\n\n"									\
			  f"**__You have {int(t/60)} mins to place all your ships.__**"
		return msg

	# finds the the pixel to center the text
	def _center(self, start, end, text, font):
		dist = end - start
		width = font.getsize(text)[0]
		start_pos = start + ((dist - width) / 2)
		return int(start_pos)



	def parse_range(self, m: str):

		def customRange(lst):
			# different letter
			if lst[0][0] != lst[1][0]:
				test 	= ord(lst[0][0]) < ord(lst[1][0])
				start	= (ord(lst[1][0]), ord(lst[0][0]))[test]
				end		= (ord(lst[0][0]), ord(lst[1][0]))[test]
				for c in range(start, end + 1, 1):
					yield chr(c) + lst[0][1:]
			# different number
			else:
				test 	= int(lst[0][1:]) < int(lst[1][1:])
				start	= (int(lst[1][1:]), int(lst[0][1:]))[test]
				end		= (int(lst[0][1:]), int(lst[1][1:]))[test]
				for n in range(start, end + 1):
					yield lst[0][0] + str(n)

		l = []
		l2 = []
		expanded_l = []
		m = m.lower()
		i = 0
		charset_accept = "- 0123456789abcdefghij"

		# Make sure '-' char is inside the input
		if not '-' in m:
			return None

		# Check for illegal chars
		while i < len(m):
			if m[i] not in charset_accept:
				return None
			i += 1
		i = 0

		# Seperate into list
		m = m.split('-')

		# Get rid of whitespacing
		while i < len(m):
			m[i] = m[i].strip()
			i += 1

		# Check if only 2 elems in list
		if len(m) != 2:
			return None

		# check both elem (sides) if 2 or 3 chars
		for elem in m:
			if (len(elem) == 3 and "10" not in elem) or	\
			   (len(elem) == 2 and "0" in elem) or		\
			   (not 1 < len(elem) < 4) or				\
			   elem.isalpha() or 						\
			   elem.isdigit():
				return None

		# re-arrange list with letter as 0-index
		for elem in m:
			if len(elem) == 2 and elem[0].isdigit():
				l.append(elem[1].lower() + elem[0])
			elif len(elem) == 3 and elem[0:2].isdigit():
				l.append(elem[2].lower() + elem[0:2])
			else:
				l.append(elem.lower())

		# check for straight lines
		if l[0][0] != l[1][0] and l[0][1:] != l[1][1:]:
			return None
		# check for ships from 2 to 5 slots
		if l[0][0] == l[1][0] and 		\
		   not 0 < abs(int(l[0][1:]) - int(l[1][1:])) < 5:
			return None
		if l[0][1:] == l[1][1:] and		\
		   not 0 < abs(ord(l[0][0]) - ord(l[1][0])) < 5:
			return None

		# expand from 2 coords to a list of coords
		for value in customRange(l):
			expanded_l.append(value)
		# transform coords to index-like ints
		for value in expanded_l:
			l2.append((ord(value[0]) - 97) * 10 + int(value[1:]))

		return l2

	def is_winner(self, bo: list):
		return ((1 not in bo and
				 2 not in bo and
				 3 not in bo and
				 4 not in bo and
				 5 not in bo))

	def reward_player(self, user: dis.Member, money: int):
		userinfo = self.db.users.find_one({"user_id": user.id})
		curr_credit = userinfo["servers"][str(user.guild.id)]["credit"]
		self.db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{user.guild.id}.credit": curr_credit + money
		}})



	async def delete_channel(self, chan: dis.TextChannel, sec: float):
		try:
			await chan.send((":boom: Channel will self-destruct in {} seconds."
							 " Because the game has ended. :boom:").format(sec))
			await asyncio.sleep(sec)
			await chan.delete(reason="Game ended.")
		except:
			pass

	async def init_channel(self, ctx, users: list, sec: float):
		guild = ctx.guild
		gid = str(ctx.guild.id)


		# Set perms accordingly
		p0 = {
			ctx.guild.default_role: self.perm_deny,
			users[0]: self.perm_read_write
		}
		p1 = {
			ctx.guild.default_role: self.perm_deny,
			users[1]: self.perm_read_write
		}
		# Create "Games" category if inexistant
		games_exists = find(lambda c: c.name == "Games", guild.categories)
		if games_exists is None:
			await guild.create_category("Games")
		games = find(lambda c: c.name == "Games", guild.categories)
		# Create battleships channel
		i = 1
		try:
			while find(lambda c: c.name == f"battleships_{i}", guild.text_channels):
				i += 1
		except:
			pass

		# Create channels and welcome players
		tmp_cpy = []
		for u in users:
			tmp_cpy.append(u)
		tmp_cpy.reverse()
		chan0 = await guild.create_text_channel(f"battleships_{i}",
									overwrites=p0, category=games)
		chan1 = await guild.create_text_channel(f"battleships_{i}",
									overwrites=p1, category=games)
		await chan0.send(self.display_welcome_msg(users, sec))
		await chan1.send(self.display_welcome_msg(tmp_cpy, sec))
		del(tmp_cpy)
		# Append to dict
		if gid not in self.active_channels.keys():
			self.active_channels[gid] = {}
			self.active_channels[gid][str(users[0].id)] = {}
			self.active_channels[gid][str(users[1].id)] = {}
		self.active_channels[gid][str(users[0].id)] = {
			"channel": chan0,
			"bo": [ -1 ] * 101,
			"boats": [],
			"ready": False,
			"message": None
		}
		self.active_channels[gid][str(users[1].id)] = {
			"channel": chan1,
			"bo": [ -1 ] * 101,
			"boats": [],
			"ready": False,
			"message": None
		}


	async def get_player_boats(self, ctx, players: list, index: int, layer: Image.Image, half: tuple, t: int):

		def check(m):
			return m.channel == chan and m.author == player

		player = players[index]
		boats = self.active_channels[str(ctx.guild.id)][str(player.id)]["boats"]
		try:
			while len(boats) != 5:
				span = None
				answer = ""
				boat = 0
				msg = self.active_channels[str(ctx.guild.id)][str(player.id)]["message"]
				chan = self.active_channels[str(ctx.guild.id)][str(player.id)]["channel"]
				board = self.active_channels[str(ctx.guild.id)][str(player.id)]["bo"]
				# Get ranges
				while not span:
					try:
						msg = await chan.send(file=dis.File(open(f"tmp/bts_{player.id}.png", "rb")))
						await chan.send("```Where do you want place your next boat?```")
						msg1 = await self.bot.wait_for("message", check=check, timeout=t)
						span = self.parse_range(msg1.content)
						if span is not None:
							for i in span:
								if board[i] != -1:
									span = None
									await chan.send("Error, a boat is already placed there.")
									break
						else:
							await chan.send("Input error.")
						await msg.delete()
					except asyncio.TimeoutError:
						pass

				# Check for (existing) boats
				if span is not None:
					if len(span) == 2:
						boat = 1
					elif len(span) == 3:
						boat = (2, 3)[2 in boats]
					elif len(span) == 4:
						boat = 4
					elif len(span) == 5:
						boat = 5
					if boat in boats:
						boat = 0
						await chan.send("This boat is already placed!")
					# Place boat markers on temporary img
					if boat != 0:
						self.place_boat_on_grid(layer, half, player, span)
						msg = await chan.send(file=dis.File(open(f"tmp/bts_{player.id}_tmp.png", "rb")))
						# get confirmation
						while answer == "" or (answer != "yes" and answer != "y" and answer != "no" and answer != "n"):
							try:
								#while answer == "" or :
								msg3 = await chan.send(
									"```asciidoc\n"
									"= Do you want to place you boat here? =\n\n"
									"[answer with (yes / no)]```")
								msg2 = await self.bot.wait_for("message", check=check, timeout=60)
								if msg2 is not None:
									answer = msg2.content.lower()
								if answer == "no" or answer == "n":
									await msg3.delete()
									continue
								elif answer == "yes" or answer == "y":
									await msg3.delete()
									break
								await msg3.delete()
							except asyncio.TimeoutError:
								continue
						await msg.delete()

				# Place boat (permanently)
				if answer == "yes" or answer == "y":
					for i in span:
						board[i] = boat
					boats.append(boat)
					new = Image.open(f"tmp/bts_{player.id}_tmp.png")
					new.save(f"tmp/bts_{player.id}.png", "PNG", quality=100)
					await chan.send("Boat placed.", delete_after=10)
					self.remove(f"tmp/bts_{player.id}_tmp.png")


			self.active_channels[str(ctx.guild.id)][str(player.id)]["ready"] = True
			await chan.send("Waiting for opponent to finish placing their boats.")
				#return 0
		except:
			return None

	async def input_player_move(self, ctx, turn, input: str, u: list, grid: Image.Image, halves: list):
		tenth = int(grid.width/11)

		# definition for game's namespace
		f0 = f"tmp/bts_{u[0].id}.png"
		f1 = f"tmp/bts_{u[1].id}.png"
		chan0 = self.active_channels[str(ctx.guild.id)][str(u[0].id)]["channel"]
		chan1 = self.active_channels[str(ctx.guild.id)][str(u[1].id)]["channel"]
		chan = (chan0, chan1)[turn == 1]
		board0 = self.active_channels[str(ctx.guild.id)][str(u[0].id)]["bo"]
		board1 = self.active_channels[str(ctx.guild.id)][str(u[1].id)]["bo"]
		board = (board0, board1)[turn == 0]
		# definition for images
		ret0_img = Image.open(f"tmp/bts_{u[0].id}.png")
		ret1_img = Image.open(f"tmp/bts_{u[1].id}.png")
		tmp0 = Image.open(f"data/assets/bts/water_sprite.png")
		tmp1 = Image.open(f"data/assets/bts/explo_sprite.png")
		tmp0 = tmp0.resize((tenth, tenth), Image.ANTIALIAS)
		tmp1 = tmp1.resize((tenth, tenth), Image.ANTIALIAS)
		sprite = Image.new("RGBA", (tenth, tenth), (0, 0, 0, 0))
		# definition for local variables
		i = 0
		ret = ""
		charset_accept = "0123456789abcdefghij"

		# basic checks
		while i < len(input):
			if input[i] not in charset_accept:
				return await chan.send("Input error.")
			i += 1
		# check for input errors
		if (len(input) == 3 and "10" not in input) or	\
		   (len(input) == 2 and "0" in input) or		\
		   (not 1 < len(input) < 4) or					\
		   input.isalpha() or 							\
		   input.isdigit():
			return await chan.send("Input error.")
		# re-arrange list with letter as 0-index
		if len(input) == 2 and input[0].isdigit():
			ret = input[1] + input[0]
		elif len(input) == 3 and input[0:2].isdigit():
			ret = input[2] + input[0:2]
		else:
			ret = input


		# get board index from given input
		ind = (ord(ret[0]) - 97)*10 + int(ret[1:])
		#print(f"player {u[turn]}\nplayed {ret}\nindex {ind}\n")

		# check if already sunk
		if board[ind] == 0:
			return await chan.send("Already played there !")

		# 0 if water else 1 if boat
		state = (0, 1)[0 < board[ind] < 6]
		# get sprite accordingly
		if state:
			sprite.paste(tmp1, (0, 0))
		else:
			sprite.paste(tmp0, (0, 0))
		# set board cell to '0'
		board[ind] = 0

		y = int(ind / 10)
		x = ind - y * 10
		if x == 0:
			x = 10
			y -= 1


		grid.paste(sprite, (tenth*x, tenth*(y+1)), mask=sprite)
		if turn == 0:
			ret0_img.paste(grid, halves[1], mask=grid)
			ret1_img.paste(grid, halves[0], mask=grid)
			ret0_img.save(f0, "PNG", quality=100)
			ret1_img.save(f1, "PNG", quality=100)
		else:
			ret0_img.paste(grid, halves[0], mask=grid)
			ret1_img.paste(grid, halves[1], mask=grid)
			ret0_img.save(f0, "PNG", quality=100)
			ret1_img.save(f1, "PNG", quality=100)

		if self.active_channels[str(ctx.guild.id)][str(u[0].id)]["message"] is not None:
			await self.active_channels[str(ctx.guild.id)][str(u[0].id)]["message"].delete()
		if self.active_channels[str(ctx.guild.id)][str(u[1].id)]["message"] is not None:
			await self.active_channels[str(ctx.guild.id)][str(u[1].id)]["message"].delete()
		self.active_channels[str(ctx.guild.id)][str(u[0].id)]["message"] = await chan0.send(file=dis.File(open(f0, "rb")))
		self.active_channels[str(ctx.guild.id)][str(u[1].id)]["message"] = await chan1.send(file=dis.File(open(f1, "rb")))
		if turn == 0:
			await chan1.send("{} played **`{}`**!".format(u[turn].name, ret.upper()))
		else:
			await chan0.send("{} played **`{}`**!".format(u[turn].name, ret.upper()))
		return None



	async def start_game(self, ctx, users: list):
		# 5 mins
		t = 300
		# Appende playing users
		for u in users:
			self.playing.append(u)
		# Init images
		grid, half0, half1 = self.init_image(users)
		# Init channels
		await self.init_channel(ctx, users, t)

		gid = str(ctx.guild.id)
		chan0 = self.active_channels[gid][str(users[0].id)]["channel"]
		chan1 = self.active_channels[gid][str(users[1].id)]["channel"]

		loop = asyncio.get_event_loop()
		tasks = [asyncio.ensure_future(self.get_player_boats(ctx, users, 0, grid, half0, t), loop=loop),
				 asyncio.ensure_future(self.get_player_boats(ctx, users, 1, grid, half0, t), loop=loop)]

		try:
			async with timeout(10):
				loop.run_until_complete(asyncio.gather(*tasks, loop=loop))
		except:
			pass

		i = 0
		# Start only when both players are ready
		while not														\
			 (self.active_channels[gid][str(users[0].id)]["ready"] and	\
			  self.active_channels[gid][str(users[1].id)]["ready"]):
			if i == t:
				await self.delete_channel(chan0, 10)
				return await self.delete_channel(chan1, 10)
			await asyncio.sleep(1)
			i += 1
		msg0 = await chan0.send("**__All players ready !__**", file=dis.File(open(f"tmp/bts_{users[0].id}.png", "rb")))
		msg1 = await chan1.send("**__All players ready !__**", file=dis.File(open(f"tmp/bts_{users[1].id}.png", "rb")))

		# --------------------
		game_is_playing = True
		turn = random.randint(0, 1)
		amount = 5000
		t1 = 180

		while game_is_playing:

			def check(msg):
				return msg.author == users[turn] and	\
					   msg.channel == self.active_channels[str(ctx.guild.id)][str(users[turn].id)]["channel"]

			ret = 0
			m0 = "It's your turn to play {}, you have {} seconds to make your move.".format(
				users[turn].mention, t1)
			m1 = "It's {}'s turn to play.".format(users[turn])
			await chan0.send((m1, m0)[turn == 0])
			await chan1.send((m0, m1)[turn == 0])
			while ret is not None:
				try:
					grid_cpy = grid.copy()
					msg = await self.bot.wait_for("message", check=check, timeout=t1)
					msg = msg.content.lower().strip()
					ret = await self.input_player_move(ctx, turn, msg, users, grid_cpy, [half0, half1])
				except asyncio.TimeoutError:
					await chan0.send("Game was cancelled because {} is afk.".format(users[turn].mention))
					await chan1.send("Game was cancelled because {} is afk.".format(users[turn].mention))
					game_is_playing = False
					break

			if msg0 is not None or msg1 is not None:
				await msg0.delete()
				await msg1.delete()
				msg0 = msg1 = None
			bo = self.active_channels[gid][str(users[(0, 1)[turn == 0]].id)]["bo"]
			if self.is_winner(bo):
				m2 = ("Congratulations {}! You have won the game!\n"
					  "{} has been awarded `{}`$.").format(
							users[turn].mention, users[turn], amount)
				m3 = ("Too bad {}, you have lost the game...\n"
					  "{} has been awarded `{}`$.").format(
							users[(0, 1)[turn == 0]].mention, users[(0, 1)[turn == 0]], int(amount/2))
				self.reward_player(users[turn], amount)
				self.reward_player(users[(0, 1)[turn == 0]], int(amount/2))
				await chan0.send((m3, m2)[turn == 0])
				await chan1.send((m2, m3)[turn == 0])
				game_is_playing = False

			# Change turn
			turn = (0, 1)[turn == 0]


		# Remove playing users
		for u in users:
			self.playing.remove(u)
		# Remove tmp chan
		await self.delete_channel(chan0, 30)
		await self.delete_channel(chan1, 30)









def setup(bot):
	bot.add_cog(Games(bot))
