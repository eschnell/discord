from discord.ext    import commands as cmds

from cogs.utils.perms	import *
from cogs.utils.locale	import get_user_locale

import discord as dis
import requests
import urllib.parse
import os

#import pprint
#pp = pprint.PrettyPrinter(indent=4)


class BugTracker(cmds.Cog):
	""" A bug report utility module. """

	def __init__(self, bot):
		self.bot = bot
		self.gitlab = self.bot.apis["gitlab"]
		self.headers = {"PRIVATE-TOKEN": self.gitlab}
		self.repo_name = "discordbot"			# repo name in gitlab
		self.issues_link = self.find_link()		# cache it

	def get_gitlab_userid(self, name: str):
		r = requests.get(f"https://gitlab.com/api/v4/users?username={name}")		# get user_id
		if r.status_code != 200:
			r.close()
			del(r)
			return f"Didn't find `{name}` user."
		r.close()
		return r.json()[0]["id"]

	def get_gitlab_repoid(self):
		body_data = {
			"search": self.repo_name,
			"owned": True,
			"simple": True
		}
		r = requests.get(f"https://gitlab.com/api/v4/projects",
						 data=body_data,
						 headers=self.headers)
		if r.status_code != 200:
			r.close()
			del(r)
			return
		r.close()
		return r.json()[0]["id"]

	def find_link(self):
		"""Find issues link from repo name"""
		uid = self.get_gitlab_userid("eschnell")									# gitlab username here
		r = requests.get(f"https://gitlab.com/api/v4/users/{uid}/projects",
						 headers=self.headers)										# get repo projects
		if r.status_code != 200:
			return
		repos = r.json()
		r.close()
		del(r)
		issues_link = None
		i, rlen = 0, len(repos)
		while i < rlen:
			if repos[i]["name"] == self.repo_name:
				issues_link = repos[i]["_links"]["issues"]
				break
			i +=1
		del(repos)
		if issues_link is None:
			raise dis.DiscordException("Repository {self.repo_name} not found!")
		return issues_link

	async def upload_file(self, ctx):
		"""Upload file to an issue"""
		i = 0
		async for m in ctx.channel.history():
			if i > 3:
				return None
			if m.author == ctx.author and len(m.attachments) == 1:
				await m.attachments[0].save("screenshot.jpg")
				with open("screenshot.jpg", "rb") as f:
					files = {"file": ("screenshot.jpg", f, "Content-Type: multipart/form-data")}
					r = requests.post(f"https://gitlab.com/api/v4/projects/{self.get_gitlab_repoid()}/uploads",
									  files=files,
									  headers=self.headers)
				os.remove("screenshot.jpg")
				return r.json()["markdown"]
			i += 1



	@cmds.group(name="issue", no_pm=True, aliases=["i"])
	async def issue_(self, ctx):
		"""Issues options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@issue_.group(name="list", aliases=["li"])
	async def list_(self, ctx, name: str = None):
		"""List all opened issues."""
		#if not name:
		_ = get_user_locale("bugtracker", ctx.author)
		r = requests.get(self.issues_link, headers=self.headers)
		if r.status_code != 200:
			return await ctx.send(_("Something went wrong !"))
		i, ret = 0, r.json()
		r.close()
		del(r)
		if len(ret) == 0:
			await ctx.send(_("No new issues at the moment!"))
			return await ctx.send(":slight_smile:")
		em = (dis.Embed(colour=dis.Color.from_rgb(255, 140, 0),
						description=_("List of all opened issues."))
				.set_thumbnail(url="https://about.gitlab.com/images/press/logo/png/gitlab-logo-white-stacked-rgb.png"))
		while i < len(ret):
			title = ("=> ⛔ ", "=> ✅ ")[ret[i]["state"] == "opened"]
			title += f"{ret[i]['iid']}# {ret[i]['title'].capitalize()}"
			#print(f"descr1: |{ret[i]['description']}|")
			description = ret[i]["description"].split("\n\n![")[0]
			#print(f"descr2: |{description}|")
			em.add_field(name=title,
						 value=f"[{description}]({ret[i]['web_url']})",
						 inline=False)
			i += 1
		return await ctx.send(embed=em)

	@issue_.group(name="open", aliases=["op"])
	async def open_(self, ctx, id: int):
		"""Re-open an issue"""
		_ = get_user_locale("bugtracker", ctx.author)
		link = f"{self.issues_link}/{id}"
		form = {"state_event": "reopen"}
		r = requests.put(link,
						 headers=self.headers,
						 data=form)
		r.close()
		if r.status_code != 200:
			del(r)
			return await ctx.send(_("Issue #{} does not exist!").format(id))
		del(r)
		await ctx.send(_("Issue #{} Successfully re-opened.").format(id))

	@issue_.group(name="close", aliases=["cl"])
	@cmds.check(is_dev)
	async def close_(self, ctx, id: int):
		"""Close an issue"""
		_ = get_user_locale("bugtracker", ctx.author)
		link = f"{self.issues_link}/{id}"
		form = {"state_event": "close"}
		r = requests.put(link,
						 headers=self.headers,
						 data=form)
		r.close()
		if r.status_code != 200:
			del(r)
			return await ctx.send(_("Issue #{} does not exist!").format(id))
		del(r)
		await ctx.send(_("Issue #{} Successfully closed.").format(id))

	@issue_.group(name="delete", aliases=["del"])
	@cmds.check(is_me)
	async def delete_(self, ctx, id: int):
		"""Delete an issue"""
		_ = get_user_locale("bugtracker", ctx.author)
		link = f"{self.issues_link}/{id}"
		r = requests.delete(link, headers=self.headers)
		r.close()
		if r.status_code != 204:
			del(r)
			return await ctx.send(_("Issue #{} does not exist!").format(id))
		del(r)
		await ctx.send(_("Issue #{} Successfully deleted.").format(id))

	@cmds.command(aliases=["t"])
	async def bug(self, ctx, title: str, *, description: str):
		"""
		Submit a bug using the bug report utility.

		Parameters
		==========
		title: (str) Brief title for the bug.
		description: (str) Quick description of the bug, and if possible, how to reproduce.
		"""
		_ = get_user_locale("bugtracker", ctx.author)
		tmp = ""
		url = await self.upload_file(ctx)
		author = _(" - by {}#{}").format(ctx.author.name, ctx.author.discriminator)
		async with ctx.channel.typing():
			if url is not None:
				tmp += "\n\n" + url
			form = {
				"title": title.strip() + author,
				"description": description.strip() + tmp,
				"assignee_id": [int(self.get_gitlab_userid("eschnell"))]}
			r = requests.post(self.issues_link,
							  headers=self.headers,
							  data=form)
			if r.status_code != 201:
				raise dis.errors.DiscordException
			resp = r.json()
		msg = await ctx.send(_("```Bug {} has been successfully submited.```").format(resp['title']))
		await msg.add_reaction("👏")	# clap emoji









def setup(bot):
	bot.add_cog(BugTracker(bot))
