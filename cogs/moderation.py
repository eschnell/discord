from discord.ext		import commands as cmds
from discord.utils		import find
from cogs.utils.perms	import *
from cogs.utils.logger	import Logger
from pymongo			import MongoClient

import discord as dis
import asyncio
import datetime



class Moderation(cmds.Cog):
	"""
	Moderation cog
	"""

	def __init__(self, bot):
		self.bot = bot
		## For Logger module
		self.logname = __name__		# Needed by logger class
		self.log = Logger(self)		# Logger obj
		## For DB socket
		self.dbclient = MongoClient("25.105.87.145", 27717)
		self.db = self.dbclient.moderation

	async def check_mute(self):
		await asyncio.sleep(10)
		while True:
			for post in self.db.muted.find():
				roles = []
				if post["expire_date"] <= int(datetime.datetime.now().strftime("%s")):
					#print("guild_id: {}\n".format(post["guild"]))
					g = self.bot.get_guild(post["guild"])
					u = g.get_member(post["target"])
					for r in post["roles"]:
						role = g.get_role(r)
						roles.append(role)
					await u.edit(roles=roles, reason="Unmuted by threshold.")
					self.db.muted.delete_one({"_id": post["_id"]})
					#print("deleted:", post["_id"], "\n")
				#p = pprint.PrettyPrinter(indent=4)
				#p.pprint(post)
			await asyncio.sleep(1)

	async def muted_role_init(self, ctx):
		"""
		If role doesn't exist, then create it.
		Because giving an inexistant role to someone will
		raise an exception, obviously.
		"""
		muted = None
		name = "@muted"
		for r in ctx.guild.roles:
			if r.name == name:
				muted = r
				break
		if not muted:
			muted = await ctx.guild.create_role(name=name, permissions=dis.Permissions(permissions=66560),	\
				colour=dis.Colour(0x000001), hoist=False, mentionable=False, reason="Needed for /mute cmd")
		return muted

	@cmds.check(is_mod)
	@cmds.command(brief="Mute someone", description="Command to mute pesky users and/or spammers.")
	async def mute(self, ctx, name: dis.Member, mins: int, reason: str = None):
		"""
		Parameters
		==========

		name:   [str] name or id of person to mute
		mins:   [int] number of minutes to mute that user
		reason: [str] (optionnal) reason of mute
		"""
		l = []
		r = name.roles
		muted = await self.muted_role_init(ctx)
		if self.db.muted.find_one({"target": name.id}) is not None:
			return await ctx.send("{} is already muted".format(name))
		if not is_me(ctx):
			for role in name.roles:
				if "mod" in role.name.lower() or "moderation" in role.name.lower():
					return await ctx.send("Cannot mute a moderator.")
				if "dev" in role.name.lower() or "developer" in role.name.lower():
					return await ctx.send("Cannot mute a developer.")
		date = datetime.datetime.now().strftime("%s")
		msg = "{} muted {} for {} minutes".format(ctx.author, name, mins)
		if not reason:
			reason = msg

		r.pop(0)			# don't push '@everyone' role to db
		for x in r:
			l.append(x.id)
		roles = {
			"target": name.id,
			"guild": ctx.guild.id,
			"roles": l,
			"expire_date": int(date) + 60 * mins
		}
		# Temporarily stock roles in db
		self.db.muted.insert_one(roles)
		# Affect with `muted` role
		await name.edit(reason=reason, roles=[muted])
		await ctx.send(msg, delete_after=10)

	@cmds.check(is_mod)
	@cmds.command(brief="Unute someone", description="Unute someone")
	async def unmute(self, ctx, name: dis.Member):
		"""
		Parameters
		==========

		name:   [str] name or id of person to unmute
		"""
		g = ctx.guild
		roles = []
		# Get tmp roles back from db
		userinfo = self.db.muted.find_one_and_delete({"$and": [
			{"target": name.id},
			{"guild": g.id}
		]})
		if userinfo is not None:
			for r in userinfo["roles"]:
				role = g.get_role(r)
				roles.append(role)
			await name.edit(roles=roles)
			return await ctx.send("{} has been unmuted.".format(name))
		await ctx.send("User {} is not muted.".format(name), delete_after=10)

	@cmds.check(is_mod)
	@cmds.command(aliases=["mm"])
	async def massmove(self, ctx, from_channel: dis.VoiceChannel, to_channel: dis.VoiceChannel):
		"""
		Parameters
		==========

		from_channel:  [int] id of initial channel to move members
		to_channel:       [int] id of destination channel to move members
		"""
		try:
			u = ctx.author
			i = 0
			members_nb = len(from_channel.members)
			self.log.info(f"User {u} with UID: {u.id} requested `mm`. Starting move on VC_ID: {from_channel.id}")
			self.log.debug("Getting copy of current list to move")
			for member in from_channel.members:
				await member.move_to(to_channel, reason="Massmove")
				self.log.debug(f"Member {member} with UID: {member.id} moved to channel {to_channel.id}")
				i += 1
				await asyncio.sleep(0.05)
		except dis.Forbidden:
			self.log.warn(f"Forbidden - No permissions to move {member} with UID: {member.id}")
			await ctx.send("I have no permission to move members.")
		except Exception as e:
			self.log.error(f"Exception occurred: {e}\n") #, stack_info=True, exc_info=False)
			await ctx.send("A error occured. Please try again")
		await ctx.send("Moved `{}/{}` users from <#{}> to <#{}>.".format(i, members_nb, from_channel.id, to_channel.id))

	@cmds.check(is_mod)
	@cmds.command(aliases=["vb"])
	async def viewbans(self, ctx, user: dis.Member = None):
		"""
		Parameters
		==========

		user:    [user] (optionnal)
		"""
		m = "```md\n# List of active bans"
		if user:
			u = find(lambda k: k.user.id == user.id, await ctx.guild.bans())
			return print(u)
		b = await ctx.guild.bans()
		for entry in b:
			reason = (entry.reason, "_None_")[entry.reason is None]
			s = ''.join([ "+ User: ", str(entry.user), "\n> ID: ", str(entry.user.id), "\n- Reason: ", reason])
			m = f"{m}\n\n{s}"
		m = f"{m}```"
		await ctx.send(m)

	@cmds.check(is_mod)
	@cmds.command(aliases=["unban", "deban"])
	async def pardon(self, ctx, user: int, reason: str = None):
		"""
		Parameters
		==========

		user:       [int] ID of user to unban
		reason:  [str] reason for unban (optionnal)
		"""
		user = await self.bot.fetch_user(user)

		await ctx.guild.unban(user, reason=reason)
		self.log.warning(f"User {ctx.author} with UID {ctx.author.id} unbanned {user}")
		await ctx.send("User {} with ID {} successfully unbanned.".format(user, user.id))

	@cmds.check(is_mod)
	@cmds.command(aliases=["hb"])
	async def hackban(self, ctx, user: int, *reason: str):
		"""
		Parameters
		==========

		user:       [int] ID of user to hackban
		reason:  [str] reason for hackban
		"""
		reason = ' '.join(reason).strip()
		user = await self.bot.fetch_user(user)
		on_guild = find(lambda u: u.id == user.id, ctx.guild.members)

		if on_guild:
			return await ctx.send("Cannot hackban this user.")
		await ctx.guild.ban(user, reason=reason, delete_message_days=0)
		self.log.warning(f"User {ctx.author} with UID {ctx.author.id} banned {user}")
		await ctx.send("User {} with ID {} successfully hackbanned.".format(user, user.id))



def setup(bot):
	bot.loop.create_task(Moderation(bot).check_mute())
	bot.add_cog(Moderation(bot))
