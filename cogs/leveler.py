from discord.ext				import commands as cmds
from discord.utils				import find
from pymongo					import MongoClient

from cogs.utils.perms			import *
from cogs.utils.dataIO			import dataIO
from cogs.utils.chat_formatting	import pagify
import cogs.help as h

try:
	from PIL import Image, ImageDraw, ImageFont, ImageColor, ImageOps, ImageFilter
except:
	raise RuntimeError("Can't load pillow. Do 'pip3 install pillow'.")

import aiohttp
import asyncio
import discord as dis
import math
import operator
import os
import platform
import random
import re
import string
import time
import textwrap

#################
import json, pprint

try:
	#client = MongoClient("localhost", 27017)		# debugging db
	client = MongoClient("25.105.87.145", 27717)	# prod db
	db = client.leveler
except:
	print("Can't load database. Follow instructions on Git/online to install MongoDB.")


class Leveler(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, ctx):
		self.bot = ctx
		self.settings = dataIO.load_hjson("config/config_leveler.hjson")
		self.backgrounds = dataIO.load_hjson("data/leveler/backgrounds.json")
		self.badges = dataIO.load_hjson("data/leveler/badges.json")
		self.default_avatar_url = "http://i.imgur.com/XPDO9VH.jpg"
		self.font_file = "data/assets/font/font.ttf"
		self.font_bold_file = "data/assets/font/font_bold.ttf"
		self.font_unicode_file = "data/assets/font/unicode.ttf"
		self.font_thin_file = "data/assets/font/Uni_Sans_Thin.ttf"
		self.font_heavy_file = "data/assets/font/Uni_Sans_Heavy.ttf"
		self.font_reg_file = "data/assets/font/SourceSansPro-Regular.ttf"
		self.font_bold_file = "data/assets/font/SourceSansPro-Semibold.ttf"
		self.looping = []
		#self.bot.add_listener(self.voice_update_hook, "on_voice_state_update")


	@cmds.Cog.listener()
	async def on_voice_state_update(self, member, before, after):
		# check if `after` channel exists
		if after.channel is not None:
			c = after.channel
			g = member.guild

			#print(after)

			# check if channel is not already looped or if channel is != AFK
			if c.id not in self.looping and		\
			   "afk" not in c.name.lower():
				self.looping.append(c.id)
				# Indefinite loop while there is members
				while True:
					if len(c.members) == 0 or (len(c.members) == 1 and c.members[0].bot):
						self.looping.remove(c.id)
						break
					for u in c.members:
						if not u.bot:
							prfx = f"servers.{g.id}"
							await self._create_user(u, g)
							# Process an amount of random xp
							rand_exp = random.randint(4, 30)
							# Process xp and lvl
							userinfo = db.users.find_one({"user_id": u.id})
							exp_frac = userinfo["servers"][str(g.id)]["current_exp"]
							curr_lvl = self._find_level(exp_frac)
							required = self._required_exp(curr_lvl)
							total = exp_frac - self._level_exp(curr_lvl) + rand_exp

							new_xp = exp_frac + rand_exp
							db.users.update_one({"user_id": u.id}, {"$set": {
								f"servers.{g.id}.current_exp": new_xp
							}})
							if total >= required:
								# process if lvl up
								base = 1500
								curr_cred = userinfo["servers"][str(g.id)]["credit"]
								db.users.update_one({"user_id": u.id}, {"$set": {
									f"{prfx}.level": self._find_level(new_xp),
									f"{prfx}.credit": curr_cred + base + base * int(self._find_level(exp_frac) // 10),
								}})
					await asyncio.sleep(random.randint(20, 90))


# ===================================================
#	COMMANDS PART
# ===================================================

	@cmds.check(is_me)
	@cmds.command(hidden=True)
	async def apply(self, ctx):
		#pp = pprint.PrettyPrinter(indent=2)
		all_users = db.users.find({})

		#for u in all_users:
			#msg = "```python\n"
			#msg += json.dumps(str(u), indent=4, separators=(",", ":")).replace(", ", ",\n").replace("{", "{\n").replace("}", "\n}")
			#msg += "```"
			#await ctx.send(msg)
		""" db.users.update_many({}, {"$unset": {
			"profile_block": "",
			"rank_block": "",
			"rep_block": ""
		}}) """

	#@cmds.check(is_me)
	@cmds.command(hidden=True, aliases=["sp"])
	async def show_profile(self, ctx, user: dis.Member):
		userinfo = db.users.find_one({"user_id": user.id})
		if userinfo is not None:
			s = pprint.pformat(userinfo["servers"][str(ctx.guild.id)], indent=2)
			await ctx.send("```python\n" + s + "\n```")
		else:
			await ctx.send(f"{user} not found.")

	@cmds.cooldown(1, 2, cmds.BucketType.user)
	@cmds.command(name="balance", no_pm=True, aliases=["bal"])
	async def balance(self, ctx, *, user: dis.Member = None):
		"""Displays a user balance."""
		if user == None:
			user = ctx.message.author
		g = user.guild

		# creates user if doesn't exist
		await self._create_user(user, g)
		userinfo = db.users.find_one({"user_id": user.id})
		name = (user.name + " has", "You have")[user == ctx.message.author]
		credit_thousd = f'{int(userinfo["servers"][str(g.id)]["credit"]):,}'
		await ctx.send(f"```asciidoc\n{name} :: {credit_thousd} $```")

	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command(name="pay", no_pm=True, aliases=["give"])
	async def pay(self, ctx, user: dis.Member, amount: int):
		"""Give $$$ to another player."""
		org_user = ctx.message.author
		g = user.guild

		# creates users if doesn't exist
		await self._create_user(user, user.guild)
		await self._create_user(org_user, user.guild)
		# can't steal money è_é
		if amount <= 0:
			return await ctx.send(f"Error. Can't pay {amount}$ !")
		# can't pay yourself
		if org_user.id == user.id:
			return await ctx.send("You can't pay yourself!")
		org_userinfo = db.users.find_one({"user_id": org_user.id})
		userinfo = db.users.find_one({"user_id": user.id})
		org_g_info = org_userinfo["servers"][str(g.id)]
		g_info = userinfo["servers"][str(g.id)]
		# check balance
		if amount > org_g_info["credit"]:
			return await ctx.send(f"You don't have enough to pay {user.name} ${amount} !")
		# update balances
		db.users.update_one({"user_id": org_user.id}, {"$set":{
			f"servers.{g.id}.credit": org_g_info["credit"] - amount,
		}})
		db.users.update_one({"user_id": user.id}, {"$set":{
			f"servers.{g.id}.credit": g_info["credit"] + amount,
		}})
		await ctx.send(f"**You successfully sent ${amount} to {user.name}.**")


	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command(name="profile", no_pm=True, aliases=["prof"])
	async def profile(self, ctx, *, user: dis.Member = None):
		"""Displays a user profile."""
		await asyncio.sleep(0.5)
		if user == None:
			user = ctx.message.author
		channel = ctx.message.channel
		g = user.guild
		curr_time = time.time()

		# creates user if doesn't exist
		await self._create_user(user, g)
		userinfo = db.users.find_one({"user_id": user.id})
		# check if disabled
		if self._disabled(g):
			return await ctx.send("**Leveler commands for this server are disabled!**")
		# no cooldown for text only
		if "text_only" in self.settings and g.id in self.settings["text_only"]:
			em = await self.profile_text(user, g, userinfo)
			await ctx.send(content=None, embed=em)
		else:
			await self.draw_profile(user, g)
			async with channel.typing():
				await channel.send(
					content=f"**User profile for {user.name + '#' + user.discriminator}**",
					file=dis.File(open(f"data/leveler/tmp/{user.id}_profile.png", "rb"))) #self._is_mention(user)))
				db.users.update_one({"user_id": user.id}, {"$set": {
					f"servers.{g.id}.profile_block": curr_time,
				}}, upsert = True)
			try:
				os.remove(f"data/leveler/tmp/{user.id}_profile.png")
			except:
				pass

	@cmds.cooldown(1, 5, cmds.BucketType.user)
	@cmds.command(pno_pm=True)
	async def rank(self, ctx, user: dis.Member = None):
		"""Displays the rank of a user."""
		if user == None:
			user = ctx.message.author
		ch = ctx.message.channel
		g = user.guild
		curr_time = time.time()

		# creates user if doesn't exist
		await self._create_user(user, g)
		userinfo = db.users.find_one({"user_id": user.id})
		# check if disabled
		if self._disabled(g):
			return await ctx.send("**Leveler commands for this server are disabled!**")

		# no cooldown for text only
		#if "text_only" in self.settings and g.id in self.settings["text_only"]:
		#	em = await self.rank_text(user, g, userinfo)
		#	await ctx.send(content=None, embed=em)
		#else:
		await self.draw_rank(user, g)
		async with ch.typing():
			await ctx.send(
				content=f"**Ranking & Statistics for {user.name}#{user.discriminator}**",
				file=dis.File(open(f"data/leveler/tmp/{user.id}_rank.png", "rb"))) #self._is_mention(user)))
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"servers.{g.id}.rank_block": curr_time,
			}}, upsert=True)
		try:
			os.remove(f"data/leveler/tmp/{user.id}_rank.png")
		except:
			pass

	@cmds.group(name="lvlset", no_pm=True, aliases=["levelset"])
	async def lvlset(self, ctx):
		"""Configuration options"""
		if ctx.invoked_subcommand is None:
			return await ctx.send_help(ctx.command)

	@lvlset.group(name="profile", no_pm=True)
	async def profileset(self, ctx):
		"""Profile options"""
		if ctx.invoked_subcommand is None or isinstance(ctx.invoked_subcommand, cmds.Group):
			return await ctx.send_help(ctx.command)

	@lvlset.group(name="rank", no_pm=True)
	async def rankset(self, ctx):
		"""Rank options"""
		if ctx.invoked_subcommand is None or isinstance(ctx.invoked_subcommand, cmds.Group):
			return await ctx.send_help(ctx.command)

	@profileset.command(no_pm=True)
	async def info(self, ctx, *, info):
		"""Set your user info."""
		user = ctx.message.author
		g = ctx.message.guild
		await self._create_user(user, g)
		max_char = 150
		if self._disabled(g):
			return await ctx.send("Leveler commands for this server are disabled.")
		if len(info) < max_char:
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"servers.{g.id}.profile_info": info
			}})
			await ctx.send("**Your info section has been succesfully set!**")
		else:
			await ctx.send(f"**Your description has too many characters! Must be <{max_char}**")

	@profileset.command(name="bg", no_pm=True)
	async def profilebg(self, ctx, *, image_name: str):
		"""Set your profile background"""
		image_name = image_name.lower()
		user = ctx.message.author
		g = ctx.message.guild
		await self._create_user(user, g)
		if self._disabled(g):
			return await ctx.send("Leveler commands for this server are disabled.")
		if "text_only" in self.settings and g.id in self.settings["text_only"]:
			return await ctx.send("**Text-only commands allowed.**")
		if image_name in self.backgrounds["profile"].keys():
			if await self._process_purchase(ctx, "profile", image_name):
				db.users.update_one({"user_id": user.id}, {"$set": {
					f"servers.{g.id}.profile_background": self.backgrounds["profile"][image_name]
				}})
				await ctx.send("**Your new profile background has been succesfully set!**")
		else:
			prf = await self.bot.get_prefix(ctx.message)
			await ctx.send(f"That is not a valid bg. See available bgs at `{prf}bgs profile`")

	@rankset.command(name="bg", no_pm=True)
	async def rankbg(self, ctx, *, image_name: str):
		"""Set your rank background"""
		user = ctx.message.author
		g = ctx.message.guild
		await self._create_user(user, g)
		if self._disabled(g):
			return await ctx.send("Leveler commands for this server are disabled.")
		if "text_only" in self.settings and g.id in self.settings["text_only"]:
			return await ctx.send("**Text-only commands allowed.**")
		if image_name in self.backgrounds["rank"].keys():
			if await self._process_purchase(ctx, "rank", image_name):
				db.users.update_one({"user_id": user.id}, {"$set": {
					f"servers.{g.id}.rank_background": self.backgrounds["rank"][image_name]
				}})
				await ctx.send("**Your new rank background has been succesfully set!**")
		else:
			prf = await self.bot.get_prefix(ctx.message)
			await ctx.send(f"That is not a valid bg. See available bgs at `{prf}bgs rank`")

	@cmds.command(name="backgrounds", no_pm=True, aliases=["bgs"])
	async def disp_backgrounds(self, ctx, type: str = None):
		"""Gives a list of backgrounds. [p]backgrounds [profile|rank|levelup]"""
		g = ctx.message.guild
		user = ctx.message.author
		max_all = 18
		if self._disabled(g):
			return await ctx.send("**Leveler commands for this server are disabled!**")

		em = dis.Embed(colour=user.colour)
		if not type:
			em.set_author(name=f"All Backgrounds for {self.bot.user.name}", icon_url=self.bot.user.avatar_url)

			for category in self.backgrounds.keys():
				bg_url = []
				for background_name in sorted(self.backgrounds[category].keys()):
					bg_url.append(f"[{background_name}]({self.backgrounds[category][background_name]})")
				max_bg = min(max_all, len(bg_url))
				bgs = ", ".join(bg_url[0:max_bg])
				if len(bg_url) >= max_all:
					bgs += "..."
				em.add_field(name = category.upper(), value=bgs, inline=False)
			await ctx.send(content=None, embed=em)
		else:
			if type.lower() == "profile":
				em.set_author(name=f"Profile Backgrounds for {self.bot.user.name}", icon_url=self.bot.user.avatar_url)
				bg_key = "profile"
			elif type.lower() == "rank":
				em.set_author(name=f"Rank Backgrounds for {self.bot.user.name}", icon_url=self.bot.user.avatar_url)
				bg_key = "rank"
			elif type.lower() == "levelup":
				em.set_author(name=f"Level Up Backgrounds for {self.bot.user.name}", icon_url=self.bot.user.avatar_url)
				bg_key = "levelup"
			else:
				bg_key = None

			if bg_key:
				bg_url = []
				for background_name in sorted(self.backgrounds[bg_key].keys()):
					price = self.settings["bg_price"][bg_key][background_name]
					bg_url.append(f"[{background_name}]({self.backgrounds[bg_key][background_name]}): price ${price}")
				bgs = '\n'.join(bg_url)

				total_pages = 0
				for page in pagify(bgs, [" "]):
					total_pages +=1

				counter = 1
				for page in pagify(bgs, [" "]):
					em.description = page
					em.set_footer(text=f"Page {counter} of {total_pages}")
					await ctx.send(content=None, embed=em)
					counter += 1
			else:
				await ctx.send("**Invalid Background Type. (profile, rank, levelup)**")


	@cmds.check(is_admin)
	@cmds.group(aliases=["lvla"])
	async def lvladmin(self, ctx):
		"""Admin Toggle Features"""
		if ctx.invoked_subcommand is None:
			await ctx.send_help(ctx.command)

	@cmds.check(is_admin)
	@lvladmin.group()
	async def overview(self, ctx):
		"""A list of settings"""
		user = ctx.message.author
		disabled_servers = []
		private_levels = []
		disabled_levels = []
		locked_channels = []

		for server in self.bot.guilds:
			gid = str(server.id)
			if "disabled_servers" in self.settings.keys() and gid in self.settings["disabled_servers"]:
				disabled_servers.append(server.name)
			if "lvl_msg_lock" in self.settings.keys() and gid in self.settings["lvl_msg_lock"].keys():
				for channel in server.channels:
					if self.settings["lvl_msg_lock"][gid] == channel.id:
						locked_channels.append(f"\n{server.name} → #{channel.name}")
			if "lvl_msg" in self.settings.keys() and gid in self.settings["lvl_msg"]:
				disabled_levels.append(server.name)
			if "private_lvl_msg" in self.settings.keys() and gid in self.settings["private_lvl_msg"]:
				private_levels.append(server.name)
		num_users = 0
		for i in db.users.find({}):
			del(i)
			num_users += 1
		msg = ""
		msg += f"**Servers:** {len(self.bot.guilds)}\n"
		msg += "**Unique Users:** {num_users}\n"
		if "mention" in self.settings.keys():
			msg += f"**Mentions:** {str(self.settings['mention'])}\n"
		bgs_price = json.dumps(self.settings["bg_price"], indent=1)
		msg += f"**Background Price:** {bgs_price}\n"
		if "badge_type" in self.settings.keys():
			msg += f"**Badge type:** {self.settings['badge_type']}\n"
		msg += f"**Disabled Servers:** {', '.join(disabled_servers)}\n"
		msg += f"**Enabled Level Messages:** {', '.join(disabled_levels)}\n"
		msg += f"**Private Level Messages:** {', '.join(private_levels)}\n"
		msg += f"**Channel Locks:** {', '.join(locked_channels)}\n"
		em = dis.Embed(description=msg, colour=user.colour)
		em.set_author(name=f"Settings Overview for {self.bot.user.name}")
		await ctx.send(content=None, embed=em)

	@lvladmin.group(aliases=["b"])
	async def badge(self, ctx):
		"""Badge Configuration Options"""
		if ctx.invoked_subcommand is None:
			await ctx.send_help(ctx.command)

	@badge.command(name="available", no_pm=True, aliases=["list"])
	async def available(self, ctx, global_badge: str = None):
		"""Get a list of available badges for server or 'global'."""
		user = ctx.message.author
		server = ctx.message.guild

		# get server stuff
		ids = [("global", "Global", self.bot.user.avatar_url), (server.id, server.name, server.icon_url)]

		title_text = "**Available Badges**"
		index = 0
		for serverid, servername, icon_url in ids:
			em = dis.Embed(colour=user.colour)
			em.set_author(name=f"{servername}", icon_url=icon_url)
			msg = ""
			server_badge_info = db.badges.find_one({"server_id": serverid})
			if server_badge_info:
				server_badges = server_badge_info["badges"]
				for badgename in server_badges:
					badgeinfo = server_badges[badgename]
					if badgeinfo["price"] == -1:
						price = "Non-purchasable"
					elif badgeinfo["price"] == 0:
						price = "Free"
					else:
						price = badgeinfo["price"]

					msg += f"**• {badgename}** ({price}) - {badgeinfo['description']}\n"
			else:
				msg = "None"

			em.description = msg
			total_pages = 0
			for page in pagify(msg, ["\n"]):
				total_pages += 1
			counter = 1
			for page in pagify(msg, ["\n"]):
				if index == 0:
					await ctx.send(title_text, embed=em)
				else:
					await ctx.send(embed=em)
				index += 1

				em.set_footer(text=f"Page {counter} of {total_pages}")
				counter += 1

	@badge.command(name="add", no_pm=True)
	async def addbadge(self, ctx, name: str, bg_img: str, border_color: str, priority: int, price: int, *, description: str):
		"""
		Add a badge.

		user = Badge name
		bg_img = url
		border_color = #hex
		priotity =
		price = -1(non-purchasable), 0, ...
		"""
		user = ctx.message.author
		server = ctx.message.guild

		# check members
		required_members = 25
		members = 0
		for member in server.members:
			if not member.bot:
				members += 1

		if user.id == self.bot.owner_id:
			pass
		elif members < required_members:
			return await ctx.send(f"**You may only add badges in servers with {required_members}+ non-bot members**")

		if "-global" in description and user.id == self.bot.owner_id:
			description = description.replace("-global", "")
			serverid = "global"
			servername = "global"
		else:
			serverid = server.id
			servername = server.name

		if '.' in name:
			return await ctx.send("**Name cannot contain `.`**")
		if not await self._valid_image_url(bg_img):
			return await ctx.send("**Background is not valid. Enter hex or image url!**")
		if not self._is_hex(border_color):
			return await ctx.send("**Border color is not valid!**")
		if price < -1:
			return await ctx.send("**Price is not valid!**")
		if priority == 0:
			return await ctx.send("**0 priority is a special reserved slot!**")
		if len(description.split(' ')) > 40:
			return await ctx.send("**Description is too long! <=40**")

		badges = db.badges.find_one({"server_id": serverid})
		if not badges:
			db.badges.insert_one({"server_id": serverid,
				"badges": {}})
			badges = db.badges.find_one({"server_id": serverid})

		new_badge = {
				"badge_name": name,
				"bg_img": bg_img,
				"price": price,
				"description": description,
				"border_color": border_color,
				"server_id": serverid,
				"server_name": servername,
				"priority_num": 0
			}

		if name not in badges["badges"].keys():
			# create the badge regardless
			badges["badges"][name] = new_badge
			db.badges.update_one({"server_id": serverid}, {"$set": {
				"badges": badges["badges"]
			}})
			await ctx.send(f"**`{name}` Badge added in `{servername}` server.**")
		else:
			# update badge in the server
			badges["badges"][name] = new_badge
			db.badges.update_one({"server_id":serverid}, {"$set": {
				"badges": badges["badges"]
			}})

			# go though all users and update the badge. Doing it this way because dynamic does more accesses when doing profile
			for user in db.users.find({}):
				try:
					user = self._badge_convert_dict(user, server)
					userbadges = user["badges"]
					badge_name = f"{name}_{serverid}"
					if badge_name in userbadges.keys():
						user_priority_num = userbadges[badge_name]["priority_num"]
						new_badge["priority_num"] = user_priority_num # maintain old priority number set by user
						userbadges[badge_name] = new_badge
						db.users.update_one({"user_id": user["user_id"]}, {"$set": {
							"badges": userbadges
						}})
				except:
					pass
			await ctx.send(f"**The `{name}` badge has been updated**")

	@badge.command(name="delete", no_pm=True, aliases=["del"])
	async def delbadge(self, ctx, *, name: str):
		"""Delete a badge and remove from all users."""
		user = ctx.message.author
		channel = ctx.message.channel
		server = user.guild

		if "-global" in name and user.id == self.bot.owner_id:
			name = name.replace(" -global", "")
			serverid = "global"
		else:
			serverid = server.id

		# creates user if doesn't exist
		await self._create_user(user, server)
		userinfo = db.users.find_one({"user_id": user.id})
		userinfo = self._badge_convert_dict(userinfo, server)

		if server.id in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")

		serverbadges = db.badges.find_one({"server_id": serverid})
		if name in serverbadges["badges"].keys():
			del serverbadges["badges"][name]
			db.badges.update_one({"server_id": serverbadges["server_id"]}, {"$set": {
				"badges": serverbadges["badges"]
			}})
			# remove the badge if there
			for user_info_temp in db.users.find({}):
				try:
					user_info_temp = self._badge_convert_dict(user_info_temp, server)

					badge_name = f"{name}_{serverid}"
					if badge_name in user_info_temp["badges"].keys():
						del user_info_temp["badges"][badge_name]
						db.users.update_one({"user_id": user_info_temp["user_id"]}, {"$set": {
							"badges": user_info_temp["badges"]
						}})
				except:
					pass

			await ctx.send(f"**The `{name}` badge has been removed.**")
		else:
			await ctx.send("**That badge does not exist.**")

	@badge.command(no_pm=True)
	async def give(self, ctx, user: dis.Member, name: str):
		"""Give a user a badge with a certain name"""
		org_user = ctx.message.author
		server = org_user.guild

		# creates user if doesn't exist
		await self._create_user(user, server)
		userinfo = db.users.find_one({"user_id": user.id})
		userinfo = self._badge_convert_dict(userinfo, server)

		if server.id in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")

		serverbadges = db.badges.find_one({"server_id": server.id})
		if serverbadges is None:
			return await ctx.send("No badges in this server.")
		badges = serverbadges["badges"]
		badge_name = f"{name}_{server.id}"

		if name not in badges:
			return await ctx.send("**That badge doesn't exist in this server!**")
		elif badge_name in badges.keys():
			return await ctx.send(f"**{self._is_mention(user)} already has that badge!**")
		else:
			userinfo["servers"][str(server.id)]["badges"][badge_name] = badges[name]
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"servers.{server.id}.badges": userinfo["servers"][str(server.id)]["badges"]
			}})
			await ctx.send(f"**{self._is_mention(org_user)} has just given `{self._is_mention(user)}` the `{name}` badge!**")


	@lvladmin.command(aliases=["ud"])
	async def userdel(self, ctx, user_id: int):
		"""Delete a user's profile from DB"""
		query = db.users.find_one_and_delete({"user_id": user_id})
		if query:
			gid = str(ctx.guild.id)
			username = query['servers'][gid]['username']
			return await ctx.send(f"**User {username}'s entry, with id: {user_id} has been deleted.**")
		await ctx.send(f"**User with id: {user_id} not found.**")

	@lvladmin.command(no_pm=True, aliases=["setbg", "bgset"])
	async def setbackground(self, ctx, user: dis.Member, param: str, bg: str):
		"""Set a user's background."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)
		bg = bg.lower()
		param = param.lower()
		if not param in self.settings["bg_price"].keys():
			return await ctx.send(f"**{param.capitalize()}** param doesn't exist!")
		if not bg in self.settings["bg_price"][param].keys():
			return await ctx.send(f"**{bg.capitalize()}** background name doesn't exist!")
		param_field = f"servers.{g.id}.{param}_background"
		db.users.update_one({"user_id": user.id}, {"$set": {
			param_field: self.backgrounds[param][bg]	# <=== BG Link
		}})
		await ctx.send(f"**{self._is_mention(user)}'s {param} background set to: `{bg}`!**")

	@lvladmin.command(no_pm=True, aliases=["setc", "cset", "creditset"])
	async def setcredit(self, ctx, user: dis.Member, credit: int):
		"""Set a user's credit."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)

		if g.id in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")
		db.users.update_one({"user_id": user.id}, {"$set": {f"servers.{g.id}.credit": credit}})
		await ctx.send(f"**{user.name}'s Credit has been set to `{credit}$`.**")

	@lvladmin.command(no_pm=True, aliases=["seti", "iset", "infoset"])
	async def setinfo(self, ctx, user: dis.Member, *, info):
		"""Set a user's info."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)

		if str(g.id) in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")
		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{g.id}.profile_info": info
		}})
		await ctx.send(f"**{user.name}'s info section has been succesfully set!**")

	@lvladmin.command(no_pm=True, aliases=["setlvl", "setl", "lset", "lvlset", "levelset"])
	async def setlevel(self, ctx, user: dis.Member, level: int):
		"""Set a user's level. (What a cheater :c)."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)

		if str(g.id) in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")
		if level < 0:
			return await ctx.send("**Please enter a positive number.**")

		required = self._level_exp(level)
		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{g.id}.level": level,
			f"servers.{g.id}.current_exp": required
		}})
		await ctx.send(f"**{user.name}'s level has been set to `{level}`.**")

	@lvladmin.command(no_pm=True, aliases=["sett", "tset", "titleset"])
	async def settitle(self, ctx, user: dis.Member, *, title: str):
		"""Set a user's title."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)

		if str(g.id) in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")

		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{g.id}.title": title
		}})
		await ctx.send(f"**{user.name}'s title has been set to `{title}`.**")

	@lvladmin.command(no_pm=True, aliases=["gxp"])
	async def givexp(self, ctx, user: dis.Member, xp: int):
		"""Give a user's some xp."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)
		userinfo = db.users.find_one({"user_id": user.id})

		if str(g.id) in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")

		new_xp = userinfo["servers"][str(g.id)]["current_exp"] + xp
		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{g.id}.level": self._find_level(new_xp),
			f"servers.{g.id}.current_exp": new_xp
		}})

	@cmds.check(is_me)
	@lvladmin.command(no_pm=True, aliases=["setp", "pset", "priceset"])
	async def setprice(self, ctx, param: str, bg: str, price: int):
		"""Set a price for background changes."""
		g = ctx.message.guild
		param = param.lower()
		bg = bg.lower()
		if g.id in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")
		if price < 0:
			return await ctx.send("**That is not a valid background price.**")
		if not param in self.settings["bg_price"].keys():
			return await ctx.send(f"**{param.capitalize()}** param doesn't exist!")
		if not bg in self.settings["bg_price"][param].keys():
			return await ctx.send(f"**{bg.capitalize()}** background name doesn't exist!")

		self.settings["bg_price"][param][bg] = price
		dataIO.save_hjson("config/config_leveler.hjson", self.settings)
		await ctx.send(f"**Background price set to: `{price}`!**")

	@lvladmin.command(no_pm=True, aliases=["setrep", "setr", "rset", "repset"])
	async def setreputation(self, ctx, user: dis.Member, rep: int):
		"""Set a user's reputation."""
		g = user.guild
		# creates user if doesn't exist
		await self._create_user(user, g)

		if str(g.id) in self.settings["disabled_servers"]:
			return await ctx.send("Leveler commands for this server are disabled.")
		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{g.id}.rep": rep
		}})
		await ctx.send(f"**{user.name}'s Reputation has been set to `{rep}`.**")


	@cmds.check(is_me)
	@cmds.command(aliases=["rb"], enabled=False)
	async def rep_block(self, ctx, user: dis.Member, delta: float):
		gid = str(ctx.guild.id)
		userinfo = db.users.find_one({"user_id": user.id})

		db.users.update_one({"user_id": user.id}, {"$set": {
			f"servers.{gid}.rep_block": time.time() - delta
		}})

	@cmds.cooldown(1, 2, cmds.BucketType.user)
	@cmds.command(aliases=["rep"])
	async def reputation(self, ctx, user: dis.Member = None):
		"""Gives a reputation point to a designated player."""
		m = ctx.message
		org_user = m.author
		server = org_user.guild
		# creates user if doesn't exist
		await self._create_user(org_user, server)

		if user:
			await self._create_user(user, server)
		if server.id in self.settings["disabled_servers"]:
			return await ctx.send("**Leveler commands for this server are disabled!**")
		org_userinfo = db.users.find_one({"user_id": org_user.id})
		g_org_info = org_userinfo["servers"][str(server.id)]
		curr_time = time.time()

		if user and user.id == org_user.id:
			return await ctx.send("**You can't give a rep to yourself!**")
		if user and user.bot:
			return await ctx.send("**You can't give a rep to a bot!**")
		if "rep_block" not in g_org_info:
			g_org_info["rep_block"] = 0

		delta = float(curr_time) - float(g_org_info["rep_block"])
		#print(f"delta = {delta}\ndelta_mins = {delta/60}\ndelta_hours = {delta/3600}\n\n")

		# calculate points & points threshold
		rep_points = self.settings["max_rep_points"]
		threshold = float(self.settings["rep_cooldown"]) / rep_points
		# calulate time left
		divider, rest = divmod(int(delta), int(threshold))
		if user and delta >= threshold and delta > 0:

			## Formulae to change					<===================
			curr_xp = g_org_info["current_exp"]
			new_xp = 180 + 2 * self._find_level(curr_xp)

			if delta >= threshold and divider < rep_points:
				curr_time -= (delta - threshold)
			else:
				curr_time -= threshold

			db.users.update_one({"user_id": org_user.id}, {"$set": {
				f"servers.{server.id}.rep_block": curr_time
			}})
			userinfo = db.users.find_one({"user_id": user.id})
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"servers.{server.id}.rep": userinfo["servers"][str(server.id)]["rep"] + 1
			}})
			await ctx.send(f"**You have just given {self._is_mention(user)} a reputation point!**")
			await self._process_exp(m, org_userinfo, new_xp)
		else:
			if divider > 0:
				return await ctx.send("**You can give {} rep point(s)!**".format(
					(rep_points, divider)[divider <= rep_points]
				))
			m, s = divmod(threshold - rest, 60)
			h, m = divmod(m, 60)
			await ctx.send(f"**You need to wait {int(h)} hours, {int(m)} minutes, and {int(s)} seconds until you can give reputation again!**")


	@cmds.cooldown(1, 3, cmds.BucketType.user)
	@cmds.command(no_pm=True)
	async def top(self, ctx, *options):
		"""
		Displays leaderboard.
		Add "g" parameter for global or "r" parameter for reputation.

		Examples:
		=========
		[p]top 2
		[p]top g
		[p]top r 3
		[p]top g r 4
		"""
		g = ctx.message.guild
		gid = str(g.id)
		user = ctx.message.author
		if gid in self.settings["disabled_servers"]:
			return await self.bot.say("**Leveler commands for this server are disabled!**")

		users = []
		board_type = ''
		user_stat = None
		all_infos = db.users.find({})
		if "r" in options and "g" in options:
			title = f"Global Rep Leaderboard for {self.bot.user.name}\n"
			for userinfo in all_infos:
				try:
					g_info = userinfo["servers"][gid]
				except:
					continue
				try:
					users.append((g_info["username"], g_info["rep"]))
				except:
					users.append((userinfo["user_id"], g_info["rep"]))

				if user.id == userinfo["user_id"]:
					user_stat = g_info["rep"]
			footer_text = f"Your Rank: {await self._find_global_rep_rank(user)}\nRep Points: {user_stat}"
			icon_url = self.bot.user.avatar_url
		elif "g" in options:
			title = f"Global Exp Leaderboard for {self.bot.user.name}\n"
			for userinfo in all_infos:
				try:
					g_info = userinfo["servers"][gid]
				except:
					continue
				try:
					users.append((g_info["username"], g_info["current_exp"]))
				except:
					users.append((userinfo["user_id"], g_info["current_exp"]))

				if user.id == userinfo["user_id"]:
					user_stat = f"{g_info['current_exp']:,}".replace(',', '\'')

			footer_text = f"Your Rank: {await self._find_global_rank(user)}\nExp Points: {user_stat}"
			icon_url = self.bot.user.avatar_url
		elif "r" in options:
			title = f"Rep Leaderboard for {g.name}\n"
			for userinfo in all_infos:
				try:
					g_info = userinfo["servers"][gid]
				except:
					continue
				if "servers" in userinfo and gid in userinfo["servers"]:
					try:
						users.append((g_info["username"], g_info["rep"]))
					except:
						users.append((userinfo["user_id"], g_info["rep"]))
				if user.id == userinfo["user_id"]:
					user_stat = g_info["rep"]

			footer_text = f"Your Rank: {await self._find_server_rep_rank(user, g)}\nRep Points: {user_stat}"
			icon_url = g.icon_url
		else:
			title = f"Exp Leaderboard for {g.name}\n"
			for userinfo in all_infos:
				try:
					g_info = userinfo["servers"][gid]
				except:
					continue
				if "servers" in userinfo and gid in userinfo["servers"]:
					server_exp = 0
					for i in range(g_info["level"]):
						server_exp += self._required_exp(i)
					server_exp +=  g_info["current_exp"]
					try:
						users.append((g_info["username"], server_exp))
					except:
						users.append((userinfo["user_id"], server_exp))

			serv_exp = f"{await self._find_server_exp(user, g):,}".replace(',', '\'')
			footer_text = f"Your Rank: {await self._find_server_rank(user, g)}\nExp Points: {serv_exp}"
			icon_url = g.icon_url
		#tmp = []
		#[tmp.append((u[0], f"{u[1]:,}".replace(',', '\''))) for u in sorted(users, key=operator.itemgetter(1), reverse=True)]
		sorted_list = sorted(users, key=operator.itemgetter(1), reverse=True) #tmp
		#del(tmp)
		# multiple page support
		page = 1
		per_page = 15
		pages = math.ceil(len(sorted_list) / per_page)
		for option in options:
			if str(option).isdigit():
				if page >= 1 and int(option) <= pages:
					page = int(str(option))
				else:
					return await ctx.send(f"**Please enter a valid page number! (1 - {str(pages)})**")
				break

		msg = f"**Rank              Name (Page {page}/{pages})**\n"
		rank = 1 + per_page * (page - 1)
		start_index = per_page * page - per_page
		end_index = per_page * page
		default_label = "   "
		special_labels = ["♔", "♕", "♖", "♗", "♘", "♙"]

		for single_user in sorted_list[start_index:end_index]:
			if rank - 1 < len(special_labels):
				label = special_labels[rank - 1]
			else:
				label = default_label

			msg += u'`{:<2}{:<2}{:<2}   # {:<22}'.format(rank, label, u"➤", self._truncate_text(single_user[0], 20))
			msg += u'{:>5}{:<2}{:<2}{:<5}`\n'.format(" ", " ", " ", f"Total {board_type}: {str(single_user[1])}")
			rank += 1
		msg += f"----------------------------------------------------\n`{footer_text}`"

		em = dis.Embed(colour=user.colour)
		em.set_author(name=title, icon_url=icon_url)
		em.description = msg
		await ctx.send(content=None, embed=em)


# ===================================================
#	ASYNCHRONOUS FCTS DEFINITION
# ===================================================

	# handle exp credit on msg
	async def _handle_on_message(self, message):
		try:
			g = message.guild
			gid = str(g.id)
			user = message.author
			msg = message.content
		except:		# No DM
			return
		# creates user if doesn't exist, bots are not logged.
		await self._create_user(user, g)
		try:
			userinfo = db.users.find_one({"user_id": user.id})
		except:
			return

		if not g or user.bot or		\
		   gid in self.settings["disabled_servers"]:
			return

		# check if chat_block exists
		if "chat_block" not in userinfo["servers"][gid]:
			userinfo["servers"][gid]["chat_block"] = 0
		#print("chat_block:", float(time.time()) - float(userinfo["chat_block"]))
		if msg[0:4] != "/rep":
			return
		elif msg != "" and msg[0] == '/':
			await self._process_exp(message, userinfo, random.randint(1, 5))
		elif float(time.time()) - float(userinfo["servers"][gid]["chat_block"]) >= self.settings["chat_cooldown"]:			##################################
			await self._process_exp(message, userinfo, random.randint(15, 30))

	async def _process_exp(self, message, userinfo, exp: int):
		g = message.author.guild
		g_info = userinfo["servers"][str(g.id)]
		channel = message.channel
		user = message.author
		#msg = message.content
		new_xp = userinfo["servers"][str(g.id)]["current_exp"] + exp
		# add to total exp
		try:
			exp_frac = g_info["current_exp"]
			curr_lvl = self._find_level(exp_frac)
			required = self._required_exp(curr_lvl)
			total = exp_frac - self._level_exp(curr_lvl) + exp
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"servers.{g.id}.current_exp": new_xp
			}})
		except:
			pass

		prfx = f"servers.{g.id}"
		if total >= required:
			# process if lvl up
			base = 1500
			curr_cred = g_info["credit"]
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"{prfx}.level": self._find_level(new_xp),
				#f"{prfx}.current_exp": new_xp,
				f"{prfx}.credit": curr_cred + base + base * int(self._find_level(exp_frac) // 10),
				f"{prfx}.chat_block": time.time()
			}})
			await self._handle_levelup(user, userinfo, g, channel)
		else:
			# process without lvl up
			db.users.update_one({"user_id": user.id}, {"$set": {
				f"{prfx}.current_exp": new_xp,
				f"{prfx}.chat_block": time.time()
			}})

	async def _handle_levelup(self, user, userinfo, server, channel):
		if not isinstance(self.settings["lvl_msg"], list):
			conf_path = "config/config_leveler.hjson"
			self.settings["lvl_msg"] = []
			dataIO.save_hjson(conf_path, self.settings)

		if "lvl_msg" in self.settings.keys() and server.id not in self.settings["lvl_msg"]:	# if lvl msg is enabled
			# channel lock implementation
			if "lvl_msg_lock" in self.settings.keys() and server.id in self.settings["lvl_msg_lock"].keys():
				channel_id = self.settings["lvl_msg_lock"][str(server.id)]
				channel = find(lambda m: m.id == channel_id, server.channels)

			server_identifier = ""								# super hacky
			name = self._is_mention(user)						# also super hacky
			# private message takes precedent, of course
			if "private_lvl_msg" in self.settings and server.id in self.settings["private_lvl_msg"]:
				server_identifier = f" on {server.name}"
				channel = user
				name = "You"

			new_level = str(userinfo["servers"][str(server.id)]["level"])
			# add to appropriate role if necessary
			try:
				server_roles = db.roles.find_one({"server_id": server.id})
				if server_roles != None:
					for role in server_roles["roles"].keys():
						if int(server_roles["roles"][role]["level"]) == int(new_level):
							role_obj = dis.utils.find(lambda r: r.name == role, server.roles)
							await self.bot.add_roles(user, role_obj)

							if server_roles["roles"][role]["remove_role"] != None:
								remove_role_obj = dis.utils.find(
									lambda r: r.name == server_roles["roles"][role]["remove_role"], server.roles)
								if remove_role_obj != None:
									await self.bot.remove_roles(user, remove_role_obj)
			except:
				await channel.send("Role was not set. Missing Permissions!")
			# add appropriate badge if necessary
			try:
				server_linked_badges = db.badgelinks.find_one({"server_id": server.id})
				if server_linked_badges != None:
					for badge_name in server_linked_badges["badges"]:
						if int(server_linked_badges["badges"][badge_name]) == int(new_level):
							server_badges = db.badges.find_one({"server_id": server.id})
							if server_badges != None and badge_name in server_badges["badges"].keys():
								userinfo = db.users.find_one({"user_id": user.id})
								g_info = userinfo["servers"][str(server.id)]
								new_badge_name = f"{badge_name}_{server.id}"
								g_info["badges"][new_badge_name] = server_badges["badges"][badge_name]
								db.users.update_one({"user_id": user.id}, {"$set": {
									f"servers.{server.id}.badges": g_info["badges"]
								}})
			except:
				await channel.send(content="Error. Badge was not given!")

			if "text_only" in self.settings and server.id in self.settings["text_only"]:
				async with channel.typing():
					em = dis.Embed(description=f"**{name} just gained a level{server_identifier}! (LEVEL {new_level})**",
								   colour=user.colour)
					await channel.send(content=None, embed=em)
			else:
				await self.draw_levelup(user, server)
				async with channel.typing():
					await channel.send(
						content=f"**{name} just gained a level{server_identifier}!**",
						file=dis.File(open(f"data/leveler/tmp/{user.id}_level.png", "rb")))

	async def _process_purchase(self, ctx, img_type, img_name):

		def check(msg):
			return msg.author == author

		author = ctx.message.author
		userinfo = db.users.find_one({"user_id": author.id})
		try:
			price = self.settings["bg_price"][img_type][img_name]
			if price != 0:
				diff = userinfo["servers"][str(author.guild.id)]["credit"] - price
				if diff < 0:
					await ctx.send(f"**Insufficient funds. Backgrounds changes cost: ${price}**")
					return False
				else:
					await ctx.send(f"**{self._is_mention(author)}, you are about to buy `{img_name}` background for `${price}`. Confirm by typing `yes`.**")
					try:
						answer = await self.bot.wait_for("message", check=check, timeout=10)
					except:
						await ctx.send("**Purchase canceled.**")
						return False
					if answer.content.lower() != "yes":
						await ctx.send("**Background not purchased.**")
						return False
					else:
						db.users.update_one({"user_id": author.id}, {"$set": {f"servers.{author.guild.id}.credit": diff}})
						return True
			else:
				if price == 0:
					return True
				else:
					await ctx.send("**Error**")
					return False
		except:
			if price == 0:
				return True
			else:
				raise cmds.CommandError

	async def _give_chat_credit(self, user, server):
		""" try:
			bank = self.bot.get_cog('Economy').bank
			if bank.account_exists(user) and "msg_credits" in self.settings:
				bank.deposit_credits(user, self.settings["msg_credits"][server.id])
		except: """
		pass

	async def draw_levelup(self, user, server):
		# fonts
		font_thin_file = "data/assets/font/Uni_Sans_Thin.ttf"
		level_fnt = ImageFont.truetype(font_thin_file, 23)

		userinfo = db.users.find_one({"user_id": user.id})
		g_info = userinfo["servers"][str(server.id)]

		# get urls
		bg_url = g_info["levelup_background"]
		profile_url = user.avatar_url

		# create image objects
		bg_image = Image
		profile_image = Image

		async with aiohttp.ClientSession() as client:
			async with client.get(bg_url) as r:
				image = await r.content.read()
		with open(f"data/leveler/tmp/{user.id}_tmp_level_bg.png", "wb") as f:
			f.write(image)
		async with aiohttp.ClientSession() as client:
			try:
				async with client.get(str(profile_url)) as r:
					image = await r.content.read()
			except:
				async with client.get(self.default_avatar_url) as r:
					image = await r.content.read()
		with open(f"data/leveler/tmp/{user.id}_tmp_level_profile.png", "wb") as f:
			f.write(image)

		bg_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_level_bg.png").convert("RGBA")
		profile_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_level_profile.png").convert("RGBA")

		# set canvas
		width = 176
		height = 67
		bg_color = (255, 255, 255, 0)
		result = Image.new("RGBA", (width, height), bg_color)
		process = Image.new("RGBA", (width, height), bg_color)
		draw = ImageDraw.Draw(process)

		# puts in background
		bg_image = bg_image.resize((width, height), Image.ANTIALIAS)
		bg_image = bg_image.crop((0, 0, width, height))
		result.paste(bg_image, (0, 0))

		# info section
		lvl_circle_dia = 60
		total_gap = 2
		border = int(total_gap / 2)
		info_section = Image.new("RGBA", (165, 55), (230, 230, 230, 20))
		info_section = self._add_corners(info_section, int(lvl_circle_dia / 2))
		process.paste(info_section, (border, border))

		# draw transparent overlay
		if "levelup_info_color" in g_info.keys():
			info_color = tuple(g_info["levelup_info_color"])
			info_color = (info_color[0], info_color[1], info_color[2], 150) 					# increase transparency
		else:
			info_color = (30, 30, 30, 150)
		for i in range(0, height):
			draw.rectangle([(0, height - i), (width, height - i)],
							fill=(info_color[0], info_color[1], info_color[2], 255 - i * 3))	# title overlay

		# draw circle
		multiplier = 6
		circle_left = 4
		circle_top = int((height - lvl_circle_dia) / 2)
		raw_length = lvl_circle_dia * multiplier
		# create mask
		mask = Image.new('L', (raw_length, raw_length), 0)
		draw_thumb = ImageDraw.Draw(mask)
		draw_thumb.ellipse((0, 0) + (raw_length, raw_length), fill=255, outline=0)

		# border
		lvl_circle = Image.new("RGBA", (raw_length, raw_length))
		draw_lvl_circle = ImageDraw.Draw(lvl_circle)
		draw_lvl_circle.ellipse([0, 0, raw_length, raw_length], fill=(250, 250, 250, 180))
		lvl_circle = lvl_circle.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		lvl_bar_mask = mask.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		process.paste(lvl_circle, (circle_left, circle_top), lvl_bar_mask)

		profile_size = lvl_circle_dia - total_gap
		raw_length = profile_size * multiplier
		# put in profile picture
		output = ImageOps.fit(profile_image, (raw_length, raw_length), centering=(0.5, 0.5))
		output = output.resize((profile_size, profile_size), Image.ANTIALIAS)
		mask = mask.resize((profile_size, profile_size), Image.ANTIALIAS)
		profile_image = profile_image.resize((profile_size, profile_size), Image.ANTIALIAS)
		process.paste(profile_image, (circle_left + border, circle_top + border), mask)

		# write label text
		white_text = (250, 250, 250, 255)
		dark_text = (35, 35, 35, 230)
		level_up_text = self._contrast(info_color, white_text, dark_text)
		lvl_text = f"LEVEL {self._find_level(g_info['current_exp'])}"	 #userinfo["servers"][str(server.id)]["level"])
		draw.text((self._center(60, 170, lvl_text, level_fnt), 23), lvl_text, font=level_fnt, fill=level_up_text) # Level Number

		result = Image.alpha_composite(result, process)
		result = self._add_corners(result, int(height / 2))
		filename = f"data/leveler/tmp/{user.id}_level.png"
		result.save(filename, "PNG", quality=100)


	# handles user creation, adding new server, blocking
	async def _create_user(self, user, server):
		try:
			if user.bot:
				return
			userinfo = db.users.find_one({"user_id": user.id})
			if not userinfo:
				new_account = {
					"user_id" : user.id,
					"servers": {}
				}
				db.users.insert_one(new_account)

			prfx = f"servers.{server.id}"
			userinfo = db.users.find_one({"user_id": user.id})
			if "servers" not in userinfo or str(server.id) not in userinfo["servers"]:
				db.users.update_one({"user_id": user.id}, {"$set": {
					f"{prfx}.active_badges": {},
					f"{prfx}.badges": {},
					f"{prfx}.level": 0,
					f"{prfx}.current_exp": 0,
					f"{prfx}.credit": 0,
					f"{prfx}.rep": 0,
					f"{prfx}.username": "",
					f"{prfx}.title": "",
					f"{prfx}.profile_info": "No description.", #########
					f"{prfx}.profile_background": self.backgrounds["profile"]["default"],
					f"{prfx}.rank_background": self.backgrounds["rank"]["default"],
					f"{prfx}.levelup_background": self.backgrounds["levelup"]["default"],
					f"{prfx}.rep_color": [],
					f"{prfx}.badge_col_color": [],
					f"{prfx}.rep_block": 0,
					f"{prfx}.chat_block": 0
				}}, upsert=True)
			userinfo = db.users.find_one({"user_id": user.id})
			if "username" not in userinfo["servers"][str(server.id)] or userinfo["servers"][str(server.id)]["username"] != user.name:
				db.users.update_one({"user_id": user.id}, {"$set": {
					f"{prfx}.username": user.name,
				}}, upsert=True)
		except AttributeError:
			pass

	# Text-generated user profile
	async def profile_text(self, user, server, userinfo):

		def test_empty(text):
			return (text, None)[text == ""]

		g_info = userinfo["servers"][str(server.id)]
		em = dis.Embed(colour=user.colour)
		em.add_field(name="Title:", value=test_empty(userinfo["title"]))
		em.add_field(name="Reps:", value=userinfo["rep"])
		em.add_field(name="Global Rank:", value=f"#{await self._find_global_rank(user)}")
		em.add_field(name="Server Rank:", value=f"#{await self._find_server_rank(user, server)}")
		em.add_field(name="Server Level:", value=format(g_info["level"]))
		em.add_field(name="Total Exp:", value=g_info["current_exp"])
		em.add_field(name="Server Exp:", value=await self._find_server_exp(user, server))
		em.add_field(name="Credits: ", value=f"{g_info['credit']} $")
		em.add_field(name="Info: ", value=test_empty(g_info["profile_info"]))
		em.add_field(name="Badges: ", value=test_empty(", ".join(g_info["badges"])).replace('_', ' '))
		em.set_author(name=f"Profile for {user.name}", url=user.avatar_url)
		em.set_thumbnail(url=user.avatar_url)
		return em

	async def _find_global_rep_rank(self, user):
		users = []
		for userinfo in db.users.find({}):
			try:
				userid = userinfo["user_id"]
				users.append((userid, userinfo["servers"][str(user.guild.id)]["rep"]))
			except KeyError:
				pass
		sorted_list = sorted(users, key=operator.itemgetter(1), reverse=True)
		rank = 1
		for stats in sorted_list:
			if stats[0] == user.id:
				return rank
			rank+=1

	async def _find_server_rep_rank(self, user, server):
		targetid = user.id
		users = []
		for userinfo in db.users.find({}):
			#userid = userinfo["user_id"]
			if "servers" in userinfo and str(server.id) in userinfo["servers"]:
				users.append((userinfo["user_id"], userinfo["servers"][str(server.id)]["rep"]))
		sorted_list = sorted(users, key=operator.itemgetter(1), reverse=True)
		rank = 1
		for a_user in sorted_list:
			if a_user[0] == targetid:
				return rank
			rank += 1

	async def _find_global_rank(self, user):
		users = []
		for userinfo in db.users.find({}):
			try:
				userid = userinfo["user_id"]
				users.append((userid, userinfo["servers"][str(user.guild.id)]["current_exp"]))
			except KeyError:
				pass
		sorted_list = sorted(users, key=operator.itemgetter(1), reverse=True)
		rank = 1
		for stats in sorted_list:
			if stats[0] == user.id:
				return rank
			rank += 1

	async def _find_server_rank(self, user, server):
		targetid = user.id
		users = []
		for userinfo in db.users.find({}):
			try:
				server_exp = 0
				userid = userinfo["user_id"]
				for i in range(userinfo["servers"][str(server.id)]["level"]):
					server_exp += self._required_exp(i)
				server_exp += userinfo["servers"][str(server.id)]["current_exp"]
				users.append((userid, server_exp))
			except:
				pass
		sorted_list = sorted(users, key=operator.itemgetter(1), reverse=True)
		rank = 1
		for a_user in sorted_list:
			if a_user[0] == targetid:
				return rank
			rank += 1

	async def _find_server_exp(self, user, server):
		server_exp = 0
		userinfo = db.users.find_one({"user_id":user.id})
		try:
			for i in range(userinfo["servers"][str(server.id)]["level"]):
				server_exp += self._required_exp(i)
			server_exp +=  userinfo["servers"][str(server.id)]["current_exp"]
			return server_exp
		except:
			return server_exp

	async def _valid_image_url(self, url):
		#max_byte = 1000
		try:
			async with aiohttp.ClientSession() as client:
				async with client.get(url) as r:
					image = await r.content.read()
					with open("data/leveler/tmp/test.png", "wb") as f:
						f.write(image)
			image = Image.open("data/leveler/tmp/test.png").convert("RGBA")
			os.remove("data/leveler/tmp/test.png")
			return True
		except:
			return False

	async def draw_rank(self, user, server):
		name_fnt = ImageFont.truetype(self.font_heavy_file, 24)
		name_u_fnt = ImageFont.truetype(self.font_unicode_file, 24)
		label_fnt = ImageFont.truetype(self.font_bold_file, 16)
		exp_fnt = ImageFont.truetype(self.font_bold_file, 9)
		large_fnt = ImageFont.truetype(self.font_thin_file, 24)
		#large_cred_fnt = ImageFont.truetype(self.font_bold_file, 20)
		symbol_u_fnt = ImageFont.truetype(self.font_unicode_file, 15)

		# get urls
		userinfo = db.users.find_one({"user_id": user.id})
		g_info = userinfo["servers"][str(server.id)]
		bg_url = g_info["rank_background"]
		profile_url = user.avatar_url
		# create image objects
		bg_image = Image
		profile_image = Image
		async with aiohttp.ClientSession() as session:											# BG
			async with session.get(bg_url) as r:
				image = await r.content.read()
		with open("data/leveler/tmp/tmp_rank_bg.png", "wb") as f:
			f.write(image)
		try:																					# user icon
			async with aiohttp.ClientSession() as session:
				async with session.get(str(profile_url)) as r:
					image = await r.content.read()
		except:
			async with aiohttp.ClientSession() as session:
				async with session.get(self.default_avatar_url) as r:
					image = await r.content.read()
		with open(f"data/leveler/tmp/{user.id}_tmp_rank_profile.png", "wb") as f:
			f.write(image)
		bg_image = Image.open("data/leveler/tmp/tmp_rank_bg.png").convert("RGBA")
		profile_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_rank_profile.png").convert("RGBA")

		# set canvas
		width = 390
		height = 100
		bg_color = (255, 255, 255, 0)
		bg_width = width - 50
		result = Image.new("RGBA", (width, height), bg_color)
		process = Image.new("RGBA", (width, height), bg_color)
		draw = ImageDraw.Draw(process)
		# info section
		info_section = Image.new("RGBA", (bg_width, height), bg_color)
		info_section_process = Image.new("RGBA", (bg_width, height), bg_color)
		draw_info = ImageDraw.Draw(info_section)
		# puts in background
		bg_image = bg_image.resize((width, height), Image.ANTIALIAS)
		bg_image = bg_image.crop((0, 0, width, height))
		info_section.paste(bg_image, (0, 0))
		# draw transparent overlays
		draw_overlay = ImageDraw.Draw(info_section_process)
		draw_overlay.rectangle([(0, 0), (bg_width, 20)], fill=(230, 230, 230, 200))
		draw_overlay.rectangle([(0, 20), (bg_width, 30)], fill=(120, 120, 120, 180))							# Level bar
		lvl = int(g_info["level"])
		exp_frac = int(g_info["current_exp"]) - self._level_exp(lvl)
		exp_total = self._required_exp(lvl)
		exp_width = int(bg_width * (exp_frac / exp_total))
		if "rank_info_color" in g_info.keys():
			exp_color = tuple(g_info["rank_info_color"])
			exp_color = (exp_color[0], exp_color[1], exp_color[2], 180)											# increase transparency
		else:
			exp_color = (47, 47, 47, 255)
		#print("\n\nexp_width: {}\nexp_color: {}\n".format(
		#	exp_width, exp_color))
		draw_overlay.rectangle([(0, 20), (exp_width + 30, 30)], fill=exp_color)									# Exp bar
		draw_overlay.rectangle([(0, 30), (bg_width, 31)], fill=(0, 0, 0, 255))									# Divider
		draw_overlay.rectangle([(0, 35), (bg_width, 100)], fill=(230, 230, 230, 0))								# title overlay
		for i in range(0, 70):
			draw_overlay.rectangle([(0, height - i), (bg_width, height - i)], fill=(20, 20, 20, 255 - i * 3))	# title overlay

		# draw corners and finalize
		info_section = Image.alpha_composite(info_section, info_section_process)
		info_section = self._add_corners(info_section, 25)
		process.paste(info_section, (35, 0))
		# draw level circle
		multiplier = 6
		lvl_circle_dia = 100
		circle_left = 0
		circle_top = int((height - lvl_circle_dia) / 2)
		raw_length = lvl_circle_dia * multiplier
		# create mask
		mask = Image.new('L', (raw_length, raw_length), 0)
		draw_thumb = ImageDraw.Draw(mask)
		draw_thumb.ellipse((0, 0) + (raw_length, raw_length), fill=255, outline=0)
		# drawing level border
		lvl_circle = Image.new("RGBA", (raw_length, raw_length))
		draw_lvl_circle = ImageDraw.Draw(lvl_circle)
		draw_lvl_circle.ellipse([0, 0, raw_length, raw_length], fill=(250, 250, 250, 250))
		# determines exp bar color
		"""
		if "rank_exp_color" not in g_info.keys() or not g_info["rank_exp_color"]:
			exp_fill = (255, 255, 255, 230)
		else:
			exp_fill = tuple(g_info["rank_exp_color"])"""
		#exp_fill = (255, 255, 255, 230)

		# put on profile circle background
		lvl_circle = lvl_circle.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		lvl_bar_mask = mask.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		process.paste(lvl_circle, (circle_left, circle_top), lvl_bar_mask)
		# draws mask
		total_gap = 6
		border = int(total_gap / 2)
		profile_size = lvl_circle_dia - total_gap
		raw_length = profile_size * multiplier
		# put in profile picture
		output = ImageOps.fit(profile_image, (raw_length, raw_length), centering=(0.5, 0.5))
		output = output.resize((profile_size, profile_size), Image.ANTIALIAS)
		mask = mask.resize((profile_size, profile_size), Image.ANTIALIAS)
		profile_image = profile_image.resize((profile_size, profile_size), Image.ANTIALIAS)
		process.paste(profile_image, (circle_left + border, circle_top + border), mask)

		# draw text
		grey_color = (100, 100, 100, 255)
		white_color = (220, 220, 220, 255)
		# name
		left_text_align = 130
		name_color = 0
		self._write_unicode(draw, self._truncate_text(self._name(user, 20), 20), 100, 0, name_fnt, name_u_fnt, grey_color)						# Name
		# labels
		v_label_align = 75
		info_text_color = white_color
		draw.text((self._center(100, 200, "  RANK", label_fnt), v_label_align), "  RANK", font=label_fnt, fill=info_text_color)					# Rank
		draw.text((self._center(100, 360, "  LEVEL", label_fnt), v_label_align), "  LEVEL", font=label_fnt, fill=info_text_color)				# Rank
		draw.text((self._center(260, 360, "$ BALANCE", label_fnt), v_label_align), "$ BALANCE", font=label_fnt, fill=info_text_color)			# Rank
		self._write_unicode(draw, u"\U0001F3E0", 117, v_label_align + 4, label_fnt, symbol_u_fnt, info_text_color)								# Symbol
		self._write_unicode(draw, u"\U0001F3E0", 195, v_label_align + 4, label_fnt, symbol_u_fnt, info_text_color)								# Symbol
		# userinfo
		server_rank = "#{}".format(await self._find_server_rank(user, server))
		level_text = f"{g_info['level']}"
		global_credit = self._get_credits_str(int(g_info["credit"]))
		exp_text = f"{exp_frac}/{exp_total}"
		draw.text((self._center(100, 200, server_rank, large_fnt), v_label_align - 30), server_rank, font=large_fnt, fill=info_text_color)		# Rank
		draw.text((self._center(95, 360, level_text, large_fnt), v_label_align - 30), level_text, font=large_fnt, fill=info_text_color)			# Level
		draw.text((self._center(260, 360, global_credit, large_fnt), v_label_align - 30), global_credit, font=large_fnt, fill=info_text_color)	# Balance
		draw.text((self._center(80, 360, exp_text, exp_fnt), 19), exp_text, font=exp_fnt, fill=info_text_color)									# Rank

		# process img
		result = Image.alpha_composite(result, process)
		result.save(f"data/leveler/tmp/{user.id}_rank.png", "PNG", quality=100)
		# try remove imgs
		try:
			os.remove("data/leveler/tmp/tmp_rank_bg.png")
		except:
			pass
		try:
			os.remove(f"data/leveler/tmp/{user.id}_tmp_rank_profile.png")
		except:
			pass

	async def _draw_badge(self, process, guild, user, userinfo, badges, exp_fill, info_fill, counter):
		vert_pos = 172
		right_shift = 0
		left = 9 + right_shift
		right = 52 + right_shift
		size = 38
		total_gap = 4 # /2
		hor_gap = 3
		vert_gap = 3
		border_width = int(total_gap / 2)
		multiplier = 6					# for antialiasing
		raw_length = size * multiplier
		mult = [
			(0, 1), (1, 1), (2, 1),
			(0, 2), (1, 2), (2, 2)
		]
		#for num in range(6):																		####################################
		if counter == 0:
			coord = (left + int(hor_gap + size), vert_pos)	# Main Badge here
		else:												# Pins here
			counter -= 1
			coord = (
				left + int(mult[counter][0]) * int(hor_gap + size),
				vert_pos + int(mult[counter][1]) * int(vert_gap + size))
		if counter == 0:
			try:
				level = userinfo["servers"][str(guild.id)]["level"]
				# draw mask circle
				mask = Image.new('L', (raw_length, raw_length), 0)
				draw_thumb = ImageDraw.Draw(mask)
				draw_thumb.ellipse((0, 0) + (raw_length, raw_length), fill=255, outline=0)
				badge_image = Image.open(f"data/assets/badges/lvl_{level}.png").convert("RGBA")
				badge_image = badge_image.resize((raw_length, raw_length), Image.ANTIALIAS)
				# put on ellipse/circle
				output = ImageOps.fit(badge_image, (raw_length, raw_length), centering=(0.5, 0.5))
				output = output.resize((size, size), Image.ANTIALIAS)
				outer_mask = mask.resize((size, size), Image.ANTIALIAS)
				process.paste(output, coord, outer_mask)
			except:
				pass

		"""
		if counter < len(badges[:6]):
			pair = badges[counter]
			badge = pair[0]
			bg_color = badge["bg_img"]
			border_color = badge["border_color"]
			# draw mask circle
			mask = Image.new('L', (raw_length, raw_length), 0)
			draw_thumb = ImageDraw.Draw(mask)
			draw_thumb.ellipse((0, 0) + (raw_length, raw_length), fill=255, outline=0)

			# determine image or color for badge bg
			#print(bg_color)
			if await self._valid_image_url(bg_color):
				# get image
				async with aiohttp.ClientSession() as session:
					async with session.get(bg_color) as r:
						image = await r.content.read()
					with open(f"data/leveler/tmp/{user.id}_tmp_badge.png", "wb") as f:
						f.write(image)
				badge_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_badge.png").convert("RGBA")
				badge_image = badge_image.resize((raw_length, raw_length), Image.ANTIALIAS)

				# structured like this because if border = 0, still leaves outline.
				if border_color:
					square = Image.new("RGBA", (raw_length, raw_length), border_color)
					# put border on ellipse/circle
					output = ImageOps.fit(square, (raw_length, raw_length), centering=(0.5, 0.5))
					output = output.resize((size, size), Image.ANTIALIAS)
					outer_mask = mask.resize((size, size), Image.ANTIALIAS)
					process.paste(output, coord, outer_mask)
					# put on ellipse/circle
					output = ImageOps.fit(badge_image, (raw_length, raw_length), centering=(0.5, 0.5))
					output = output.resize((size - total_gap, size - total_gap), Image.ANTIALIAS)
					inner_mask = mask.resize((size - total_gap, size - total_gap), Image.ANTIALIAS)
					process.paste(output, (coord[0] + border_width, coord[1] + border_width), inner_mask)
				else:
					# put on ellipse/circle
					output = ImageOps.fit(badge_image, (raw_length, raw_length), centering=(0.5, 0.5))
					output = output.resize((size, size), Image.ANTIALIAS)
					outer_mask = mask.resize((size, size), Image.ANTIALIAS)
					process.paste(output, coord, outer_mask)
			else:
				plus_fill = exp_fill
				# put on ellipse/circle
				plus_square = Image.new("RGBA", (raw_length, raw_length))
				plus_draw = ImageDraw.Draw(plus_square)
				plus_draw.rectangle([(0, 0), (raw_length, raw_length)], fill=(info_fill[0], info_fill[1], info_fill[2], 245))
				# draw plus signs
				margin = 60
				thickness = 40
				v_left = int(raw_length / 2 - thickness / 2)
				v_right = v_left + thickness
				v_top = margin
				v_bottom = raw_length - margin
				plus_draw.rectangle([(v_left, v_top), (v_right, v_bottom)], fill=(plus_fill[0], plus_fill[1], plus_fill[2], 245))
				h_left = margin
				h_right = raw_length - margin
				h_top = int(raw_length / 2 - thickness / 2)
				h_bottom = h_top + thickness
				plus_draw.rectangle([(h_left, h_top), (h_right, h_bottom)], fill=(plus_fill[0], plus_fill[1], plus_fill[2], 245))
				# put border on ellipse/circle
				output = ImageOps.fit(plus_square, (raw_length, raw_length), centering=(0.5, 0.5))
				output = output.resize((size, size), Image.ANTIALIAS)
				outer_mask = mask.resize((size, size), Image.ANTIALIAS)
				process.paste(output, coord, outer_mask) """

		# attempt to remove badge image
		try:
			#pass
			os.remove(f"data/leveler/tmp/{user.id}_tmp_badge.png")
		except:
			pass

	async def draw_profile(self, user, server):
		name_fnt = ImageFont.truetype(self.font_heavy_file, 26)
		name_u_fnt = ImageFont.truetype(self.font_unicode_file, 26)
		title_fnt = ImageFont.truetype(self.font_heavy_file, 20)
		title_u_fnt = ImageFont.truetype(self.font_unicode_file, 20)
		label_fnt = ImageFont.truetype(self.font_bold_file, 18)
		exp_fnt = ImageFont.truetype(self.font_bold_file, 13)
		large_fnt = ImageFont.truetype(self.font_thin_file, 33)
		cred_fnt = ImageFont.truetype(self.font_thin_file, 29)
		rep_fnt = ImageFont.truetype(self.font_heavy_file, 26)
		rep_u_fnt = ImageFont.truetype(self.font_unicode_file, 30)
		text_fnt = ImageFont.truetype(self.font_reg_file, 14)
		text_u_fnt = ImageFont.truetype(self.font_unicode_file, 14)
		symbol_u_fnt = ImageFont.truetype(self.font_unicode_file, 15)
		symbol_u_big_fnt = ImageFont.truetype(self.font_unicode_file, 20)

		# get urls
		userinfo = db.users.find_one({"user_id": user.id})
		self._badge_convert_dict(userinfo, server)
		g_info = userinfo["servers"][str(server.id)]
		bg_url = g_info["profile_background"]
		profile_url = user.avatar_url
		# COLORS
		white_color = (240, 240, 240, 255)
		light_color = (160, 160, 160, 255)
		rep_fill = (tuple(g_info["rep_color"]), (92, 130, 203, 230))						\
					["rep_color" not in g_info.keys() or not g_info["rep_color"]]
		badge_fill = (tuple(g_info["badge_col_color"]), (128, 151, 165, 230))				\
					["badge_col_color" not in g_info.keys() or not g_info["badge_col_color"]]
		if "profile_info_color" in g_info.keys():
			info_fill = tuple(g_info["profile_info_color"])
		else:
			info_fill = (30, 30, 30, 220)
		info_fill_tx = (info_fill[0], info_fill[1], info_fill[2], 150)
		if "profile_exp_color" not in g_info.keys() or not g_info["profile_exp_color"]:
			exp_fill = (255, 255, 255, 230)
		else:
			exp_fill = tuple(g_info["profile_exp_color"])
		level_fill = (self._contrast(exp_fill, rep_fill, badge_fill), (128, 151, 165, 230))	\
			[badge_fill == (128, 151, 165, 230)]

		# create image objects
		bg_image = Image
		profile_image = Image
		async with aiohttp.ClientSession() as session:
			async with session.get(bg_url) as r:
				image = await r.content.read()
		with open(f"data/leveler/tmp/{user.id}_tmp_profile_bg.png", "wb") as f:
			f.write(image)
		async with aiohttp.ClientSession() as session:
			try:
				async with session.get(str(profile_url)) as r:
					image = await r.content.read()
			except:
				async with session.get(self.default_avatar_url) as r:
					image = await r.content.read()
		with open(f"data/leveler/tmp/{user.id}_tmp_profile_profile.png", "wb") as f:
			f.write(image)
		bg_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_profile_bg.png").convert("RGBA")
		profile_image = Image.open(f"data/leveler/tmp/{user.id}_tmp_profile_profile.png").convert("RGBA")
		# set canvas
		bg_color = (255, 255, 255, 0)
		result = Image.new("RGBA", (340, 390), bg_color)
		process = Image.new("RGBA", (340, 390), bg_color)
		# draw
		draw = ImageDraw.Draw(process)
		# puts in background
		bg_image = bg_image.resize((340, 340), Image.ANTIALIAS)
		bg_image = bg_image.crop((0, 0, 340, 305))
		result.paste(bg_image, (0, 0))
		# draw filter
		draw.rectangle([(0, 0), (340, 340)], fill=(0, 0, 0, 10))
		# draw transparent overlay
		vert_pos = 305
		left_pos = 0
		right_pos = 340
		title_height = 30
		gap = 3
		draw.rectangle([(0,134), (340, 325)], fill=info_fill_tx)	#general content
		# draw profile circle
		multiplier = 8
		lvl_circle_dia = 116
		circle_left = 14
		circle_top = 48
		raw_length = lvl_circle_dia * multiplier
		# create mask
		mask = Image.new('L', (raw_length, raw_length), 0)
		draw_thumb = ImageDraw.Draw(mask)
		draw_thumb.ellipse((0, 0) + (raw_length, raw_length), fill=255, outline=0)
		# border
		lvl_circle = Image.new("RGBA", (raw_length, raw_length))
		draw_lvl_circle = ImageDraw.Draw(lvl_circle)
		draw_lvl_circle.ellipse([0, 0, raw_length, raw_length], fill=(255, 255, 255, 255), outline=(255, 255, 255, 250))
		# put border
		lvl_circle = lvl_circle.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		lvl_bar_mask = mask.resize((lvl_circle_dia, lvl_circle_dia), Image.ANTIALIAS)
		process.paste(lvl_circle, (circle_left, circle_top), lvl_bar_mask)
		# put in profile picture
		total_gap = 6
		border = int(total_gap / 2)
		profile_size = lvl_circle_dia - total_gap
		raw_length = profile_size * multiplier
		output = ImageOps.fit(profile_image, (raw_length, raw_length), centering=(0.5, 0.5))
		output = output.resize((profile_size, profile_size), Image.ANTIALIAS)
		mask = mask.resize((profile_size, profile_size), Image.ANTIALIAS)
		profile_image = profile_image.resize((profile_size, profile_size), Image.ANTIALIAS)
		process.paste(profile_image, (circle_left + border, circle_top + border), mask)
		# write label text
		white_color = (240, 240, 240, 255)
		light_color = (160, 160, 160, 255)
		dark_color = (35, 35, 35, 255)
		head_align = 140
		 # determine info text color
		info_text_color = self._contrast(info_fill, white_color, dark_color)
		self._write_unicode(draw, self._truncate_text(user.name, 12).upper(), head_align, 142, name_fnt, name_u_fnt, info_text_color)		# NAME
		self._write_unicode(draw, g_info["title"].upper(), head_align, 170, title_fnt, title_u_fnt, info_text_color)						# TITLE
		# draw divider
		draw.rectangle([(0, 323), (340, 324)], fill=(0, 0, 0, 255))																			# box
		# draw text box
		draw.rectangle([(0, 324), (340, 390)], fill=(info_fill[0], info_fill[1], info_fill[2], 255))										# box
		# REP
		rep_text = f"{g_info['rep']}"
		self._write_unicode(draw, "❤", 257, 9, rep_fnt, rep_u_fnt, info_text_color)
		draw.text((self._center(278, 340, rep_text, rep_fnt), 10), rep_text, font=rep_fnt, fill=info_text_color)							# Exp Text
		lvl_left = 100
		label_align = 362	# vertical
		draw.text((self._center(0, 140, "    RANK", label_fnt), label_align), "    RANK", font=label_fnt, fill=info_text_color)				# Rank
		draw.text((self._center(0, 340, "    LEVEL", label_fnt), label_align), "   LEVEL", font=label_fnt, fill=info_text_color)			# Exp
		draw.text((self._center(200, 340, "$ BALANCE", label_fnt), label_align), "$ BALANCE", font=label_fnt, fill=info_text_color)			# Credits
		self._write_unicode(draw, u"\U0001F30E", 36, label_align + 5, label_fnt, symbol_u_fnt, info_text_color)								# Symbol 🌎
		self._write_unicode(draw, u"\U00002BB8", 134, label_align + 5, label_fnt, symbol_u_big_fnt, info_text_color)						# Symbol ⮸
		# userinfo
		global_rank = f"#{await self._find_global_rank(user)}"
		global_level = f"{self._find_level(g_info['current_exp'])}"
		global_credit = self._get_credits_str(int(g_info["credit"]))
		draw.text((self._center(0, 140, global_rank, large_fnt), label_align - 27), global_rank, font=large_fnt, fill=info_text_color)		# Rank
		draw.text((self._center(0, 340, global_level, large_fnt), label_align - 27), global_level, font=large_fnt, fill=info_text_color)	# Exp
		draw.text((self._center(0, 540, global_credit, cred_fnt), label_align - 27), global_credit, font=cred_fnt, fill=info_text_color)	# Credit
		# draw level bar
		exp_font_color = self._contrast(exp_fill, light_color, dark_color)
		exp_frac = int(g_info["current_exp"] - self._level_exp(int(global_level)))
		exp_total = self._required_exp(int(global_level))
		bar_length = int(exp_frac / exp_total * 340)
		draw.rectangle([(0, 305), (340, 323)], fill=(level_fill[0], level_fill[1], level_fill[2], 245))										# level box
		draw.rectangle([(0, 305), (bar_length, 323)], fill=(exp_fill[0], exp_fill[1], exp_fill[2], 255))									# box
		exp_text = f"{exp_frac}/{exp_total}"																								# Exp
		draw.text((self._center(0, 340, exp_text, exp_fnt), 305), exp_text, font=exp_fnt, fill=exp_font_color)								# Exp Text
		offset = (195, 170)[g_info["title"] == ""]
		margin = 140
		txt_color = self._contrast(info_fill, white_color, dark_color)
		for line in textwrap.wrap(g_info["profile_info"], width=31):
			self._write_unicode(draw, line, margin, offset, text_fnt, text_u_fnt, txt_color)												# Info
			offset += text_fnt.getsize(line)[1] + 2
		# sort badges
		priority_badges = []
		for badgename in g_info["badges"].keys():
			badge = g_info["badges"][badgename]
			priority_num = badge["priority_num"]
			#if priority_num != 0 and priority_num != -1:
			priority_badges.append((badge, priority_num))
		sorted_badges = sorted(priority_badges, key=operator.itemgetter(1), reverse=True)


		# TODO: simplify this. it shouldn't be this complicated... sacrifices conciseness for customizability
		if "badge_type" not in self.settings.keys() or self.settings["badge_type"] == "circles":
			# circles require antialiasing
			#for c in range(7):
			await self._draw_badge(process, server, user, userinfo, sorted_badges, exp_fill, info_fill, 0) #c)


		# process img
		result = Image.alpha_composite(result, process)
		result = self._add_corners(result, 25)
		result.save(f"data/leveler/tmp/{user.id}_profile.png", "PNG", quality=100)
		# remove imgs
		try:
			os.remove(f"data/leveler/tmp/{user.id}_tmp_profile_bg.png")
		except:
			pass
		try:
			os.remove(f"data/leveler/tmp/{user.id}_tmp_profile_profile.png")
		except:
			pass


# ===================================================
#	SYNCHRONOUS FCTS DEFINITION
# ===================================================

	def _disabled(self, guild: dis.Guild):
		return guild.id in self.settings["disabled_servers"]

	def _is_hex(self, color: str):
		if color != None and len(color) != 4 and len(color) != 7:
			return False
		regex = r"^#(?:[0-9a-fA-F]{3}){1,2}$"
		return re.search(regex, str(color))

	def _write_unicode(self, draw: ImageDraw.Draw, text, init_x, y, font, unicode_font, fill):
		write_pos = init_x
		for char in text:
			if char.isalnum() or char in string.punctuation or char in string.whitespace:
				draw.text((write_pos, y), char, font=font, fill=fill)
				write_pos += font.getsize(char)[0]
			else:
				draw.text((write_pos, y), u"{}".format(char), font=unicode_font, fill=fill)
				write_pos += unicode_font.getsize(char)[0]

	def _add_corners(self, im, rad, multiplier = 6):
		raw_length = rad * 2 * multiplier
		circle = Image.new('L', (raw_length, raw_length), 0)
		draw = ImageDraw.Draw(circle)
		draw.ellipse((0, 0, raw_length, raw_length), fill=255)
		circle = circle.resize((rad * 2, rad * 2), Image.ANTIALIAS)
		alpha = Image.new('L', im.size, 255)
		w, h = im.size
		alpha.paste(circle.crop((0, 0, rad, rad)), (0, 0))
		alpha.paste(circle.crop((0, rad, rad, rad * 2)), (0, h - rad))
		alpha.paste(circle.crop((rad, 0, rad * 2, rad)), (w - rad, 0))
		alpha.paste(circle.crop((rad, rad, rad * 2, rad * 2)), (w - rad, h - rad))
		im.putalpha(alpha)
		return im

	def _get_credits_str(self, credits: int):
		if credits > 999:
			credits /= 1000
		else:
			return f"{credits}"
		#
		return (f"{math.floor(credits * 10) / 10}k", f"{int(credits / 1000)}M")[credits >= 1000]

	# returns color that contrasts better in background
	def _contrast(self, bg_color, color1, color2):

		def luminance(color):
			# convert to greyscale
			luminance = float((0.2126 * color[0]) + (0.7152 * color[1]) + (0.0722 * color[2]))
			return luminance

		def contrast_ratio(bgcolor, foreground):
			# return contrast ration for best contrast
			f_lum = float(luminance(foreground) + 0.05)
			bg_lum = float(luminance(bgcolor) + 0.05)
			ratio = (f_lum / bg_lum, bg_lum / f_lum)[bg_lum > f_lum]
			return ratio

		color1_ratio = contrast_ratio(bg_color, color1)
		color2_ratio = contrast_ratio(bg_color, color2)
		color = (color2, color1)[color1_ratio >= color2_ratio]
		return color

	def _badge_convert_dict(self, userinfo, server):
		if "badges" not in userinfo["servers"][str(server.id)] or 					\
			not isinstance(userinfo["servers"][str(server.id)]["badges"], dict):
			db.users.update_one({"user_id": userinfo["user_id"]}, {"$set": {
				f"server.{server.id}.badges": {},
			}})
		return db.users.find_one({"user_id": userinfo["user_id"]})

	def _name(self, user, max_length):
		truncated = self._truncate_text(user.display_name, max_length - len(user.name) - 3)
		return (f"{user.name} ({truncated})", user.name)[user.name == user.display_name]
		#if user.name == user.display_name:
		#	return user.name
		#else:
		#	return "{} ({})".format(user.name, self._truncate_text(user.display_name, max_length - len(user.name) - 3), max_length)

	def _is_mention(self,user):
		return (user.name + '#' + user.discriminator, user.mention)		\
				["mention" not in self.settings.keys() or self.settings["mention"]]

	# truncate text based on max_length
	def _truncate_text(self, text, max_length):
		text = str(text)
		if len(text) > max_length:
			if text.strip('$').isdigit():
				text = int(text.strip('$'))
				return "${:.2E}".format(text)
			return text[:max_length-3] + "..."
		return text

	# finds the the pixel to center the text
	def _center(self, start, end, text, font):
		dist = end - start
		width = font.getsize(text)[0]
		start_pos = start + ((dist - width) / 2)
		return int(start_pos)

	def _find_level(self, total_exp):
		return int((1 / 278) * (9 + math.sqrt(81 + 1112 * total_exp)))

	def _level_exp(self, level: int):
		return level * 65 + 139 * level * (level - 1) // 2

	# calculates required exp for next level
	def _required_exp(self, level: int):
		return ((139 * level + 65), 0)[level < 0]



# ===================================================
#	SETUP
# ===================================================

def check_files():
	if not os.path.exists("data/leveler"):
		print("Creating data/leveler folder...")
		os.makedirs("data/leveler")

	if not os.path.exists("data/leveler/tmp"):
		print("Creating data/leveler/tmp folder...")
		os.makedirs("data/leveler/tmp")

	default = {
		"bg_price": {
			"profile": {
				"default": 0,
				"alice": 3000,
				"bluestairs": 2000,
				"coastline": 5000,
				"greenery": 2000,
				"iceberg": 5000,
				"lamp": 5000,
				"miraiglasses": 7000,
				"miraikuriyama": 8000,
				"mountaindawn": 5500,
				"redblack": 10000,
				"waterlilies": 3000
			},
			"rank": {
				"default": 0,
				"abstract": 8000,
				"aurora": 35000,
				"city": 28000,
				"mountain": 35000,
				"nebula": 48000
			},
			"levelup": {
				"default": 0
			}
		},
		"lvl_msg": [], # enabled lvl msg servers
		"disabled_servers": [],
		"badge_type": "circles",
		"mention" : True,
		"text_only": [],
		"server_roles": {},
		"rep_cooldown": 43200,
		"chat_cooldown": 120
		}
	conf_path = "config/config_leveler.hjson"
	if not os.path.isfile(conf_path):
		print("Creating default leveler config_leveler.hjson...")
		dataIO.save_hjson(conf_path, default)

	bgs = {
			"profile": {
				"default": "http://i.imgur.com/8T1FUP5.jpg",
				"alice": "http://i.imgur.com/MUSuMao.png",
				"bluestairs": "http://i.imgur.com/EjuvxjT.png",
				"coastline": "http://i.imgur.com/XzUtY47.jpg",
				"greenery": "http://i.imgur.com/70ZH6LX.png",
				"iceberg": "http://i.imgur.com/8KowiMh.png",
				"lamp": "http://i.imgur.com/0nQSmKX.jpg",
				"miraiglasses": "http://i.imgur.com/2Ak5VG3.png",
				"miraikuriyama": "http://i.imgur.com/jQ4s4jj.png",
				"mountaindawn": "http://i.imgur.com/kJ1yYY6.jpg",
				"redblack": "http://i.imgur.com/74J2zZn.jpg",
				"waterlilies": "http://i.imgur.com/qwdcJjI.jpg",
				"weed": "https://i.imgur.com/gCBuyVE.png"
			},
			"rank": {
				"default" : "http://i.imgur.com/SorwIrc.jpg",
				"abstract" : "http://i.imgur.com/70ZH6LX.png",
				"aurora" : "http://i.imgur.com/gVSbmYj.jpg",
				"city": "http://i.imgur.com/yr2cUM9.jpg",
				"mountain" : "http://i.imgur.com/qYqEUYp.jpg",
				"nebula": "http://i.imgur.com/V5zSCmO.jpg"
			},
			"levelup": {
				"default" : "http://i.imgur.com/eEFfKqa.jpg",
			},
		}
	bgs_path = "data/leveler/backgrounds.json"
	if not os.path.isfile(bgs_path):
		print("Creating default leveler backgrounds.json...")
		dataIO.save_hjson(bgs_path, bgs)

	f = "data/leveler/badges.json"
	if not dataIO.is_valid_hjson(f):
		print("Creating badges.json...")
		dataIO.save_hjson(f, {})


def setup(bot):
	check_files()
	bot.add_listener(Leveler(bot)._handle_on_message, "on_message")
	bot.add_cog(Leveler(bot))
