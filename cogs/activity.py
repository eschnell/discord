import discord as dis

from discord.ext			import commands as cmds
from cogs.utils.perms		import *


class Status(int):

	def __init__(self, int_type: int):
		self.object = [k for j, k in									\
				enumerate((												\
					(":black_large_square:", dis.Status.invisible),		\
					(":green_square:", dis.Status.online),				\
					(":orange_square:", dis.Status.idle),				\
					(":red_square:", dis.Status.dnd))) 					\
						if j == int_type][0]

class   Activity(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, ctx):
		self.bot = ctx

	@cmds.check(is_mod)
	@cmds.command(aliases=["act", "activity", "prs", "presence"])
	async def change_activity(self, ctx, status: int, *activity: str):
		"""
		0: invisible
		1: online
		2: idle
		3: dnd (or do not disturb)
		"""
		if status < 0 or status > 3:
			return await ctx.send("`{}` is not a valid status. Check help with `{}help {}`".format(
				status, self.bot.prefixes[0], ctx.command.name))
		status_obj = Status(status).object

		if not len(activity):
			activity = ""
		else:
			activity = ' '.join(activity)

		await ctx.bot.change_presence(status=status_obj[1], activity=dis.Game(activity))
		await ctx.send(("Bot __status__ and __custom message__, "
						"both respectively set to:\n"
						"|{}| `{}` and `{}`.").format(
							status_obj[0], status_obj[1], activity))



def setup(bot):
	bot.add_cog(Activity(bot))
