from discord.ext		import commands as cmds
#from pymongo			import MongoClient			# DB

from cogs.utils.perms	import *
from cogs.utils.logger	import Logger
from cogs.utils.checks	import embed_perms, update_bot
#from cogs.utils.dataIO	import dataIO				# JSON & HJSON IO
#from cogs.utils.locale	import get_user_locale		# Locales

import discord as dis
import cprint
import datetime
import os
import sys
import time

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

class Bot_Core_Commands(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot

		## For Logger module
		self.logname = __name__								# Needed by logger class
		self.log = Logger(self)								# Logger obj

		## For DB socket
		#self.dbclient = MongoClient("25.105.87.145", 27717)	# IP and port
		#self.db = self.dbclient.___							# DB Table name


#############
#	EVENTS
################################################################

	# Chainload Cogs
	def _load_extensions(self):
		for extension in self.bot.startup_cogs:
			try:
				if str(extension) == "@bot_core_commands":
					continue
				self.bot.load_extension(f"cogs.{extension}")
				print(f"loaded [{extension}]")
			except (AttributeError, ImportError) as e:
				exc = "{}: {}".format(type(e).__name__, e)
				cprint.cprint.fatal(f"extension error {extension}\n{exc}", interrupt=False)
				continue

	## Load variables and modules
	@cmds.Cog.listener()
	async def on_ready(self):
		self._load_extensions()

		# Print bot id and python version to console
		name = f"Logged in as {self.bot.user}"
		uid = f"user id {self.bot.user.id}"
		tmp = sys.version.splitlines()
		separator = '-' * max(len(name), len(uid), len(tmp[0]), len(tmp[1]))
		print(f"{separator}\n{name}\n{uid}\n{tmp[0]}\n{tmp[1]}\n{separator}\n")

		if os.path.isfile("restart.txt"):
			channel = self.bot.get_channel(int(open("restart.txt").read()))
			os.remove("restart.txt")
			await channel.send("Bot has restarted.")

	## Prompt disconnects
	@cmds.Cog.listener()
	async def on_disconnect(self):
		d = datetime.datetime.now()
		t = time.strftime("%A %d/%m/%Y %I:%M:%S %p", d.timetuple())
		print(f"\n[warning]: unexpected disconnect\t\t@ {t}\n")

###############
#	COMMANDS
################################################################

	## Update bot
	@cmds.command(aliases=["upd", "upg"])
	@cmds.check(is_admin)
	async def update(self, ctx, msg = None):
		if msg is None: msg = False
		latest = update_bot((True, False)[msg])

		if latest:
			if not msg == "show":
				if embed_perms(ctx.message):
					try:
						await ctx.send(content=None, embed=latest)
					except:
						pass
				await ctx.send("There is an update available. Downloading update and restarting (check your console to see the progress)...")
			else:
				try:
					await ctx.send(content=None, embed=latest)
				except Exception as e:
					print(e)
				return
			with open("quit.txt", "w", encoding="utf8") as fp:
				fp.write("update")
			with open("restart.txt", "w", encoding="utf8") as fp:
				fp.write(str(ctx.message.channel.id))
			os._exit(15)
		else:
			await ctx.send("The bot is up to date.")

	## Restart command
	@cmds.command(aliases=["rst", "reboot"])
	@cmds.check(is_admin)
	async def restart(self, ctx):

		def check(m):
			return (False, True)[ctx.author == m.author]

		user = ctx.author
		msg_restart = f"User {user} requested a restart."
		if not self.bot.debug: self.log.info(msg_restart)
		self.log.warning(msg_restart)
		await ctx.channel.send("Restart {} Bot? (**y**/**n**)".format(self.bot.name))
		reply = None
		try:
			reply = await self.bot.wait_for("message", check=check, timeout=15) #lambda ctx, message: message.author == ctx.author)
		except:
			await ctx.send("Timed out, please retype command if you wish to restart the Bot.")

		if not reply or reply.content.lower().strip() == 'n':
			self.log.info(f"{user} Aborted restart.\n")
			await ctx.channel.send("Cancelled.")
		elif reply.content.lower().strip() == 'y':
			self.log.warning("{} confirmed the restart. Bot program now rebooting.\n".format(user))
			self.log.info("Restarting...\n")
			with open("restart.txt", "w", encoding="utf8") as fp:
				fp.write(str(ctx.message.channel.id))
			await ctx.channel.send("Restarting...")
			#if bot.subpro:
			#	bot.subpro.kill()
			os._exit(0)
		else:
			await ctx.channel.send("Please select `(y/n)` as answer.")

	""" @cmds.check(is_me)
	@cmds.command()
	async def test(self, ctx):
		var = self.log._get_logger()
		print(var.name)
		print(f"name: {var}\ntype: {type(var)}\ndir: {dir(var)}\n\n") """

	## Reload bot
	@cmds.check(is_dev)
	@cmds.command(aliases=["rl"])
	async def reload(self, ctx, txt: str = None):
		u = ctx.author
		await ctx.message.delete()
		if txt:
			try:
				########################################
				self.log.warning(f"User {u} with UID: {u.id} requested a {txt} Module reload...")
				self.bot.unload_extension(f"cogs.{txt}")
				self.bot.load_extension(f"cogs.{txt}")
				await ctx.send("{} Reloaded {} module.".format(self.bot.bot_prefix, txt))
			except Exception as e:
					self.log.fatal("While reloading -> {}:\n{}\n".format(type(e).__name__, e))
					await ctx.send("``` {}: {} ```".format(type(e).__name__, e))
		else:
			k = []
			[ k.append(i) for i in self.bot.extensions ]
			l = len(k)
			self.log.warning(f"User {u} with UID: {u.id} requested for {l} Modules to be reloaded.")
			for i in k:
				self.bot.unload_extension(i)
				try:
					self.bot.load_extension(i)
				except Exception as e:
					await ctx.send("{} Failed to reload module `{}` ``` {}: {} ```".format(
						self.bot.bot_prefix, i, type(e).__name__, e))
					l -= 1
			await ctx.send("{} Reloaded {} of {} module(s).".format(self.bot.bot_prefix, l, len(k)))

	## shutdown
	@cmds.check(is_me)
	@cmds.command(aliases=["std"], hidden=True)
	async def shutdown(self, ctx):
		with open("quit.txt", "w", encoding="utf8") as fp:
			fp.write("quit")
		await ctx.send("{} Bot shut down.".format(self.bot.bot_prefix))
		os._exit(0)









def setup(bot):
	bot.add_cog(Bot_Core_Commands(bot))
