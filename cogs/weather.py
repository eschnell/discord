from discord.ext		import commands as cmds
from darksky			import forecast
from datetime			import datetime
from pytz				import timezone, country_timezones

from cogs.utils.locale	import get_user_locale

import discord as dis
import requests as req
import pycountry
import urllib.parse

from pprint				import PrettyPrinter
pp = PrettyPrinter(indent=4)

class Weather(cmds.Cog):

	def __init__(self, bot):
		self.bot = bot
		self.location_iq = self.bot.apis["location_iq"]
		self.darksky = self.bot.apis["darksky"]

	def get_moon_phase(self, ctx, val: float):
		_ = get_user_locale("weather", ctx.author)
		if val == 0:
			ret1, ret2 = _("New Moon"), "new_moon"
		elif val > 0 and val < 0.25:
			ret1, ret2 = _("Waxing Crescent Moon"), "waxing_crescent_moon"
		elif val == 0.25:
			ret1, ret2 = _("First Quarter Moon"), "first_quarter_moon"
		elif val > 0.25 and val < 0.5:
			ret1, ret2 = _("Waxing Gibbous Moon"), "waxing_gibbous_moon"
		elif val == 0.5:
			ret1, ret2 = _("Full Moon"), "full_moon"
		elif val > 0.5 and val < 0.75:
			ret1, ret2 = _("Waning Gibbous Moon"), "waning_gibbous_moon"
		elif val == 0.75:
			ret1, ret2 = _("Last Quarter Moon"), "last_quarter_moon"
		else:
			ret1, ret2 = _("Waning Crescent Moon"), "waning_crescent_moon"
		return ret1, ret2

	@cmds.command(pass_context=True, aliases=["wtr", "wr"])
	async def weather(self, ctx, city: str):
		_ = get_user_locale("weather", ctx.author)
		url = f"https://eu1.locationiq.com/v1/search.php?key={self.location_iq}&q={urllib.parse.quote_plus(city)}&format=json"
		r = req.get(url)

		#pp.pprint(r.json())
		if r.status_code != 200:
			return await ctx.send(_("City {} not found.").format(city))

		city = city.lower().capitalize()
		lst = r.json()[0]["display_name"].split(", ")
		country = lst[len(lst) - 1]
		country = pycountry.countries.search_fuzzy(country)[0]

		fmt = "\n%H:%M:%S %Z%z\n%d/%m/%Y"
		tz = timezone(country_timezones(country.alpha_2)[0])
		dt_now = datetime.now(tz=tz)
		dt = dt_now.strftime(fmt)

		lat = r.json()[0]["lat"]
		lon = r.json()[0]["lon"]
		forc = forecast(self.darksky, lat, lon, units="si")

		#increment on week (but get 1st day only)
		i = 0
		for day in forc.daily:
			if i == 1:
				break
			#pprint(	', '.join(i for i in dir(day) if not i.startswith('__')))
			i += 1

		moon_msg, moon_ico = self.get_moon_phase(ctx, float(day.moonPhase))

		#description="coming soon",
		em = dis.Embed(title=_("Forecast for {}, {} :flag_{}:").format(city, country.name, country.alpha_2.lower()),
					   description=_("__**Local time:**__{}").format(dt),
					   colour=dis.Colour.from_rgb(254, 254, 0))

		em.add_field(name=_("Temperature :thermometer:"),
					 value=_("{} °C\n{}°//{}° (Min // Max Temp)").format(forc.temperature, day.temperatureLow, day.temperatureHigh))

		em.add_field(name=_("Wind Speed :wind_blowing_face:"), value=f"{day.windSpeed} m/sec")

		time_fmt = datetime.utcfromtimestamp(day.uvIndexTime).strftime("%-H:%M %p")
		em.add_field(inline=False,
					 name=_("UV Index :radioactive:"),
					 value=_("Index {} at {}").format(day.uvIndex, time_fmt))

		em.add_field(name=_("Humidity :droplet:"), value=f"{int(float(day.humidity) * 100)}%")

		em.add_field(name=_("Pressure :dash:"), value=f"{day.pressure} hPa")

		em.add_field(name=_("Moon Phase :{}:").format(moon_ico),
					 value=moon_msg, inline=False)

		em.add_field(name=_("Daily forecast"), value=forc.daily.summary)

		em.set_footer(text=_("Powered by DarkSky.net"),
					  icon_url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/facebook/200/sparkling-heart_1f496.png")
		await ctx.send(content=None, embed=em)




def setup(bot):
	bot.add_cog(Weather(bot))
