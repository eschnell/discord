from discord.ext		import commands as cmds

import discord as dis
import urbandict as ud
import urllib

ud_author	= ""
ud_dictio	= []
ud_dlen		= 0
ud_title	= ""
i			= 1

class UrbanDictionnary(cmds.Cog):

	def __init__(self, bot):
		self.bot = bot

	###########
	# On Events
	@cmds.Cog.listener()
	async def on_reaction_add(self, reaction, user):
		global i

		if reaction.emoji == "\U00002B05" or reaction.emoji == "\U000027A1":
			if (i == 1 and reaction.emoji == "\U00002B05") or \
				(i == ud_dlen and reaction.emoji == "\U000027A1"):
				return
			if user.id == ud_author:
				i += (1, -1)[reaction.emoji == "\U00002B05"]
				txt = ud_dictio[i-1]["def"]
				if len(txt) > 1024:
					txt = txt[:1018] + "[...]"
				em = dis.Embed(title="Definition for {}".format(ud_title),
							   colour=dis.Colour.dark_teal(),
							   description=txt)
				if isinstance(str, type(ud_dictio[0]["example"])):
					txt = ud_dictio[i-1]["example"]
					if len(txt) > 1024:
						txt = txt[:1018] + "[...]"
					em.add_field(name="Example", value=txt, inline=False)
				em.add_field(name="Page", value=f"{i}/{ud_dlen}", inline=False)
				await reaction.message.edit(embed=em)

	@cmds.Cog.listener()
	async def on_reaction_remove(self, reaction, user):
		await self.on_reaction_add(reaction, user)

	############
	# On Command
	@cmds.command(pass_context=True, aliases=["ud"])
	async def urbandict(self, ctx, query = None):
		"""
		Parameters
		==========

		query: [str] text to lookup on UrbanDictionnary
		"""
		m = ctx.message.content
		if not query:
			cmd = self.bot.get_command(ctx.command.name)
			h = ''.join(["/urbandict [query]\n**Aliases:** ud\n\n", cmd.help])
			return await ctx.send(h)

		try:
			global ud_author, ud_dictio, ud_dlen, ud_title

			ud_title = m.split(" ", 1)[1]
			del(m)
			ud_author = ctx.message.author.id
			ud_dictio = ud.define(ud_title)
			ud_dlen = len(ud_dictio)
			txt = ud_dictio[0]["def"]
			if len(txt) > 1024:
				txt = txt[:1018] + "[...]"
			em = dis.Embed(title="Definition for {}".format(ud_title),
							colour=dis.Colour.dark_teal(),
							description=txt)
			if isinstance(str, type(ud_dictio[0]["example"])):
				txt = ud_dictio[0]["example"]
				if len(txt) > 1024:
					txt = txt[:1018] + "[...]"
				em.add_field(name="Example", value=txt, inline=False)
			em.add_field(name="Page", value=f"1/{ud_dlen}", inline=False)
			m = await ctx.send(embed=em)
			if ud_dlen > 1:
				await m.add_reaction("\U00002B05") # Left arrow "⬅"
				await m.add_reaction("\U000027A1") # Right arrow
		except urllib.error.HTTPError:
			await ctx.send("No result.")
		except Exception as e:
			print(e)
			raise cmds.CommandInvokeError(e)



def setup(bot):
	bot.add_cog(UrbanDictionnary(bot))
