# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-18 23:22+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: cogs/osu.py:336
msgid "{} Profile for {}"
msgstr ""

#: cogs/osu.py:343
msgid "**▸ {} Rank:** #{} {}\n"
msgstr ""

#: cogs/osu.py:344
msgid "**▸ Level:** {} ({:.2f}%)\n"
msgstr ""

#: cogs/osu.py:345
msgid "**▸ Total PP:** {}\n"
msgstr ""

#: cogs/osu.py:346
msgid "**▸ Playcount:** {}\n"
msgstr ""

#: cogs/osu.py:347
msgid "**▸ Hit Accuracy:** {}%"
msgstr ""
