��          �      |      �     �               &  	   D     N     a     p     �     �     �     �     �     �     �               1     E     d  #   z  �   �     <     R     k  !   �     �     �     �     �     �     �     
     (     8     S     k     �     �     �  #   �     �  #                     
            	                                                                   City {} not found. Daily forecast First Quarter Moon Forecast for {}, {} :flag_{}: Full Moon Humidity :droplet: Index {} at {} Last Quarter Moon Moon Phase :{}: New Moon Powered by DarkSky.net Pressure :dash: Temperature :thermometer: UV Index :radioactive: Waning Crescent Moon Waning Gibbous Moon Waxing Crescent Moon Waxing Gibbous Moon Wind Speed :wind_blowing_face: __**Local time:**__{} {} °C
{}°//{}° (Min // Max Temp) MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: Weather
Language: fr
 Ville {} introuvable. Prévisions quotidiennes Premier Quart de Lune Prévisions pour {}, {} :flag_{}: Pleine Lune Humidité :droplet: Indice {} à {} Dernier Quart de Lune Phase de Lune :{}: Nouvelle Lune Fonctionnant avec DarkSky.net Pression :dash: Température :thermometer: Indice UV :radioactive: Croissant de Lune Décroissant Lune Gibbeuse Décroissante Croissant de Lune Lune Gibbeuse Croissante Vitesse du Vent :wind_blowing_face: __**Heure Locale:**__{} {} °C
{}°//{}° (Min // Max Temp) 