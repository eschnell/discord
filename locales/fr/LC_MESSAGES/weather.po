msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: POEditor.com\n"
"Project-Id-Version: Weather\n"
"Language: fr\n"

#. wesh wesh broooo
#: cogs/weather.py:31
msgid "New Moon"
msgstr "Nouvelle Lune"

#: cogs/weather.py:33
msgid "Waxing Crescent Moon"
msgstr "Croissant de Lune"

#: cogs/weather.py:35
msgid "First Quarter Moon"
msgstr "Premier Quart de Lune"

#: cogs/weather.py:37
msgid "Waxing Gibbous Moon"
msgstr "Lune Gibbeuse Croissante"

#: cogs/weather.py:39
msgid "Full Moon"
msgstr "Pleine Lune"

#: cogs/weather.py:41
msgid "Waning Gibbous Moon"
msgstr "Lune Gibbeuse Décroissante"

#: cogs/weather.py:43
msgid "Last Quarter Moon"
msgstr "Dernier Quart de Lune"

#: cogs/weather.py:45
msgid "Waning Crescent Moon"
msgstr "Croissant de Lune Décroissant"

#: cogs/weather.py:54
msgid "City {} not found."
msgstr "Ville {} introuvable."

#: cogs/weather.py:81
msgid "Forecast for {}, {} :flag_{}:"
msgstr "Prévisions pour {}, {} :flag_{}:"

#: cogs/weather.py:82
msgid "__**Local time:**__{}"
msgstr "__**Heure Locale:**__{}"

#: cogs/weather.py:85
msgid "Temperature :thermometer:"
msgstr "Température :thermometer:"

#: cogs/weather.py:86
msgid "{} °C\n"
"{}°//{}° (Min // Max Temp)"
msgstr "{} °C\n"
"{}°//{}° (Min // Max Temp)"

#: cogs/weather.py:88
msgid "Wind Speed :wind_blowing_face:"
msgstr "Vitesse du Vent :wind_blowing_face:"

#: cogs/weather.py:92
msgid "UV Index :radioactive:"
msgstr "Indice UV :radioactive:"

#: cogs/weather.py:93
msgid "Index {} at {}"
msgstr "Indice {} à {}"

#: cogs/weather.py:95
msgid "Humidity :droplet:"
msgstr "Humidité :droplet:"

#: cogs/weather.py:97
msgid "Pressure :dash:"
msgstr "Pression :dash:"

#: cogs/weather.py:99
msgid "Moon Phase :{}:"
msgstr "Phase de Lune :{}:"

#: cogs/weather.py:102
msgid "Daily forecast"
msgstr "Prévisions quotidiennes"

#: cogs/weather.py:104
msgid "Powered by DarkSky.net"
msgstr "Fonctionnant avec DarkSky.net"

