��          �      |      �     �               &  	   D     N     a     p     �     �     �     �     �     �     �               1     E     d  #   z  �   �     <     W     i     �  	   �     �     �     �     �     �               +     E     ^     v     �     �  '   �     �  /   �                  
            	                                                                   City {} not found. Daily forecast First Quarter Moon Forecast for {}, {} :flag_{}: Full Moon Humidity :droplet: Index {} at {} Last Quarter Moon Moon Phase :{}: New Moon Powered by DarkSky.net Pressure :dash: Temperature :thermometer: UV Index :radioactive: Waning Crescent Moon Waning Gibbous Moon Waxing Crescent Moon Waxing Gibbous Moon Wind Speed :wind_blowing_face: __**Local time:**__{} {} °C
{}°//{}° (Min // Max Temp) MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: POEditor.com
Project-Id-Version: Weather
Language: pt
 Cidade {} não encontrada. Previsão diária Lua do Primeiro Trimestre Previsão para {}, {} :flag_{}: Lua Cheia Umidade :droplet: Índice {} em {} Lua do Último Trimestre Fase da Lua :{}: Lua Nova Ativado por DarkSky.net Pressão :dash: Temperatura :thermometer: Índice UV :radioactive: Lua Crescente Minguante Lua Minguante Gibbous Lua Crescente Crescente Lua Gibbosa Encerada Velocidade do Vento :wind_blowing_face: __**Horário local:**__{} {} ° C
{} ° // {} ° (Mín. // Temp. Máxima) 