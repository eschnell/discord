# -*- coding: utf-8 -*-




###########################################################
# XMPP League of Legends ChatBot
# by n2468txd
#
# SleekXMPP and JSON is required
# Template config file is in the repo
# Command listings on GitHub
#
# Username and Password for bot account required
# Main Account Summoner ID is required to message main account debug info if necessary and use admin commands
# Status format for XMPP can be found online with a quick google search
# Riot developer API key required, a development key will work just as well unless the bot is highly used
# IP prefrably in number format, just nslookup your region's xmpp server
# This bot is tested on Oceania region, untested on others.
# Was built against: OCE, OC1, SummonerVer=1.4, StaticDataVer=1.2, LeagueVer=2.5 for API modules in the Riot API
#
# http://github.com/n2468txd
###########################################################







############### Library / Module Imports ##################
from discord.ext                import commands as cmds

# ssllib
import urllib3
import time
import math
import logging
import ssl
import json
import requests
import os.path
import sys
from urllib             import parse


from cogs.utils.methods import getEmojiByName


################### VARS ##########################
# open json config
if os.path.exists('config/lolbot_config.json'):
        with open('config/lolbot_config.json') as f:
                data = json.load(f)
else:
        print("[WARNING] -- lolbot error: config does not exist")
        sys.exit()

riotApiKey = data["apiKey"]
riotApiRegion = data["apiRegion"]
riotApiRegionID = data["apiRegionSpectateID"]
riotApiSummonerVer = data["apiSummonerVer"]
riotApiStaticDataVer = data["apiStaticDataVer"]
riotApiLeagueVer = data["apiLeagueVer"]


r = requests.get("https://ddragon.leagueoflegends.com/realms/euw.json")
if r.status_code != 200:
        print("Invalid / having issues at the bot right now.")
ddVer = r.json()["dd"]
ddItem = r.json()["n"]["item"]
ddRune = r.json()["n"]["rune"]
ddMastery = r.json()["n"]["mastery"]
ddSummoner = r.json()["n"]["summoner"]
ddChampion = r.json()["n"]["champion"]
ddProfileIcon = r.json()["n"]["profileicon"]
ddMap = r.json()["n"]["map"]
ddLang = r.json()["n"]["language"]
ddSticker = r.json()["n"]["sticker"]

base_url = "https://%s.api.riotgames.com/lol" % riotApiRegion
############################ END OF CONFIG

################### Classes #######################

class Tier(enumerate):
        IRON = 0
        BRONZE = 1
        SILVER = 2
        GOLD = 3
        PLATINE = 4
        DIAMOND = 5
        MASTER = 6
        GRANDMASTER = 7
        CHALLENGER = 8

class Rank(enumerate):
        I = 1
        II = 2
        III = 3
        IV = 4
        V = 5

################### Functions #######################

def dd_getsummurl(summonerName):
        """ [str] Return summoner icon url """
        r = requests.get("%s/summoner/v%s/summoners/by-name/%s?api_key=%s" %
                (base_url, riotApiSummonerVer, summonerName, riotApiKey))
        if r.status_code != 200:
                return 0
        s = "https://ddragon.leagueoflegends.com/cdn/%s/img/profileicon/%s.png" % (ddVer, r.json()["profileIconId"])
        return s

def dd_getchampicon(championId, champURL):
        """ [str] link to new smaller icon"""
        fd = ""
        basepath, ext = "tmp/", ".png"

        def downloadImage(filename, url):
                r = requests.get(url)
                with open(filename, "wb") as out:
                        out.write(r.content)

        filename = basepath + championId + ext
        if not os.path.exists(basepath): os.makedirs(basepath)
        try:
                fd = {'{}'.format(filename): open(filename, "rb")}
        except:
                pass
        # If file didn't exist
        if not fd:
                downloadImage(filename, champURL)
        return filename


def dd_getchampurl(championName):
        """ [str] Return champion icon url """
        s = "https://ddragon.leagueoflegends.com/cdn/%s/img/champion/%s.png" % (ddVer, RiotGetChampionID(championName))
        return s


def RiotGetID(t, summonerName):
        """ [int]
        Get summoner OR account ID.
        """
        r = requests.get("%s/summoner/v%s/summoners/by-name/%s?api_key=%s" %
                (base_url, riotApiSummonerVer, summonerName, riotApiKey))
        if r.status_code != 200:
                return 0
        if t == "sum":
                ID = r.json()["id"]
        elif t == "acc":
                ID = r.json()["accountId"]
        return ID

def RiotGetChampionID(championName):
        """ [int] """
        r = requests.get("http://ddragon.leagueoflegends.com/cdn/%s/data/en_US/champion.json" % ddVer)
        if r.status_code != 200:
                return 0
        championlst = find_values("name", r.text)
        ind = championlst.index(str(championName))
        idlst = find_values("id", r.text)
        return idlst[ind]

def RiotGetChampionByKey(championID):
        """ Return Champ Name from ID """
        r = requests.get("http://ddragon.leagueoflegends.com/cdn/%s/data/en_US/champion.json" % ddVer)
        if r.status_code != 200:
                return 0

        championlst = find_values("key", r.text)
        ind = championlst.index(str(championID))
        championname = find_values("name", r.text)
        return championname[ind]

def RiotGetChampionList():
        r = requests.get('https://global.api.pvp.net/api/lol/static-data/' + riotApiRegion + '/v' + riotApiStaticDataVer + '/champion?dataById=true&api_key=' + riotApiKey)
        return r.json()

def RiotGetRegion():
        return riotApiRegion

## 10r/0,08sec <==> (125r // 1 sec)
def RiotGetLastGames(summonerName):
        i, nb = 0, 10
        eid = RiotGetID("acc", summonerName)
        l = []

        ## Request to get list of gameId of (10 last) match history games
        r = requests.get("%s/match/v%s/matchlists/by-account/%s?endIndex=%s&beginIndex=0&api_key=%s"
                % (base_url, riotApiSummonerVer, eid, str(nb), riotApiKey))
        if r.status_code != 200:
                return "Problem loading data\nfor **Top Champions**."

        while i < nb:
                l.append(r.json()["matches"][i]["gameId"])
                i += 1
        k = d = a = i = x = y = 0
        for mid in l:
                try:
                        ## Request for getting game info (basically x10 [nb] times)
                        r = requests.get("%s/match/v%s/matches/%s?api_key=%s"
                                % (base_url, riotApiSummonerVer, mid, riotApiKey))
                except Exception as e:
                        sys.stderr.write(e)
                state = find_values("win", r.text)
                ind = find_values("summonerName", r.text).index(summonerName)
                win = state[2:][ind]

                k += find_values("kills", r.text)[ind]
                d += find_values("deaths", r.text)[ind]
                a += find_values("assists", r.text)[ind]
                if win == True:
                        x += 1
                else:
                        y += 1
                i += 1

        k, d, a, wr = round(k / nb, 2), round(d / nb, 2), round(a / nb, 2), round(x * 100 / nb)
        kda = "{:.2g}/{:.2g}/{:.2g}".format(k, d, a)
        s = "**Winrate:** {}G ({}W {}L) | {}%WR\n**KDA Ratio:** [ {} ]".format(nb, x, y, wr, kda)
        return s

async def RiotGetChampionMastery(self, summonerName):

        async def upload_ico_as_emoji(ctx, cn):
                l = []
                img0 = dd_getchampicon(RiotGetChampionID(cn[0]), dd_getchampurl(cn[0]))
                img1 = dd_getchampicon(RiotGetChampionID(cn[1]), dd_getchampurl(cn[1]))
                img2 = dd_getchampicon(RiotGetChampionID(cn[2]), dd_getchampurl(cn[2]))
                with open(img0, "rb") as fd: img0 = fd.read()
                with open(img1, "rb") as fd: img1 = fd.read()
                with open(img2, "rb") as fd: img2 = fd.read()
                l.append(await ctx.guild.create_custom_emoji(name=cn[0].lower(), image=img0, reason="Lol info"))
                l.append(await ctx.guild.create_custom_emoji(name=cn[1].lower(), image=img1, reason="Lol info"))
                l.append(await ctx.guild.create_custom_emoji(name=cn[2].lower(), image=img2, reason="Lol info"))
                return l

        r = requests.get("%s/champion-mastery/v%s/champion-masteries/by-summoner/%s?api_key=%s"
                % (base_url, riotApiSummonerVer, RiotGetID("sum", summonerName), riotApiKey))
        if r.status_code != 200:
                return "Problem loading data\nfor **Top Champions**."
        x, y, z = r.json()[0], r.json()[1], r.json()[2]
        cn_array = [
                RiotGetChampionByKey(x["championId"]),
                RiotGetChampionByKey(y["championId"]),
                RiotGetChampionByKey(z["championId"])]
        to_del = await upload_ico_as_emoji(self, cn_array)
        return ("{} **M[{}]** 1. {}: {}\n{} **M[{}]** 2. {}: {}\n{} **M[{}]** 3. {}: {}".format(
                to_del[0], x["championLevel"], cn_array[0], "{:,}".format(int(x["championPoints"])),
                to_del[1], y["championLevel"], cn_array[1], "{:,}".format(int(y["championPoints"])),
                to_del[2], z["championLevel"], cn_array[2], "{:,}".format(int(z["championPoints"]))),
                to_del)

def RiotGetLevel(summonerName):
        """ Passed """
        r = requests.get("%s/summoner/v%s/summoners/by-name/%s?api_key=%s"
                        % (base_url, riotApiSummonerVer, summonerName, riotApiKey))
        if r.status_code != 200:
                return "Invalid / having issues at the bot right now."
        summonerLevel = r.json()["summonerLevel"]
        return str(summonerLevel)

def RiotGetRank(self, summonerName):

        def getRankString(j: dict, ind: int):
                t = {
                        "RANKED_SOLO_5x5": "**SOLO/DUO** Summoner's Rift",
                        "RANKED_FLEX_SR": "**FLEX** Summoner's Rift",
                        "RANKED_FLEX_TT": "**FLEX** Twisted Treeline"
                }
                d = j[ind]
                emoji = getEmojiByName(self, name=d["tier"].lower())
                return "{} **{} {}**\nin {}\n**{}LP** | ({}W {}L)\nWinrate: {}%".format(
                        emoji, d["tier"][0] + d["tier"][1:].lower(), d["rank"],
                        t[d["queueType"]],
                        d["leaguePoints"], d["wins"], d["losses"],
                        round(d["wins"] * 100 / (d["wins"] + d["losses"])))

        r = requests.get("%s/league/v%s/entries/by-summoner/%s?api_key=%s"
                % (base_url, riotApiSummonerVer, RiotGetID("sum", summonerName), riotApiKey))
        if r.status_code != 200:
                return "Invalid / having issues at the bot right now."
        qt = find_values("queueType", r.text)

        try:
                s = getRankString(r.json(), qt.index("RANKED_SOLO_5x5"))
        except:
                try:
                        s = getRankString(r.json(), qt.index("RANKED_FLEX_SR"))
                except:
                        try:
                                s = getRankString(r.json(), qt.index("RANKED_FLEX_TT"))
                        except:
                                s = "**Unranked**"
        return s


## Find function for json
def find_values(id, json_repr):
    results = []

    def _decode_dict(a_dict):
        try:
                results.append(a_dict[id])
        except KeyError:
                pass
        return a_dict

    json.loads(json_repr, object_hook=_decode_dict)  # Return value ignored.
    return results
