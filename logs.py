# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    logs.py                                          .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: eschnell <eschnell@student.le-101.fr>      +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/04/16 11:01:03 by eschnell     #+#   ##    ##    #+#        #
#    Updated: 2019/04/25 23:17:53 by eschnell    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #
import logging, os, sys
from discord.ext    import commands
from logging        import handlers



bot = commands.Bot(command_prefix=("/"))

## For logs
def set_log():
    errformat = logging.Formatter(
        '%(asctime)s %(levelname)s %(module)s %(funcName)s %(lineno)d: '
        '%(message)s',
        datefmt="[%d/%m/%Y %H:%M]")

    path = 'logs/discord.log'
    mb_size = 3
    loglvl = logging.DEBUG
    logger = logging.getLogger('discord')
    logger.setLevel(loglvl)
    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setLevel(loglvl)
    
    if not os.path.exists(path):
        os.makedirs('logs')
    errhandler = handlers.RotatingFileHandler(
        filename=path, encoding='utf-8', mode='a',
        maxBytes=mb_size*1024, backupCount=5)

    errhandler.setFormatter(errformat)
    logger.addHandler(errhandler)
    
    return (logger)
